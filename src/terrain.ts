import { Vector2, Vector3, Vector4, Quaternion, BufferGeometry, BufferAttribute, Texture, RepeatWrapping, OrthographicCamera, Scene,
	WebGLRenderTarget, WebGLRenderer, DataTexture, RGBFormat, AmbientLight, DirectionalLight, MeshBasicMaterial, Mesh, LinearFilter } from 'three';
import Transients from './transients';
import { COS30, SIN30, COS60, SIN60 } from './constants';
import { HexVector, hexCircleArea } from './hexagon';
import { HexCell, HexDecoration, cellPoints, cellRotations, cellOptions, HEX_GROUP_SIZE, HEX_GROUP_LENGTH } from './grid';
import { Renderer } from './renderer';
import { Renderable, ModelType, Model } from './renderables';
import { World, WorldSpecs } from './world';
import { HexGroup } from './grid';

export class TerrainRenderable extends Renderable {
	protected vertexShader = `
		attribute vec4 posrot;
		attribute vec4 cella;
		attribute vec4 cellb;
		attribute vec4 cellc;
		attribute vec3 barycentric;
		varying float voverall;
		varying float vstone;
		varying vec3 vbarycentric;
		varying vec3 vgrasstint;
		varying vec3 voverlay;

		vec3 applyRot2D(vec3 pos, vec2 rot) {
			vec2 applied = pos.x * rot + pos.z * vec2(-rot.y, rot.x);
			return vec3(applied.x, pos.y, applied.y);
		}

		vec3 normFromXY(float x, float y) {
			return vec3(x, sqrt(1.0-x*x-y*y), y);
		}

		vec3 permute(in vec3 x) { return mod( x*x*34.+x, 289.); }
		float snoise(in vec2 v) {
			vec2 i = floor((v.x+v.y)*.36602540378443 + v);
			vec2 x0 = (i.x+i.y)*.211324865405187 + v - i;
			float s = step(x0.x, x0.y);
			vec2 j = vec2(1.0-s, s);
			vec2 x1 = x0 - j + .211324865405187;
			vec2 x3 = x0 - .577350269189626;
			i = mod(i, 289.);
			vec3 p = permute(permute(i.y + vec3(0, j.y, 1)) + i.x + vec3(0, j.x, 1));
			vec3 m = max( .5 - vec3(dot(x0, x0), dot(x1, x1), dot(x3, x3)), 0.);
			vec3 x = fract(p * .024390243902439) * 2. - 1.;
			vec3 h = abs(x) - .5;
			vec3 a0 = x - floor(x + .5);
			return .5 + 65. * dot(pow(m, vec3(4.))*(-0.85373472095314*(a0*a0 + h*h)+1.79284291400159), a0 * vec3(x0.x, x1.x, x3.x) + h * vec3(x0.y, x1.y, x3.y));
		}

		float rescale(in float value, in float minVal, in float maxVal) {
			return value*(maxVal-minVal) + minVal;
		}

		vec4 unpack4(in float i) {
			return mod(floor(vec4(i, i / 64.0, i / 4096.0, i / 262144.0)), 64.0) / 63.0;
		}

		#include <std_vert_parameters>

		void main() {
			#include <std_vert_color>

			vec4 avalues = unpack4(cella.w);
			vec4 bvalues = unpack4(cellb.w);
			vec4 cvalues = unpack4(cellc.w);
			voverall = barycentric.x*avalues.x + barycentric.y*bvalues.x + barycentric.z*cvalues.x;
			vstone = barycentric.x*avalues.y + barycentric.y*bvalues.y + barycentric.z*cvalues.y;
			voverlay = vec3(avalues.z, bvalues.z, cvalues.z);

			#include <std_vert_normal>

			vec3 norma = normFromXY(cella.y, cella.z);
			vec3 normb = normFromXY(cellb.y, cellb.z);
			vec3 normc = normFromXY(cellc.y, cellc.z);
			objectNormal = normalize(barycentric.x*norma + barycentric.y*normb + barycentric.z*normc);

			#include <std_vert_position>

			vbarycentric = barycentric;
			float myheight = barycentric.x*cella.x + barycentric.y*cellb.x + barycentric.z*cellc.x;
			transformed = vec3(posrot.x, 0, posrot.y) + applyRot2D(transformed + vec3(0.0, myheight, 0.0), vec2(posrot.z, posrot.w));

			vUv = transformed.xz * 0.5;

			vec2 loc1 = transformed.xz;
			vec2 loc2 = vec2(loc1.y, loc1.x)+vec2(3000.0,7000.0);
			vec2 loc3 = vec2(loc1.y, loc1.x)+vec2(13000.0,3000.0);
			vec2 loc4 = transformed.xz+vec2(5000.0,11000.0);

			float drygrass = snoise(loc1 * 0.05 + vec2(snoise(loc2*0.05), snoise(loc3*0.05))*0.5)*0.5 + snoise(loc1 * 0.5)*0.5;
			float darkgrass = snoise(loc2 * 0.05 + vec2(snoise(loc3*0.05), snoise(loc4*0.05))*0.5)*0.5 + snoise(loc2 * 0.5)*0.5;
			vgrasstint = vec3(drygrass, darkgrass, 0.0);
			vgrasstint.x = vgrasstint.x*vgrasstint.x;
			vgrasstint.y = vgrasstint.y*vgrasstint.y;
			//vgrasstint = vec3(cella.w, cellb.w, cellc.w);

			#include <std_vert_extras>
		}
	`;

	protected fragmentShader = `
		varying float voverall;
		varying float vstone;
		varying vec3 vbarycentric;
		varying vec3 vgrasstint;
		varying vec3 voverlay;

		uniform sampler2D grassmap;
		uniform sampler2D flowersmap;
		uniform sampler2D stonemap;
		uniform sampler2D dirtmap;
		uniform sampler2D grassheightmap;
		uniform sampler2D stoneheightmap;
		uniform sampler2D noisemap;

		vec3 textureNoTile(in sampler2D texture, in vec2 uv) {
			float k = texture2D(noisemap, 0.05*uv).x;

			vec2 duvdx = dFdx(uv);
			vec2 duvdy = dFdy(uv);

			float l = k*8.0;
			float i = floor(l);
			float f = fract(l);

			vec2 offa = sin(vec2(3.0,7.0)*(i+0.0));
			vec2 offb = sin(vec2(3.0,7.0)*(i+1.0));

			vec3 cola = texture2DGradEXT(texture, uv + offa, duvdx, duvdy).xyz;
			vec3 colb = texture2DGradEXT(texture, uv + offb, duvdx, duvdy).xyz;

			vec3 colabdiff = cola-colb;
			return mix(cola, colb, smoothstep(0.2,0.8,f-0.1*(colabdiff.x+colabdiff.y+colabdiff.z)));
		}

		float heightmapblend(float a, float b, in float atob, in float margin) {
			float marginplus = 1.0+margin;
			float aheight = marginplus*(1.0-atob) + a;
			float bheight = marginplus*atob + b;
			return clamp((bheight-aheight)/margin, -1.0, 1.0)*0.5+0.5;
		}

		vec3 rgb2hsv(vec3 c) {
			vec4 K = vec4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
			vec4 p = mix(vec4(c.bg, K.wz), vec4(c.gb, K.xy), step(c.b, c.g));
			vec4 q = mix(vec4(p.xyw, c.r), vec4(c.r, p.yzx), step(p.x, c.r));

			float d = q.x - min(q.w, q.y);
			float e = 1.0e-10;
			return vec3(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);
		}

		vec3 hsv2rgb(vec3 c) {
			vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
			vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
			return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
		}

		vec3 toColor(float x) {
			return x == 0.0 ? vec3(0.0) : hsv2rgb(vec3(x, 1, 1));
		}

		vec3 permute(in vec3 x) { return mod( x*x*34.+x, 289.); }
		float snoise(in vec2 v) {
			vec2 i = floor((v.x+v.y)*.36602540378443 + v);
			vec2 x0 = (i.x+i.y)*.211324865405187 + v - i;
			float s = step(x0.x, x0.y);
			vec2 j = vec2(1.0-s, s);
			vec2 x1 = x0 - j + .211324865405187;
			vec2 x3 = x0 - .577350269189626;
			i = mod(i, 289.);
			vec3 p = permute(permute(i.y + vec3(0, j.y, 1)) + i.x + vec3(0, j.x, 1));
			vec3 m = max( .5 - vec3(dot(x0, x0), dot(x1, x1), dot(x3, x3)), 0.);
			vec3 x = fract(p * .024390243902439) * 2. - 1.;
			vec3 h = abs(x) - .5;
			vec3 a0 = x - floor(x + .5);
			return .5 + 65. * dot(pow(m, vec3(4.))*(-0.85373472095314*(a0*a0 + h*h)+1.79284291400159), a0 * vec3(x0.x, x1.x, x3.x) + h * vec3(x0.y, x1.y, x3.y));
		}

		float rescale(in float value, in float minVal, in float maxVal) {
			return value*(maxVal-minVal) + minVal;
		}

		#include <std_frag_parameters>

		void main() {
			#include <std_frag_color>

			vec3 grass = textureNoTile(grassmap, vUv);
			vec3 dirt = textureNoTile(dirtmap, vUv);
			vec3 flowers = textureNoTile(flowersmap, vUv);
			vec3 stone = textureNoTile(stonemap, vUv*0.2);

			vec3 drygrass = rgb2hsv(grass);
			drygrass.x -= 0.05;
			drygrass = hsv2rgb(drygrass);

			vec3 darkgrass = rgb2hsv(grass);
			darkgrass.z -= 0.06;
			darkgrass.x += 0.06;
			darkgrass = hsv2rgb(darkgrass);

			vec2 loc1 = vUv*4.0;
			vec2 loc2 = (vUv+vec2(13000.0,3000.0))*4.0;
			vec2 loc3 = vec2(loc1.y, loc1.x)+vec2(3000.0,7000.0);
			vec2 loc4 = vec2(loc1.y, loc1.x)-vec2(11000.0,5000.0);

			vec2 offset = vec2(snoise(loc4), snoise(loc3)*0.3);
			float dryness = snoise(loc1 + offset);
			float darkness = snoise(loc2 + offset);
			float vgrasstintx = rescale(vgrasstint.x, 0.0, 0.8) + rescale(dryness, -0.2, 0.2);
			float vgrasstinty = rescale(vgrasstint.y, 0.0, 0.8) + rescale(darkness, -0.2, 0.2);
			float dirtheight = dryness * snoise(loc2);
			float flowersheight = darkness * snoise(loc2);

			grass = mix(grass, drygrass, vgrasstintx);
			grass = mix(grass, darkgrass, vgrasstinty);
			grass = mix(grass, dirt, heightmapblend(texture2D(grassheightmap, vUv*2.0).x*0.5, dirtheight*0.6, vgrasstint.x*0.6, 0.1));
			grass = mix(grass, flowers, heightmapblend(texture2D(grassheightmap, vUv*2.0).x*0.5, dirtheight*0.7, vgrasstint.y*0.6, 0.1));

			float factor = heightmapblend(texture2D(grassheightmap, vUv).x*0.5, textureNoTile(stoneheightmap, vUv).x, vstone, 0.5);
			diffuseColor.rgb = mix(grass, stone, factor);
			diffuseColor.rgb *= voverall;

			// diffuseColor.rgb = vec3(voverall);
			if(voverlay.x > 0.0 && vbarycentric.x > vbarycentric.y && vbarycentric.x > vbarycentric.z) { diffuseColor.rgb = toColor(voverlay.x); }
			if(voverlay.y > 0.0 && vbarycentric.y > vbarycentric.x && vbarycentric.y > vbarycentric.z) { diffuseColor.rgb = toColor(voverlay.y); }
			if(voverlay.z > 0.0 && vbarycentric.z > vbarycentric.y && vbarycentric.z > vbarycentric.x) { diffuseColor.rgb = toColor(voverlay.z); }
			float width = 0.01;
			float xy = (abs(vbarycentric.x-vbarycentric.y) < width) && (vbarycentric.x > vbarycentric.z) ? 1.0 : 0.0;
			float xz = (abs(vbarycentric.x-vbarycentric.z) < width) && (vbarycentric.x > vbarycentric.y) ? 1.0 : 0.0;
			float zy = (abs(vbarycentric.z-vbarycentric.y) < width) && (vbarycentric.z > vbarycentric.x) ? 1.0 : 0.0;
			// xy = vbarycentric.x < width ? 1.0 : 0.0;
			// xz = vbarycentric.y < width ? 1.0 : 0.0;
			// zy = vbarycentric.z < width ? 1.0 : 0.0;
			float line = xy+xz+zy > 0.0 ? -0.2 : 0.0;
			diffuseColor.rgb *= 1.0+vec3(line);

			#include <std_frag_extras>
		}
	`;

	protected depthVertexShader = `
		attribute vec4 posrot;
		attribute vec4 cella;
		attribute vec4 cellb;
		attribute vec4 cellc;
		attribute vec3 barycentric;

		vec3 applyRot2D(vec3 pos, vec2 rot) {
			vec2 applied = pos.x * rot + pos.z * vec2(-rot.y, rot.x);
			return vec3(applied.x, pos.y, applied.y);
		}

		#include <std_depth_vert_parameters>

		void main() {
			#include <std_depth_vert_position>

			float myheight = barycentric.x*cella.x + barycentric.y*cellb.x + barycentric.z*cellc.x;
			transformed = vec3(posrot.x, 0.0, posrot.y) + applyRot2D(transformed + vec3(0.0, myheight, 0.0), vec2(posrot.z, posrot.w));

			#include <std_depth_vert_extras>
		}
	`;

	constructor(geometry: BufferGeometry, texture_maps: Texture[], max_instances: number) {
		super();

		for(const texture of texture_maps) {
			texture.wrapS = RepeatWrapping;
			texture.wrapT = RepeatWrapping;
		}

		this.initialize(TerrainRenderable, geometry, max_instances, [
			{ posrot: Vector4 },
			{ cella: Vector4 },
			{ cellb: Vector4 },
			{ cellc: Vector4 },
		], {
			grassmap: texture_maps[0],
			flowersmap: texture_maps[1],
			stonemap: texture_maps[2],
			dirtmap: texture_maps[3],
			grassheightmap: texture_maps[4],
			stoneheightmap: texture_maps[5],
			noisemap: texture_maps[6],
			roughness: 1,
			metalness: 0,
		}, {
			map: texture_maps[0],
		}, {
			castShadow: true,
			receiveShadow: true,
		});
	}
}

function TriangleGeometry(scale = 1, parts = 1): BufferGeometry {
	const verts = (parts + 1) * (parts + 2) / 2;
	const faces = parts * (parts + 1) / 2;

	const positions = new Float32Array(3 * verts);
	const uvs = new Float32Array(2 * verts);
	const normals = new Float32Array(3 * verts);
	const barycentrics = new Float32Array(3 * verts);
	const indexes = new Array<number>(3 * faces);

	let vert = 0;
	let face = 0;

	const pos0x = 0, pos0y = 0;
	const pos1x = scale * COS30, pos1y = scale * SIN30;
	const pos2x = scale * COS30, pos2y = scale * -SIN30;

	const uv0x = 0, uv0y = 0.5;
	const uv1x = COS30, uv1y = 0.5 + SIN30;
	const uv2x = COS30, uv2y = 0.5 - SIN30;

	for(let x = 0; x <= parts; x++) {
		for(let y = 0; y <= parts - x; y++) {
			const baryx = 1 - (x + y) / parts;
			const baryy = x / parts;
			const baryz = y / parts;

			barycentrics[vert * 3 + 0] = baryx;
			barycentrics[vert * 3 + 1] = baryy;
			barycentrics[vert * 3 + 2] = baryz;

			positions[vert * 3 + 0] = baryx * pos0x + baryy * pos1x + baryz * pos2x;
			positions[vert * 3 + 1] = 0;
			positions[vert * 3 + 2] = baryx * pos0y + baryy * pos1y + baryz * pos2y;

			uvs[vert * 2 + 0] = baryx * uv0x + baryy * uv1x + baryz * uv2x;
			uvs[vert * 2 + 1] = baryx * uv0y + baryy * uv1y + baryz * uv2y;

			normals[vert * 3 + 0] = 0;
			normals[vert * 3 + 1] = 1;
			normals[vert * 3 + 2] = 0;

			if(x + y < parts - 1) {
				indexes[face * 3 + 0] = x * (parts + 1) - (x) * (x - 1) / 2  +  y;
				indexes[face * 3 + 1] = (x + 1) * (parts + 1) - (x + 1) * (x) / 2  +  y;
				indexes[face * 3 + 2] = x * (parts + 1) - (x) * (x - 1) / 2  +  y  +  1;
				indexes[face * 3 + 3] = x * (parts + 1) - (x) * (x - 1) / 2  +  y  +  1;
				indexes[face * 3 + 4] = (x + 1) * (parts + 1) - (x + 1) * (x) / 2  +  y;
				indexes[face * 3 + 5] = (x + 1) * (parts + 1) - (x + 1) * (x) / 2  +  y  +  1;
				face += 2;
			} else if(x + y < parts) {
				indexes[face * 3 + 0] = x * (parts + 1) - (x) * (x - 1) / 2  +  y;
				indexes[face * 3 + 1] = (x + 1) * (parts + 1) - (x + 1) * (x) / 2  +  y;
				indexes[face * 3 + 2] = x * (parts + 1) - (x) * (x - 1) / 2  +  y  +  1;
				face += 1;
			}

			vert += 1;
		}
	}

	const geometry = new BufferGeometry();
	geometry.setIndex(indexes);
	geometry.setAttribute('position', new BufferAttribute(positions, 3));
	geometry.setAttribute('uv', new BufferAttribute(uvs, 2));
	geometry.setAttribute('normal', new BufferAttribute(normals, 3));
	geometry.setAttribute('barycentric', new BufferAttribute(barycentrics, 3));

	return geometry;
}

const EXTRA_HEX_GROUP_LENGTH = hexCircleArea(HEX_GROUP_SIZE * 1.5);

export class TerrainRenderer {
	private cell!: TerrainRenderable;
	private cachecell!: TerrainRenderable;
	private decorations!: Model[];
	private cachedecorations!: Model[];

	private cellPointsResult: Vector3[] = [];
	private cellRotationsResult: Quaternion[] = [];
	private cellOptionsResult: number[] = [];

	private camera!: OrthographicCamera;
	private scene!: Scene;
	private renderTarget!: WebGLRenderTarget;
	private groupmesh!: Mesh;
	private texturedata: Uint8Array = new Uint8Array(3 * 512 * 512);
	private visiblemeshes: Mesh[] = [];
	private renderables: Renderable[] = [];

	public load(renderer: Renderer, sunlight: DirectionalLight): Promise<void> {
		const grass = renderer.loadTexture('assets/textures/grass.png');
		const flowers = renderer.loadTexture('assets/textures/flowers.png');
		const stone = renderer.loadTexture('assets/textures/stone.png');
		const dirt = renderer.loadTexture('assets/textures/dirt.png');
		const grass_heightmap = renderer.loadTexture('assets/textures/grass_heightmap.png');
		const stone_heightmap = renderer.loadTexture('assets/textures/stone_heightmap.png');
		const noise = renderer.loadTexture('assets/textures/noise.png');

		const size = (HEX_GROUP_SIZE - 1) * Math.cos(Math.PI / 6) * 2 + Math.cos(Math.PI / 6);
		const uvsize = size + 0.5;

		this.camera = new OrthographicCamera(-uvsize, uvsize, uvsize, -uvsize, 1, 500);
		this.camera.position.set(0, 150, 0);
		this.camera.rotation.set(-Math.PI / 2, 0, 0);
		this.scene = new Scene();
		this.renderTarget = new WebGLRenderTarget(512, 512);

		const light = new DirectionalLight(0xffffff);
		light.position.copy(sunlight.position);
		light.target.position.copy(sunlight.target.position);
		light.castShadow = sunlight.castShadow;
		light.shadow.mapSize.width = 512;
		light.shadow.mapSize.height = 512;
		light.shadow.camera.near = -200;
		light.shadow.camera.far = 100;
		light.shadow.camera.top = uvsize * 4;
		light.shadow.camera.bottom = -uvsize * 4;
		light.shadow.camera.left = -uvsize * 4;
		light.shadow.camera.right = uvsize * 4;
		this.scene.add(light);
		this.scene.add(light.target);

		this.scene.add(new AmbientLight(0xffffff, 0.2));

		const positions = new Float32Array(6 * 3);
		const normals = new Float32Array(6 * 3);
		const uvs = new Float32Array(6 * 2);
		for(let i = 5; i >= 0; i--) {
			const x = Math.cos(Math.PI / 3 * -i + Math.PI / 3) * 0.5 + Math.cos(Math.PI / 3 * i + Math.PI / 6) * size;
			const y = Math.sin(Math.PI / 3 * -i + Math.PI / 3) * -0.5 + Math.sin(Math.PI / 3 * i + Math.PI / 6) * size;

			positions[i * 3 + 0] = x;
			positions[i * 3 + 1] = 0;
			positions[i * 3 + 2] = y;

			normals[i * 3 + 0] = 0;
			normals[i * 3 + 1] = 1;
			normals[i * 3 + 2] = 0;

			uvs[i * 2 + 0] = 0.5 + x / (2 * uvsize);
			uvs[i * 2 + 1] = 0.5 - y / (2 * uvsize);
		}
		const geometry = new BufferGeometry();
		geometry.setIndex([0, 5, 1, 1, 5, 2, 2, 5, 4, 2, 4, 3]);
		geometry.setAttribute('position', new BufferAttribute(positions, 3));
		geometry.setAttribute('normal', new BufferAttribute(normals, 3));
		geometry.setAttribute('uv', new BufferAttribute(uvs, 2));
		this.groupmesh = new Mesh(geometry, new MeshBasicMaterial());

		return Promise.all([grass, flowers, stone, dirt, grass_heightmap, stone_heightmap, noise]).then(maps => {
			this.cell = new TerrainRenderable(TriangleGeometry(2 * COS30), maps, renderer.getMaxInstances());
			renderer.addRenderable(this.cell);

			this.cachecell = this.cell.clone() as TerrainRenderable;
			this.scene.add(this.cachecell.getObject());
			this.renderables.push(this.cachecell);

			const rock = renderer.getModel(ModelType.Rock);
			const cacherock = rock.clone(true);
			for(const renderable of cacherock.getRenderables()) { this.scene.add(renderable.getObject()); this.renderables.push(renderable); }
			const rock2 = renderer.getModel(ModelType.Rock2);
			const cacherock2 = rock2.clone(true);
			for(const renderable of cacherock2.getRenderables()) { this.scene.add(renderable.getObject()); this.renderables.push(renderable); }
			const tree = renderer.getModel(ModelType.Tree);
			const cachetree = tree.clone(true);
			for(const renderable of cachetree.getRenderables()) { this.scene.add(renderable.getObject()); this.renderables.push(renderable); }
			const bush = renderer.getModel(ModelType.Bush);
			const cachebush = bush.clone(true);
			for(const renderable of cachebush.getRenderables()) { this.scene.add(renderable.getObject()); this.renderables.push(renderable); }
			this.decorations = [tree, bush, rock, rock2];
			this.cachedecorations = [cachetree, cachebush, cacherock, cacherock2];
		});
	}

	public render(world: World, renderer: Renderer, webglrenderer: WebGLRenderer, scene: Scene, visibleGroups: number[], detail: boolean): void {
		const visiblemeshes = this.visiblemeshes;
		for(let index = visiblemeshes.length - 1; index >= 0; index--) {
			visiblemeshes[index].visible = false;
		}
		visiblemeshes.length = 0;

		const groups = world.groups;
		const offset = Transients.get(Vector3).set(0, 0, 0);
		for(let index = visibleGroups.length - 1; index >= 0; index--) {
			const group = groups.getByIndex(visibleGroups[index]);
			if(!group) { continue; }
			if(detail) {
				this.renderGroup(world, group, offset, HEX_GROUP_LENGTH, this.cell, this.decorations);
			} else {
				if(!group.renderer) { this.createGroupRenderer(webglrenderer, scene, world, group, offset); }
				if(!group.renderer) { throw new Error(); }
				if(group.dirty) { this.renderGroupToTexture(webglrenderer, (group.renderer.material as {map?: Texture}).map as Texture, world, group, offset); }
				group.renderer.visible = true;
				visiblemeshes.push(group.renderer);
			}
		}
	}

	private createGroupRenderer(webglrenderer: WebGLRenderer, scene: Scene, world: World, group: HexGroup, offset: Vector3): void {
		const mesh = this.groupmesh.clone();
		const material = (mesh.material as MeshBasicMaterial).clone();
		material.map = new DataTexture(this.texturedata, 512, 512, RGBFormat);
		material.map.minFilter = LinearFilter;
		material.map.magFilter = LinearFilter;
		material.map.needsUpdate = true;
		mesh.material = material;
		group.center.toVector3(mesh.position);
		scene.add(mesh);

		this.renderGroupToTexture(webglrenderer, material.map, world, group, offset);

		group.renderer = mesh;
	}

	private renderGroupToTexture(webglrenderer: WebGLRenderer, texture: Texture, world: World, group: HexGroup, offset: Vector3): void {
		Transients.push(Vector2);

		group.dirty = false;
		group.center.toVector3(offset);
		offset.multiplyScalar(-1);
		this.renderGroup(world, group, offset, EXTRA_HEX_GROUP_LENGTH, this.cachecell, this.cachedecorations);

		for(let index = this.renderables.length - 1; index >= 0; index--) {
			this.renderables[index].update();
		}

		webglrenderer.setRenderTarget(this.renderTarget);
		webglrenderer.render(this.scene, this.camera);
		(webglrenderer as {copyFramebufferToTexture: (vector: Vector2, texture: Texture) => unknown}).copyFramebufferToTexture(Transients.get(Vector2).set(0, 0), texture);
		webglrenderer.clear();
		webglrenderer.setRenderTarget(null);

		Transients.pop(Vector2);
	}

	private renderGroup(world: World, group: HexGroup, offset: Vector3, length: number, cell: TerrainRenderable, decorations: Model[]): void {
		Transients.push(HexVector);
		Transients.push(Vector2);
		Transients.push(Vector3);
		Transients.push(Vector4);

		const hex = Transients.get(HexVector);
		const temphex: HexVector = Transients.get(HexVector);
		const position: Vector2 = Transients.get(Vector2);
		const rotation1: Vector2 = Transients.get(Vector2).set(1, 0);
		const rotation2: Vector2 = Transients.get(Vector2).set(COS60, SIN60);
		const posrot: Vector4 = Transients.get(Vector4);
		const cella: Vector4 = Transients.get(Vector4);
		const cellb: Vector4 = Transients.get(Vector4);
		const cellc: Vector4 = Transients.get(Vector4);
		const grid = world.grid;

		const off00 = Transients.get(HexVector).set(0, 0);
		const off11 = Transients.get(HexVector).set(0, 1);
		const off01 = Transients.get(HexVector).set(-1, 1);
		const off10 = Transients.get(HexVector).set(1, 0);

		for(let cellindex = length - 1; cellindex >= 0; cellindex--) {
			hex.fromIndex(cellindex).add(group.center);
			const cell00 = grid.get(temphex.copy(hex).add(off00));
			if(!cell00) { continue; }
			const cell00brightness = this.getCellBrightness(temphex, cell00, world);
			const cell11 = grid.get(temphex.copy(hex).add(off11));
			if(!cell11) { continue; }
			const cell11brightness = this.getCellBrightness(temphex, cell11, world);
			const cell01 = grid.get(temphex.copy(hex).add(off01));
			if(!cell01) { continue; }
			const cell01brightness = this.getCellBrightness(temphex, cell01, world);
			const cell10 = grid.get(temphex.copy(hex).add(off10));
			if(!cell10) { continue; }
			const cell10brightness = this.getCellBrightness(temphex, cell10, world);

			hex.toVector2(position);
			position.x += offset.x;
			position.y += offset.z;

			posrot.set(position.x, position.y, rotation1.x, rotation1.y);
			cella.set(cell00.height, cell00.normal.x, cell00.normal.z, this.pack4(cell00brightness, cell00.texture, cell00.overlay, 0));
			cellb.set(cell11.height, cell11.normal.x, cell11.normal.z, this.pack4(cell11brightness, cell11.texture, cell11.overlay, 0));
			cellc.set(cell01.height, cell01.normal.x, cell01.normal.z, this.pack4(cell01brightness, cell01.texture, cell01.overlay, 0));
			cell.render(posrot, cella, cellb, cellc);

			posrot.set(position.x, position.y, rotation2.x, rotation2.y);
			cella.set(cell00.height, cell00.normal.x, cell00.normal.z, this.pack4(cell00brightness, cell00.texture, cell00.overlay, 0));
			cellb.set(cell10.height, cell10.normal.x, cell10.normal.z, this.pack4(cell10brightness, cell10.texture, cell10.overlay, 0));
			cellc.set(cell11.height, cell11.normal.x, cell11.normal.z, this.pack4(cell11brightness, cell11.texture, cell11.overlay, 0));
			cell.render(posrot, cella, cellb, cellc);

			if((cell00.rocks || cell00.trees || cell00.decoration) && !cell00.decorations) {
				const celldecorations: HexDecoration[] = [];

				if(cell00.rocks) {
					Transients.push(Vector3);
					Transients.push(Quaternion);
					cellPoints(grid, hex, 0.2, cell00.rocks, this.cellPointsResult);
					cellRotations(hex, this.cellPointsResult.length, this.cellRotationsResult);
					cellOptions(hex, this.cellPointsResult.length, 2, this.cellOptionsResult);
					for(let i = this.cellPointsResult.length - 1; i >= 0; i--) {
						const decoration = new HexDecoration();
						decoration.type = this.cellOptionsResult[i] + 2;
						decoration.position.copy(this.cellPointsResult[i]);
						decoration.rotation.copy(this.cellRotationsResult[i]);
						decoration.scale = 1;
						celldecorations.push(decoration);
					}
					Transients.pop(Vector3);
					Transients.pop(Quaternion);
				}

				if(cell00.trees) {
					Transients.push(Vector3);
					Transients.push(Quaternion);
					cellPoints(grid, hex, 0.2, cell00.trees, this.cellPointsResult);
					cellRotations(hex, this.cellPointsResult.length, this.cellRotationsResult);
					for(let i = this.cellPointsResult.length - 1; i >= 0; i--) {
						const decoration = new HexDecoration();
						decoration.type = 0;
						decoration.position.copy(this.cellPointsResult[i]);
						decoration.rotation.copy(this.cellRotationsResult[i]);
						decoration.scale = 1;
						celldecorations.push(decoration);
					}
					cellPoints(grid, hex, 0.2, cell00.trees, this.cellPointsResult, 1);
					cellRotations(hex, this.cellPointsResult.length, this.cellRotationsResult, 1);
					for(let i = this.cellPointsResult.length - 1; i >= 0; i--) {
						const decoration = new HexDecoration();
						decoration.type = 1;
						decoration.position.copy(this.cellPointsResult[i]);
						decoration.rotation.copy(this.cellRotationsResult[i]);
						decoration.scale = 1;
						celldecorations.push(decoration);
					}
					Transients.pop(Vector3);
					Transients.pop(Quaternion);
				}

				if(cell00.decoration) {
					Transients.push(Vector3);
					Transients.push(Quaternion);
					cellPoints(grid, hex, 0.3, 0.3, this.cellPointsResult);
					cellRotations(hex, this.cellPointsResult.length, this.cellRotationsResult);
					cellOptions(hex, this.cellPointsResult.length, 3, this.cellOptionsResult);
					for(let i = this.cellPointsResult.length - 1; i >= 0; i--) {
						const decoration = new HexDecoration();
						decoration.type = this.cellOptionsResult[i] + 1;
						decoration.position.copy(this.cellPointsResult[i]);
						decoration.rotation.copy(this.cellRotationsResult[i]);
						decoration.scale = 1;
						if(decoration.type > 1) {
							decoration.scale = 0.2;
							decoration.position.y += 0.15;
						}
						celldecorations.push(decoration);
					}
					Transients.pop(Vector3);
					Transients.pop(Quaternion);
				}

				cell00.decorations = celldecorations;
			}

			if(cell00.decorations) {
				for(let i = cell00.decorations.length - 1; i >= 0; i--) {
					const decoration = cell00.decorations[i];
					const model = decorations[decoration.type];
					model.position.copy(decoration.position).add(offset);
					model.rotation.copy(decoration.rotation);
					model.color.setRGB(1, 1, 1).multiplyScalar(cell00brightness);
					model.scale.set(decoration.scale, decoration.scale, decoration.scale);
					model.render();
				}
			}
		}

		Transients.pop(HexVector);
		Transients.pop(Vector2);
		Transients.pop(Vector3);
		Transients.pop(Vector4);
	}

	private pack4(a: number, b: number, c: number, d: number): number {
		// We have 24 bits of precision in a 32 bit float
		const precision = 64; // 2^6
		const factor1 = 1; // 2^0
		const factor2 = 64; // 2^6
		const factor3 = 4096; // 2^12
		const factor4 = 262144; // 2^16
		const x = Math.floor(a * (precision - 1));
		const y = Math.floor(b * (precision - 1));
		const z = Math.floor(c * (precision - 1));
		const w = Math.floor(d * (precision - 1));
		return (x * factor1) + (y * factor2) + (z * factor3) + (w * factor4);
	}

	private smoothstep(x: number): number {
		return x * x * (3.0 - 2.0 * x);
	}

	private getCellBrightness(hex: HexVector, cell: HexCell, world: World): number {
		const cellVisibility = cell.visibility;
		// let visibility = lerpTime(cellVisibility.from, cellVisibility.to, world.visibilityChangeTime, cellVisibility.start, world.tick);
		const visibility = cellVisibility.to * 0.7 + 0.3;

		const gridSize = world.grid.size;
		const maxHeight = WorldSpecs[world.type].terrainHeight;
		const dist = hex.length();
		const edginess = (dist > gridSize - maxHeight * 0.7) ? this.smoothstep((dist - gridSize + maxHeight * 0.7) / (maxHeight * 0.7)) : 0;
		const nonedginess = 1.0 - edginess;

		return nonedginess * visibility;
	}
}
