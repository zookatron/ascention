import React, { useState } from 'react';
import { createRoot } from 'react-dom/client';
import classnames from 'classnames';
import styles from './styles.module.scss';
import { StructureType } from './structure';

interface BuildingsProps extends StateProps { hidden: boolean; select: () => void; }
// tslint:disable-next-line:variable-name
const Buildings = ({ hidden, select, state, setState }: BuildingsProps) => {
	return <div className={ styles.buildings }>
		<div className={ styles.buildingsButton } onClick={ select }>Buildings</div>
		<div className={ classnames(styles.sidebarList, { [styles.sidebarListHidden]: hidden }) }>
			<div className={ classnames(styles.sidebarListInner) }>
				<div className={ classnames(styles.building, styles.buildingDefault) } onClick={ () => setState({ make: { ...state.make, building: StructureType.Miner } }) }></div>
				<div className={ classnames(styles.building, styles.buildingDefault) } onClick={ () => setState({ make: { ...state.make, building: StructureType.Factory } }) }></div>
			</div>
		</div>
	</div>;
};

// tslint:disable-next-line:variable-name
const CommandSidebar = ({ state, setState }: StateProps) => {
	const [selected, setSelected] = useState('none');
	return <div className={ styles.commandSidebar }>
		<Buildings hidden={ selected !== 'buildings' } select={ () => setSelected(selected === 'buildings' ? 'none' : 'buildings') } state={ state } setState={ setState }/>
	</div>;
};

class InterfaceLayout extends React.Component<unknown, StateProps> {
	public render() {
		return this.state ? (<div>
			<CommandSidebar state={ this.state.state } setState={ this.state.setState }/>
			<div className={ styles.fpsCounter }>{ this.state.state.fps } FPS</div>
		</div> as React.ReactNode) : null;
	}
}

export interface StateProps {
	state: InterfaceState;
	setState: (stateChanges: InterfaceChanges) => void;
}

export interface InterfaceState {
	fps: number;
	make: {
		building: StructureType | null;
	};
}

export interface InterfaceChanges {
	fps?: number;
	make?: {
		building?: StructureType | null;
	};
}

export class Interface {
	private state: InterfaceState = {
		fps: 0,
		make: {
			building: null,
		},
	};
	private layout: InterfaceLayout | null = null;

	constructor(container: HTMLElement) {
		createRoot(container).render(<InterfaceLayout ref={this.updateLayout.bind(this)}/>);
	}

	public updateLayout(layout: InterfaceLayout | null) {
		this.layout = layout;
		if(this.layout) {
			this.layout.setState({ state: this.state, setState: (stateChanges: InterfaceChanges) => this.setState(stateChanges) });
		}
	}

	public getState(): InterfaceState {
		return this.state;
	}

	public setState(stateChanges: InterfaceChanges): void {
		if(this.layout) {
			this.state = { ...this.state, ...stateChanges } as InterfaceState;
			this.layout.setState({ state: this.state });
		}
	}
}
