import { World } from './world';
import { EPSILON } from './constants';

export class Universe {
	private worlds: Map<string, World> = new Map<string, World>();
	private currentWorld: World;
	private transitionAmount = 0;
	private transitioningTo: World | null = null;
	private transitionRate = 0.1;

	constructor() {
		this.currentWorld = new World('');
		this.worlds.set('', this.currentWorld);
	}

	public get world(): World {
		return this.currentWorld;
	}

	public get transitioning(): number {
		return this.transitionAmount;
	}

	public snapTo(id: string) {
		this.transitionAmount = 0;
		this.transitioningTo = null;
		this.currentWorld = this.getWorld(id);
	}

	public transitionTo(id: string) {
		this.transitionAmount = -1;
		this.transitioningTo = this.getWorld(id);
	}

	public update(delta: number): void {
		if(this.transitioningTo) {
			this.transitionAmount += delta * this.transitionRate;
			if(this.transitionAmount === 0) { this.transitionAmount += EPSILON; }
			if(this.transitionAmount > 0 && this.currentWorld !== this.transitioningTo) {
				this.currentWorld = this.transitioningTo;
			}
			if(this.transitionAmount > 1) {
				this.transitioningTo = null;
				this.transitionAmount = 0;
			}
		}
	}

	private getWorld(id: string): World {
		return (this.worlds.has(id) ? this.worlds.get(id) : this.worlds.set(id, new World(id)).get(id)) as World;
	}
}
