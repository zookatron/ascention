import { Vector2, Vector3, Quaternion, Ray, Mesh } from 'three';
import * as _ from 'lodash';
import Transients from './transients';
import Random from './random';
import { HexVector, HexGrid, hexCircleArea, hexNeighbors, hexLine, hexFilledCircle, hexNeighborIndexes, HexDirections, triangleRay } from './hexagon';
import { zipIterables, iterableEvery } from './utilities';
import { EPSILON } from './constants';
import { Structure } from './structure';

export class HexCellVisibility {
	constructor(
		public contributors: number = 0,
		public from: number = 0,
		public to: number = 0,
		public start: number = 0,
		public seen: boolean = false,
	) {}

	public copy(other: HexCellVisibility): HexCellVisibility {
		this.contributors = other.contributors;
		this.from = other.from;
		this.to = other.to;
		this.start = other.start;
		return this;
	}
}

export class HexCell {
	public position: HexVector = new HexVector();
	public height: number;
	public obstacle: boolean;
	public texture: number;
	public trees: number;
	public rocks: number;
	public decoration: number;
	public overlay = 0;
	public normal: Vector3 = new Vector3();
	public seen = false;
	public visible = false;
	public visibility: HexCellVisibility = new HexCellVisibility();
	public decorations: HexDecoration[] | null = null;
	public minheight = 0;
	public maxheight = 0;

	constructor(
		position: HexVector,
		height: number,
		obstacle: boolean,
		texture: number,
		trees: number,
		rocks: number,
		decoration: number,
		normal: Vector3,
	) {
		this.position.copy(position);
		this.height = height;
		this.obstacle = obstacle;
		this.texture = texture;
		this.trees = trees;
		this.rocks = rocks;
		this.decoration = decoration;
		this.normal.copy(normal);
	}
}

export class HexGroup {
	public center: HexVector = new HexVector();
	public position: Vector3 = new Vector3();
	public radius: number;
	public segments: HexGrid<number>;
	public connections: number[][];
	public structures: Structure[] = [];
	public renderer: Mesh | undefined;
	public dirty = false;

	constructor(
		center: HexVector,
		position: Vector3,
		radius: number,
		segments: HexGrid<number>,
		connections: number[][],
	) {
		this.center.copy(center);
		this.position.copy(position);
		this.radius = radius;
		this.segments = segments;
		this.connections = connections;
	}
}

export class HexDecoration {
	public type = 0;
	public position: Vector3 = new Vector3();
	public rotation: Quaternion = new Quaternion();
	public scale = 0;
}

export const HEX_GROUP_SIZE = 10;
export const HEX_GROUP_LENGTH = hexCircleArea(HEX_GROUP_SIZE);

const hexGroupIndexArray: [number, number] = [0, 0];
export function hexGroupIndex(index: number): [number, number] {
	Transients.push(HexVector);
	const indexhex = Transients.get(HexVector).fromIndex(index);
	const hex = Transients.get(HexVector);
	hexGroupIndexArray[0] = hex.copy(indexhex).tuple(HEX_GROUP_SIZE).round().toIndex();
	hexGroupIndexArray[1] = hex.antituple(HEX_GROUP_SIZE).sub(indexhex).multiplyScalar(-1).toIndex();
	Transients.pop(HexVector);
	return hexGroupIndexArray;
}

export function getCellVisibilityGroups(groups: HexGrid<HexGroup>, position: HexVector, results: HexGroup[]): void {
	Transients.push(HexVector);
	const temp = Transients.get(HexVector);
	Transients.pop(HexVector);
	results.length = 0;
	for(let index = 5; index >= 0; index--) {
		const group = groups.get(temp.fromIndex(index).add(position).tuple(HEX_GROUP_SIZE).round());
		if(!group) { continue; }
		if(!results.includes(group)) { results.push(group); }
	}
}

export function getIsUpperTriangle(pos: HexVector): boolean {
	const col = Math.floor(pos.col);
	const row = Math.floor(pos.row);
	const colpart = pos.col - col;
	const rowpart = pos.row - row;
	return colpart + rowpart > 1;
}

export function getTrianglePoints(pos: HexVector, point1: HexVector, point2: HexVector, point3: HexVector): void {
	const col = Math.floor(pos.col);
	const row = Math.floor(pos.row);
	const colpart = pos.col - col;
	const rowpart = pos.row - row;
	if(colpart + rowpart > 1) {
		point1.set(row + 1, col + 1);
	} else {
		point1.set(row, col);
	}
	point2.set(row + 1, col);
	point3.set(row, col + 1);
}

export function getTriangleBarycentrics(pos: HexVector, result: Vector3): void {
	const col = Math.floor(pos.col);
	const row = Math.floor(pos.row);
	const colpart = pos.col - col;
	const rowpart = pos.row - row;
	if(colpart + rowpart > 1) {
		result.set(colpart + rowpart - 1, 1 - colpart, 1 - rowpart);
	} else {
		result.set(1 - colpart - rowpart, rowpart, colpart);
	}
}

export function getGridHeight(grid: HexGrid<HexCell>, pos: HexVector): number {
	Transients.push(HexVector);
	Transients.push(Vector3);
	const point1 = Transients.get(HexVector);
	const point2 = Transients.get(HexVector);
	const point3 = Transients.get(HexVector);
	const bary = Transients.get(Vector3);
	getTrianglePoints(pos, point1, point2, point3);
	getTriangleBarycentrics(pos, bary);
	const point1cell = grid.get(point1);
	const point2cell = grid.get(point2);
	const point3cell = grid.get(point3);
	const point1height = point1cell ? point1cell.height : 0;
	const point2height = point2cell ? point2cell.height : 0;
	const point3height = point3cell ? point3cell.height : 0;
	Transients.pop(HexVector);
	Transients.pop(Vector3);
	return bary.x * point1height + bary.y * point2height + bary.z * point3height;
}

export function getGridOritentation(grid: HexGrid<HexCell>, pos: HexVector, result: Quaternion): void {
	Transients.push(HexVector);
	Transients.push(Vector2);
	Transients.push(Vector3);
	const point1 = Transients.get(HexVector);
	const point2 = Transients.get(HexVector);
	const point3 = Transients.get(HexVector);
	getTrianglePoints(pos, point1, point2, point3);
	const point1cell = grid.get(point1);
	const point2cell = grid.get(point2);
	const point3cell = grid.get(point3);
	const point1height = point1cell ? point1cell.height : 0;
	const point2height = point2cell ? point2cell.height : 0;
	const point3height = point3cell ? point3cell.height : 0;
	const islowhalf = point1.col <= point2.col && point1.row <= point2.row;
	const col_deriv = islowhalf ? point3height - point1height : point1height - point2height;
	const row_deriv = islowhalf ? point2height - point1height : point1height - point3height;
	const temp = Transients.get(HexVector);
	const tempvec = Transients.get(Vector2);
	const tempveca = Transients.get(Vector3);
	const tempvecb = Transients.get(Vector3);
	const normal = Transients.get(Vector3);
	const up = Transients.get(Vector3).set(0, 1, 0);
	temp.set(0, 1).toVector2(tempvec);
	const col_vector = tempveca.set(tempvec.x, col_deriv, tempvec.y);
	temp.set(1, 0).toVector2(tempvec);
	const row_vector = tempvecb.set(tempvec.x, row_deriv, tempvec.y);
	normal.copy(row_vector).cross(col_vector).normalize();
	result.setFromUnitVectors(up, normal);
	Transients.pop(HexVector);
	Transients.pop(Vector2);
	Transients.pop(Vector3);
}

export function getGridPosition(grid: HexGrid<HexCell>, pos: HexVector, result: Vector3): void {
	Transients.push(Vector2);
	const vec = Transients.get(Vector2);
	pos.toVector2(vec);
	result.set(vec.x, getGridHeight(grid, pos), vec.y);
	Transients.pop(Vector2);
}

export function getGridCircleHeight(grid: HexGrid<HexCell>, pos: HexVector, radius: number): number {
	Transients.push(Vector3);
	Transients.push(HexVector);
	const posvec = Transients.get(Vector3);
	const poshex = Transients.get(HexVector);
	let maxHeight = -Infinity;
	for(let i = hexCircleArea(radius) - 1; i >= 0; i--) {
		getGridPosition(grid, poshex.fromIndex(i).add(pos), posvec);
		if(posvec.y > maxHeight) { maxHeight = posvec.y; }
	}
	Transients.pop(Vector3);
	Transients.pop(HexVector);
	return maxHeight;
}

const numPrecalculatedAngles = 360;
const precalculatedAngles: Array<[number, number]> = precalculateAngles(numPrecalculatedAngles);
export function precalculateAngles(numangles: number): Array<[number, number]> {
	const result: Array<[number, number]> = [];
	for(let i = 0; i < numangles; i++) {
		result[i] = [Math.cos(Math.PI * 2 * i / numangles), Math.sin(Math.PI * 2 * i / numangles)];
	}
	return result;
}

const cellPointsTries = 30;
const COS30 = Math.cos(Math.PI / 6);
const cellPointsProj1 = new Vector3(Math.cos(Math.PI * 1 / 6), 0, Math.sin(Math.PI * 1 / 6));
const cellPointsProj2 = new Vector3(Math.cos(Math.PI * 3 / 6), 0, Math.sin(Math.PI * 3 / 6));
const cellPointsProj3 = new Vector3(Math.cos(Math.PI * 5 / 6), 0, Math.sin(Math.PI * 5 / 6));
function cellPointsDistanceSqr(a: Vector3, b: Vector3): number { const x = b.x - a.x; const z = b.z - a.z; return x * x + z * z; }
export function cellPoints(grid: HexGrid<HexCell>, pos: HexVector, size: number, density: number, result: Vector3[], seed = 0) {
	result.length = 0;
	if(density <= 0) { return; }
	density = density > 1 ? 1 : density;
	const mindist = size + size / density;
	const maxspread = mindist * 0.5;
	const mindistsqr = mindist * mindist;
	const maxprojrange = COS30 - size * 0.5;
	const context = Random.number.context(Random.hash.number2(pos.row, pos.col, 1) + seed);
	Transients.push(HexVector);
	const temp = Transients.get(HexVector);
	const candidate = Transients.get(Vector3);
	const initialangle = precalculatedAngles[Math.floor(Random.number.float(context) * numPrecalculatedAngles)];
	const initialdist = Random.number.float(context) * (COS30 - size * 2);
	pos.toVector3(candidate);
	const posproj1 = cellPointsProj1.dot(candidate);
	const posproj2 = cellPointsProj2.dot(candidate);
	const posproj3 = cellPointsProj3.dot(candidate);
	const initial = Transients.get(Vector3).set(initialangle[0], 0, initialangle[1]).multiplyScalar(initialdist).add(candidate);
	initial.y = getGridHeight(grid, temp.fromVector3(initial));
	result.push(initial);
	let processed = 0;
	while(processed < result.length) {
		const center = result[processed];
		for(let i = cellPointsTries; i >= 0; i--) {
			const angle = precalculatedAngles[Math.floor(Random.number.float(context) * numPrecalculatedAngles)];
			const dist = mindist + Random.number.float(context) * maxspread;
			candidate.set(angle[0], 0, angle[1]).multiplyScalar(dist).add(center);
			if(Math.abs(cellPointsProj1.dot(candidate) - posproj1) > maxprojrange) { continue; }
			if(Math.abs(cellPointsProj2.dot(candidate) - posproj2) > maxprojrange) { continue; }
			if(Math.abs(cellPointsProj3.dot(candidate) - posproj3) > maxprojrange) { continue; }
			let collides = false;
			for(let j = result.length - 1; j >= 0; j--) {
				if(j !== processed && cellPointsDistanceSqr(candidate, result[j]) < mindistsqr) {
					collides = true;
					break;
				}
			}
			if(collides) { continue; }
			temp.fromVector3(candidate);
			candidate.y = getGridHeight(grid, temp.fromVector3(candidate));
			result.push(Transients.get(Vector3).copy(candidate));
		}
		processed += 1;
	}
	Transients.pop(HexVector);
}

export function cellRotations(pos: HexVector, num: number, result: Quaternion[], seed = 0) {
	result.length = 0;
	const context = Random.number.context(Random.hash.number2(pos.row, pos.col, 2) + seed);
	Transients.push(Vector3);
	const up = Transients.get(Vector3).set(0, 1, 0);
	for(let i = 0; i < num; i++) {
		result.push(Transients.get(Quaternion).setFromAxisAngle(up, Random.number.float(context) * Math.PI * 2));
	}
	Transients.pop(Vector3);
}

export function cellOptions(pos: HexVector, num: number, options: number, result: number[], seed = 0) {
	result.length = 0;
	const context = Random.number.context(Random.hash.number2(pos.row, pos.col, 3) + seed);
	for(let i = 0; i < num; i++) {
		const x = Random.number.float(context);
		result.push(Math.floor(x * (options - EPSILON)));
	}
}

export function pointingLocation(grid: HexGrid<HexCell>, ray: Ray): HexVector | null {
	let pointingcell: HexVector | null = null;
	const result = Transients.get(HexVector);
	Transients.push(HexVector);
	Transients.push(Vector2);
	Transients.push(Vector3);

	if(ray && ray.direction.y < 0 && Math.abs(ray.direction.x) < EPSILON && Math.abs(ray.direction.z) < EPSILON) {
		pointingcell = result.fromVector2(Transients.get(Vector2).set(ray.origin.x, ray.origin.z));
	} else if(ray && ray.direction.y < 0) {
		const origin = Transients.get(Vector2).set(ray.origin.x, ray.origin.z);
		const direction = Transients.get(Vector2).set(ray.direction.x, ray.direction.z);
		const originheight = ray.origin.y;
		const heightspeed = ray.direction.y;

		const hexorigin = Transients.get(HexVector).fromVector2(origin);
		const hexdirection = Transients.get(HexVector).fromVector2(direction);
		let lastdist = -1;
		for(const [position, distance] of triangleRay(hexorigin, hexdirection)) {
			const cellheight = getGridHeight(grid, position);
			const rayheight: number = originheight + heightspeed * distance;

			if(rayheight <= cellheight) {
				const middlehex = Transients.get(HexVector);
				if(lastdist === -1) { break; }
				const middledist: number = (distance + lastdist) * 0.5;
				middlehex.copy(hexdirection).multiplyScalar(middledist).add(hexorigin);

				const point1 = Transients.get(HexVector);
				const point2 = Transients.get(HexVector);
				const point3 = Transients.get(HexVector);
				const bary = Transients.get(Vector3);
				getTrianglePoints(middlehex, point1, point2, point3);
				getTriangleBarycentrics(middlehex, bary);

				const point1cell = grid.get(point1);
				const point2cell = grid.get(point2);
				const point3cell = grid.get(point3);
				const point1height = point1cell ? point1cell.height : 0;
				const point2height = point2cell ? point2cell.height : 0;
				const point3height = point3cell ? point3cell.height : 0;
				const middleheight = bary.x * point1height + bary.y * point2height + bary.z * point3height;

				const islowhalf = point1.col <= point2.col && point1.row <= point2.row;
				const col_deriv = islowhalf ? point3height - point1height : point1height - point2height;
				const row_deriv = islowhalf ? point2height - point1height : point1height - point3height;
				const deriv = hexdirection.col * col_deriv + hexdirection.row * row_deriv;
				const intersection = (originheight - middleheight + deriv * middledist) / (deriv - heightspeed);

				pointingcell = result.copy(hexdirection).multiplyScalar(intersection).add(hexorigin);
				break;
			}

			lastdist = distance;
		}
	}

	Transients.pop(HexVector);
	Transients.pop(Vector2);
	Transients.pop(Vector3);
	return pointingcell;
}

export function groupOpen(grid: HexGrid<HexCell>, group_index: number): HexGrid<boolean> {
	Transients.push(HexVector);
	const result = new HexGrid<boolean>(HEX_GROUP_SIZE);
	const index_hex = Transients.get(HexVector).fromIndex(group_index).antituple(HEX_GROUP_SIZE);
	const hex = Transients.get(HexVector);

	for(let index = 0; index < HEX_GROUP_LENGTH; index++) {
		const value = grid.getByIndex(hex.fromIndex(index).add(index_hex).toIndex());
		result.setByIndex(index, value ? !value.obstacle : false);
	}

	Transients.pop(HexVector);
	return result;
}

export function groupSegments(cells_open: HexGrid<boolean>): HexGrid<number> {
	const result = new HexGrid<number>(HEX_GROUP_SIZE).fill(-1);
	const neighbor_segments: number[] = [];
	let next = 0;

	for(let index = 0; index < HEX_GROUP_LENGTH; index++) {
		const cell_open = cells_open.getByIndex(index);
		if(!cell_open) { continue; }

		for(const neighbor_index of hexNeighborIndexes(index)) {
			if(neighbor_index >= HEX_GROUP_LENGTH) { continue; }
			const neighbor_segment = result.getByIndex(neighbor_index);
			if(neighbor_segment !== undefined && neighbor_segment >= 0) { neighbor_segments.push(neighbor_segment); }
		}

		if(neighbor_segments.length === 0) {
			result.setByIndex(index, next);
			next += 1;
		} else {
			const segment = _.min(neighbor_segments);
			if(segment === undefined) { throw new Error(); }
			result.setByIndex(index, segment);
			for(let neighbor_segment_index = neighbor_segments.length; neighbor_segment_index >= 0; neighbor_segment_index--) {
				const neighbor_segment = neighbor_segments[neighbor_segment_index];
				if(neighbor_segment === segment) { continue; }
				for(let oldindex = 0; oldindex < HEX_GROUP_LENGTH; oldindex++) {
					if(result.getByIndex(oldindex) === neighbor_segment) { result.setByIndex(oldindex, segment); }
				}
			}
		}

		neighbor_segments.length = 0;
	}

	const finals: Map<number, number> = new Map<number, number>();
	next = 0;
	for(let index = 0; index < HEX_GROUP_LENGTH; index++) {
		let value = result.getByIndex(index);
		if(value === undefined || value < 0) { continue; }
		if(finals.has(value)) { value = finals.get(value); if(value === undefined) { throw new Error(); } } else { finals.set(value, next); value = next; next += 1; }
		result.setByIndex(index, value);
	}

	return result;
}

export function toConnection(group: number, segment: number): number {
	return group * HEX_GROUP_LENGTH + segment;
}

const fromConnectionArray: [number, number] = [0, 0];
export function fromConnection(connection: number): [number, number] {
	fromConnectionArray[0] = Math.floor(connection / HEX_GROUP_LENGTH);
	fromConnectionArray[1] = connection % HEX_GROUP_LENGTH;
	return fromConnectionArray;
}

export function groupConnections(cell_indexes: HexGrid<number>, neighbor_cell_segments: Array<HexGrid<number> | null>, neighbor_indexes: number[]): number[][] {
	Transients.push(HexVector);
	const result: number[][] = [];
	const radius = HEX_GROUP_SIZE - 1;
	const area = 3 * radius * (radius - 1) + 1;
	const hex = Transients.get(HexVector);
	const ringlength = HEX_GROUP_SIZE * radius;

	for(let pos = 0; pos < ringlength; pos++) {
		const index = area + pos;
		const value = cell_indexes.getByIndex(index);
		if(value === undefined || value < 0) { continue; }
		const segment = Math.floor(pos / radius);
		const segmentpos = pos - radius * segment + 1;

		let x: number, z: number;

		if(segment === 0) {
			x = radius;
			z = -radius + segmentpos;
		} else if(segment === 1) {
			x = radius - segmentpos;
			z = segmentpos;
		} else if(segment === 2) {
			x = -segmentpos;
			z = radius;
		} else if(segment === 3) {
			x = -radius;
			z = radius - segmentpos;
		} else if(segment === 4) {
			x = -radius + segmentpos;
			z = -segmentpos;
		} else {
			x = segmentpos;
			z = -radius;
		}

		for(let i = segmentpos === radius ? 2 : 1; i >= 0; i--) {
			const neighbor_group_index = i === 2 ? (segment + 1) % 6 : segment;
			const neighbor_group = neighbor_cell_segments[neighbor_group_index];
			if(!neighbor_group) { continue; }
			const neighbor_dir = HexDirections[(segment + i + 5) % 6];
			const neighbor_index = hex.set(z, x).add(neighbor_dir).wrap(radius).toIndex();
			const neighbor_segment = neighbor_group.getByIndex(neighbor_index);
			if(neighbor_segment !== undefined && neighbor_segment >= 0) {
				if(!result[value]) { result[value] = []; }
				const newconnection = toConnection(neighbor_indexes[neighbor_group_index], neighbor_segment);
				if(!result[value].includes(newconnection)) { result[value].push(newconnection); }
			}
		}
	}

	Transients.pop(HexVector);
	return result;
}

export function groupDistances(result: HexGrid<number[]>, groups: HexGrid<HexGroup>, start_group_index: number, start_group_segment: number) {
	let open: number[] = [toConnection(start_group_index, start_group_segment)];
	let nextopen: number[] = [];
	let distance = 0;
	while(true) {
		if(open.length === 0) { break; }
		for(let i = open.length - 1; i >= 0; i--) {
			const [group_index, segment_index] = fromConnection(open[i]);
			let group = result.getByIndex(group_index);
			if(group !== undefined && group[segment_index] !== undefined) { continue; }
			if(!group) { group = []; result.setByIndex(group_index, group); }
			group[segment_index] = distance;
			const connections = groups.getByIndex(group_index);
			const segment_connections = connections && connections.connections[segment_index];
			if(segment_connections) {
				for(let j = segment_connections.length; j >= 0; j--) {
					nextopen.push(segment_connections[j]);
				}
			}
		}
		distance += 1;
		[open, nextopen] = [nextopen, open];
		nextopen.length = 0;
	}
}

const segmentGradients: Array<HexGrid<number>> = [];
for(let group = 0; group < 7; group++) { segmentGradients[group] = new HexGrid<number>(HEX_GROUP_SIZE); }
function segmentGradient(result: HexGrid<number>, segment_distance: number, from_segment: number, to_groups: number[], to_segments: number[], to_cell: number, cell_segments: Array<HexGrid<number> | null>, group_indexes: number[]) {
	for(let group = 0; group < 7; group++) {
		segmentGradients[group].fill(-1);
	}

	let open: number[] = [];
	let nextopen: number[] = [];
	let distance = 0;

	if(to_cell !== -1) {
		open.push(toConnection(to_groups[0], to_cell));
	} else {
		for(let i = to_groups.length - 1; i >= 0; i--) {
			const to_group = to_groups[i];
			const to_segment = to_segments[i];
			const group_segments = cell_segments[to_group];
			if(!group_segments) { throw new Error(); }
			for(let index = 0; index < HEX_GROUP_LENGTH; index++) {
				if(group_segments.getByIndex(index) === to_segment) {
					open.push(toConnection(to_group, index));
				}
			}
		}
	}

	Transients.push(HexVector);
	const originhex = Transients.get(HexVector);
	const grouphex = Transients.get(HexVector);
	const cellhex = Transients.get(HexVector);
	const hex = Transients.get(HexVector);
	while(true) {
		if(open.length === 0) { break; }
		for(let i = open.length - 1; i >= 0; i--) {
			const [group_index, cell_index] = fromConnection(open[i]);
			const group_segments = cell_segments[group_index];
			if(!group_segments) { continue; }
			const cell_segment = group_segments.getByIndex(cell_index);
			if(cell_segment === undefined || cell_segment < 0) { continue; }
			const gradients = segmentGradients[group_index];
			const prev_distance = gradients.getByIndex(cell_index);
			if(prev_distance !== undefined && prev_distance >= 0) { continue; }
			gradients.setByIndex(cell_index, distance);

			originhex.fromIndex(cell_index);
			for(let direction = 0; direction < 6; direction++) {
				let neighbor_group: number, neighbor_index: number;
				hex.copy(originhex).add(HexDirections[direction]);
				if(hex.length() < HEX_GROUP_SIZE) {
					neighbor_group = group_index;
					neighbor_index = hex.toIndex();
				} else {
					neighbor_group = grouphex.fromIndex(group_index).add(cellhex.copy(hex).tuple(HEX_GROUP_SIZE).round()).toIndex();
					if(neighbor_group > 6) { continue; }
					neighbor_index = cellhex.antituple(HEX_GROUP_SIZE).sub(hex).multiplyScalar(-1).toIndex();
				}
				nextopen.push(toConnection(neighbor_group, neighbor_index));
			}
		}
		distance += 1;
		[open, nextopen] = [nextopen, open];
		nextopen.length = 0;
	}

	const segments = cell_segments[0];
	if(!segments) { throw new Error(); }
	for(let index = 0; index < HEX_GROUP_LENGTH; index++) {
		if(segments.getByIndex(index) === from_segment) {
			const gradient = segmentGradients[0].getByIndex(index);
			result.setByIndex(index, gradient !== undefined && gradient >= 0 ? segment_distance * 100000 + gradient : -1);
		}
	}
	Transients.pop(HexVector);
}

export function groupGradient(result: HexGrid<number>, to_hex: HexVector, group_index: number, groups: HexGrid<HexGroup>, distances: HexGrid<number[]>) {
	result.fill(-1);
	const group = groups.getByIndex(group_index);
	if(!group) { return; }
	const group_distances = distances.getByIndex(group_index);
	if(!group_distances) { return; }
	const [to_hex_group_index, to_hex_cell_index] = hexGroupIndex(to_hex.toIndex());
	const to_hex_group = groups.getByIndex(to_hex_group_index);
	if(!to_hex_group) { throw new Error('Invalid to_hex provided to groupGradient'); }
	const to_hex_segment = to_hex_group.segments.getByIndex(to_hex_cell_index);
	if(to_hex_segment === undefined) { throw new Error('Invalid to_hex provided to groupGradient'); }
	let to_hex_relative_group = -1;

	Transients.push(HexVector);
	const gradientsegments: Array<HexGrid<number> | null> = [];
	const gradientindexes: number[] = [];
	const origin = Transients.get(HexVector).fromIndex(group_index);
	const hex = Transients.get(HexVector);
	for(let index = 0; index < 7; index++) {
		const neighbor_index = hex.fromIndex(index).add(origin).toIndex();
		const neighbor = groups.getByIndex(neighbor_index);
		gradientsegments[index] = neighbor ? neighbor.segments : null;
		gradientindexes[index] = neighbor_index;
		if(neighbor_index === to_hex_group_index) { to_hex_relative_group = index; }
	}

	let min_dist: number;
	const to_groups: number[] = [];
	const to_segments: number[] = [];
	let target_found: boolean;
	let distance: number;
	for(let segment = group.connections.length - 1; segment >= 0; segment--) {
		min_dist = Infinity;
		to_groups.length = 0;
		to_segments.length = 0;
		target_found = false;
		distance = group_distances[segment];

		if(to_hex_group_index === group_index && to_hex_segment === segment) {
			target_found = true;
		} else {
			const connections = group.connections[segment];
			if(!connections) { continue; }

			for(let connection = connections.length - 1; connection >= 0; connection--) {
				const [connection_group_index, connection_segment] = fromConnection(connections[connection]);
				if(to_hex_group_index === connection_group_index && to_hex_segment === connection_segment) {
					to_groups.length = 0;
					to_segments.length = 0;
					target_found = true;
					break;
				} else {
					const connection_group_distances = distances.getByIndex(connection_group_index);
					if(!connection_group_distances) { continue; }
					const segment_distance = connection_group_distances[connection_segment];
					if(segment_distance < min_dist) {
						min_dist = segment_distance;
						to_groups.length = 0;
						to_segments.length = 0;
						to_groups.push(connection_group_index);
						to_segments.push(connection_segment);
					} else if(segment_distance === min_dist) {
						to_groups.push(connection_group_index);
						to_segments.push(connection_segment);
					}
				}
			}
		}

		if(target_found) {
			if(to_hex_relative_group === -1) { throw new Error(); }
			to_groups.push(to_hex_relative_group);
			segmentGradient(result, distance, segment, to_groups, to_segments, to_hex_cell_index, gradientsegments, gradientindexes);
		} else if(to_groups.length) {
			for(let to_group = to_groups.length - 1; to_group >= 0; to_group--) {
				to_groups[to_group] = gradientindexes.indexOf(to_groups[to_group]);
				if(to_groups[to_group] === -1) { throw new Error(); }
			}
			segmentGradient(result, distance, segment, to_groups, to_segments, -1, gradientsegments, gradientindexes);
		}
	}

	Transients.pop(HexVector);
}

function getClosestLocationsGrid(grids: HexGrid<HexGrid<number>>, segments: number[], group_index: number, segment_index: number): HexGrid<number> {
	let grid = grids.getByIndex(group_index);
	if(!grid) {
		grid = new HexGrid<number>(HEX_GROUP_SIZE);
		grids.setByIndex(group_index, grid);
	}
	const segment_id = toConnection(group_index, segment_index);
	if(!segments.includes(segment_id)) {
		segments.push(segment_id);
		grid.fill(-1);
	}
	return grid;
}

export function calculateClosestLocations(grid: HexGrid<HexCell>, groups: HexGrid<HexGroup>, distances: HexGrid<number[]>, target_segment_index: number, target_group_index: number, target_cell_index: number, close_segments: number[], close_gradients: HexGrid<HexGrid<number>>): void {
	close_segments.length = 0;

	const closestLocations: number[] = [];
	let open: number[] = [];
	let nextopen: number[] = [];
	let distance = 0;

	open.push(toConnection(target_group_index, target_cell_index));

	Transients.push(HexVector);
	const originhex = Transients.get(HexVector);
	const grouphex = Transients.get(HexVector);
	const cellhex = Transients.get(HexVector);
	const hex = Transients.get(HexVector);
	while(true) {
		if(open.length === 0) { break; }
		for(let i = open.length - 1; i >= 0; i--) {
			const [group_index, cell_index] = fromConnection(open[i]);
			const group = groups.getByIndex(group_index);
			if(!group) { continue; }
			const cell_segment = group.segments.getByIndex(cell_index);
			if(cell_segment === undefined || cell_segment < 0) { continue; }
			const gradients = getClosestLocationsGrid(close_gradients, close_segments, group_index, cell_segment);
			const prev_distance = gradients.getByIndex(cell_index);
			if(prev_distance !== undefined && prev_distance >= 0) { continue; }
			const cell = grid.getByIndex(grouphex.fromIndex(group_index).antituple(HEX_GROUP_SIZE).add(cellhex.fromIndex(cell_index)).toIndex());
			if(!cell) { continue; }
			const stoppedUnit = false;
			// for(let j = cell.occupiedBy.length - 1; j >= 0; j--) {
			// 	const unit = cell.occupiedBy[j];
			// 	if(!(unit.goal instanceof MoveGoal)) {
			// 		stoppedUnit = true;
			// 		break;
			// 	}
			// }
			if(!cell.obstacle && !stoppedUnit) { closestLocations.push(open[i]); continue; }
			gradients.setByIndex(cell_index, distance);

			originhex.fromIndex(cell_index);
			for(let direction = 0; direction < 6; direction++) {
				let neighbor_group: number, neighbor_index: number;
				hex.copy(originhex).add(HexDirections[direction]);
				if(hex.length() < HEX_GROUP_SIZE) {
					neighbor_group = group_index;
					neighbor_index = hex.toIndex();
				} else {
					neighbor_group = grouphex.fromIndex(group_index).add(cellhex.copy(hex).tuple(HEX_GROUP_SIZE).round()).toIndex();
					neighbor_index = cellhex.antituple(HEX_GROUP_SIZE).sub(hex).multiplyScalar(-1).toIndex();
				}
				nextopen.push(toConnection(neighbor_group, neighbor_index));
			}
		}
		if(closestLocations.length) { break; }
		distance += 1;
		[open, nextopen] = [nextopen, open];
		nextopen.length = 0;
	}

	open.length = 0;
	nextopen.length = 0;
	distance = 0;

	for(let i = closestLocations.length - 1; i >= 0; i--) {
		open.push(closestLocations[i]);
	}
	for(let i = close_segments.length - 1; i >= 0; i--) {
		const [group_index, cell_index] = fromConnection(close_segments[i]);
		_.identity(cell_index);
		const gradient = close_gradients.getByIndex(group_index);
		if(!gradient) { throw new Error(); }
		gradient.fill(-1);
	}

	while(true) {
		if(open.length === 0) { break; }
		for(let i = open.length - 1; i >= 0; i--) {
			const [group_index, cell_index] = fromConnection(open[i]);
			const group = groups.getByIndex(group_index);
			if(!group) { continue; }
			const cell_segment = group.segments.getByIndex(cell_index);
			if(cell_segment === undefined || cell_segment < 0) { continue; }
			if(!close_segments.includes(toConnection(group_index, cell_segment))) { continue; }
			const gradients = close_gradients.getByIndex(group_index);
			if(!gradients) { throw new Error(); }
			const prev_distance = gradients.getByIndex(cell_index);
			if(prev_distance !== undefined && prev_distance >= 0) { continue; }
			gradients.setByIndex(cell_index, distance);

			originhex.fromIndex(cell_index);
			for(let direction = 0; direction < 6; direction++) {
				let neighbor_group: number, neighbor_index: number;
				hex.copy(originhex).add(HexDirections[direction]);
				if(hex.length() < HEX_GROUP_SIZE) {
					neighbor_group = group_index;
					neighbor_index = hex.toIndex();
				} else {
					neighbor_group = grouphex.fromIndex(group_index).add(cellhex.copy(hex).tuple(HEX_GROUP_SIZE).round()).toIndex();
					neighbor_index = cellhex.antituple(HEX_GROUP_SIZE).sub(hex).multiplyScalar(-1).toIndex();
				}
				nextopen.push(toConnection(neighbor_group, neighbor_index));
			}
		}
		distance += 1;
		[open, nextopen] = [nextopen, open];
		nextopen.length = 0;
	}

	Transients.pop(HexVector);
}

export function pathfind(grid: HexGrid<HexCell>, start: HexVector, end: HexVector): HexVector[] | null {
	interface PathStep {index: number; pos: HexVector; priority: number; prev: PathStep | null; cost: number; }
	const index = end.toIndex();
	let options: PathStep[] = [{index, pos: end, priority: 0, prev: null, cost: 0}];
	const cost_so_far: {[n: number]: number} = {[index]: 0};
	Transients.push(HexVector);

	while(true) {
		const current = options.pop();
		if(!current) { break; }

		if(current.pos.equals(start)) {
			const path = [];
			let result: PathStep | null = current;
			while(result) {
				path.push(result.pos);
				result = result.prev;
			}
			return _.reverse(path.slice(1));
		}

		for(const neighbor of hexNeighbors(current.pos)) {
			const nindex = neighbor.toIndex();

			let first_straight_line = current;
			const line = [neighbor, current.pos];
			while(true) {
				const testing = first_straight_line.prev;
				if(!testing) { break; }
				line.push(testing.pos);
				if(!iterableEvery(zipIterables(line, hexLine(neighbor, testing.pos)), ([goodhex, testhex]) => goodhex.equals(testhex))) { break; }
				first_straight_line = testing;
			}
			const next_cost = first_straight_line.cost + neighbor.realDistanceTo(first_straight_line.pos);

			if(cost_so_far[nindex] !== undefined && cost_so_far[nindex] <= next_cost) { continue; }
			if(neighbor.length() > grid.size) { continue; }
			const ncell = grid.get(neighbor);
			if(!ncell) { continue; }
			const ncopy = Transients.get(HexVector).copy(neighbor);
			options.push({index: nindex, pos: ncopy, priority: next_cost + neighbor.realDistanceTo(start), prev: current, cost: next_cost});
			cost_so_far[nindex] = next_cost;
		}

		options = _.reverse(_.sortBy(options, 'priority'));
	}

	Transients.pop(HexVector);
	return null;
}

interface VisibilityItem {
	offset: HexVector;
	distance: number;
	dependencies: Array<{index: number, offset: HexVector, contribution_angle: number, contribution_width: number}>;
}

function precalculateVisibilityList(size: number): VisibilityItem[] {
	Transients.push(HexVector);
	Transients.push(Vector2);
	let data: Array<{ points: Vector2[], item: VisibilityItem }> = [];
	const tempvec2 = Transients.get(Vector2).set(0, 0);
	const relativePoints = _.range(0, Math.PI * 2, Math.PI / 3).map(angle => Transients.get(Vector2).set(1, 0).rotateAround(tempvec2, angle));
	const offsetvec = Transients.get(Vector2);
	for(const offset of hexFilledCircle(Transients.get(HexVector).set(0, 0), size)) {
		const offsetCopy = new HexVector().copy(offset);
		offsetCopy.toVector2(offsetvec);
		const points = relativePoints.map(point => Transients.get(Vector2).copy(point).add(offsetvec));
		const distances = points.map(point => point.length());
		const distance = offsetCopy.length() ? _.min(distances) as number : 0;
		data.push({
			points,
			item: {
				offset: offsetCopy,
				distance,
				dependencies: [],
			},
		});
	}
	data = _.sortBy(data, 'item.distance');
	for(const datum of data) {
		for(const [key, other] of data.entries()) {
			if(other.item.distance < datum.item.distance - EPSILON && other.item.offset.distanceTo(datum.item.offset) === 1) {
				const points = datum.points.filter(point => _.some(other.points, otherpoint => tempvec2.copy(point).sub(otherpoint).length() < EPSILON));
				const angles = points.map(point => Math.PI * 2 - point.angle()).sort();
				const [angle1, angle2] = angles;
				const gap = angle2 >= angle1 - EPSILON ? angle2 - angle1 : angle2 + Math.PI * 2 - angle1;
				const contribution_angle = gap > Math.PI ? angle2 : angle1;
				const contribution_width = gap > Math.PI ? Math.PI * 2 - gap : gap;
				datum.item.dependencies.push({index: key - 1, offset: other.item.offset, contribution_angle, contribution_width});
			}
		}
	}
	Transients.pop(HexVector);
	Transients.pop(Vector2);
	return _.map(data.slice(1), 'item');
}

interface VisibilityCoverage {
	angle: number;
	width: number;
}

let visibilityList: VisibilityItem[];
let coverage: VisibilityCoverage[];
export function precalculateGrid() {
	visibilityList = precalculateVisibilityList(20);
	coverage = new Array(visibilityList.length).fill(null).map(() => ({angle: 0, width: 0}));
}

export function visibility(grid: HexGrid<HexCell>, position: HexVector, sight_distance: number, result: HexVector[]): void {
	result.length = 0;
	result.push(position);
	const temp = Transients.get(HexVector);

	for(let item_index = 0, visibility_list_length = visibilityList.length; item_index < visibility_list_length; item_index++) {
		const item = visibilityList[item_index];
		if(item.distance > sight_distance) { break; }

		temp.copy(position).add(item.offset);
		const cell = grid.get(temp);
		if(!cell) {
			coverage[item_index].angle = 0;
			coverage[item_index].width = 0;
			continue;
		}

		let total_angle = 0;
		let total_width = 0;

		for(let dependency_index = 0, dependencies_length = item.dependencies.length; dependency_index < dependencies_length; dependency_index++) {
			const {index, offset, contribution_angle, contribution_width} = item.dependencies[dependency_index];
			const dependency_cell = grid.get(temp.copy(position).add(offset));
			if(!dependency_cell) { continue; }

			let add_angle, add_width;

			if(index === -1) {
				add_angle = contribution_angle;
				add_width = contribution_width;
			} else {
				const {angle: dependency_angle, width: dependency_width} = coverage[index];

				if(dependency_width === 0) {
					add_angle = 0;
					add_width = 0;
				} else {
					const difference = dependency_angle - contribution_angle;
					const distance = Math.abs(difference) > Math.PI ? difference - Math.sign(difference) * Math.PI * 2 : difference;

					const max = Math.min(contribution_width, dependency_width + distance);
					const min = Math.max(0, distance);

					add_angle = contribution_angle + min;
					add_width = max - min;
				}
			}

			if(add_width > EPSILON) {
				if(add_angle < 0) { add_angle += Math.PI * 2; }
				if(add_angle > Math.PI * 2) { add_angle -= Math.PI * 2; }

				if(total_width === 0) {
					total_angle = add_angle;
					total_width = add_width;
				} else {
					const difference = add_angle - total_angle;
					const distance = Math.abs(difference) > Math.PI ? difference - Math.sign(difference) * Math.PI * 2 : difference;

					const max = Math.max(total_width, add_width + distance);
					const min = Math.min(0, distance);

					total_angle += min;
					total_width = max - min;

					if(total_angle < 0) { total_angle += Math.PI * 2; }
					if(total_angle > Math.PI * 2) { total_angle -= Math.PI * 2; }
				}
			}
		}

		if(total_width > 0) { result.push(Transients.get(HexVector).copy(position).add(item.offset)); }

		if(cell.obstacle) {
			total_angle = 0;
			total_width = 0;
		}

		coverage[item_index].angle = total_angle;
		coverage[item_index].width = total_width;
	}
}
