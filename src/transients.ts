class TransientManager<T> {
	private instances: T[];
	private counter = 0;
	private stack: number[] = [];

	constructor(private klass: {new (): T, name: string}, amount: number) {
		this.instances = new Array<T>(amount);
		for(let i = 0; i < amount; i++) {
			this.instances[i] = new klass();
		}
	}

	public next(): T {
		if(this.counter === this.instances.length) { throw new Error(`Ran out of transients for class "${this.klass.name}"`); }
		this.counter += 1;
		return this.instances[this.counter - 1];
	}

	public push(): void {
		this.stack.push(this.counter);
	}

	public pop(): void {
		if(!this.stack.length) { throw new Error(`Transient stack for class "${this.klass.name}" popped too many times!`); }
		this.counter = this.stack.pop() || 0;
	}

	public reset(): void {
		this.counter = 0;
		if(this.stack.length) { throw new Error(`Transient stack for class "${this.klass.name}" not popped properly!`); }
	}
}

class Transients {
	private klasses: Map<new () => unknown, TransientManager<unknown>> = new Map<new () => unknown, TransientManager<unknown>>();

	public register<T>(klass: {new (): T, name: string}, amount: number): void {
		this.klasses.set(klass, new TransientManager<T>(klass, amount));
	}

	public get<T>(klass: {new (): T, name: string}): T {
		const manager = this.klasses.get(klass);
		if(!manager) { throw new Error(`Unknown class "${klass.name}" provided to Transients`); }
		return manager.next() as T;
	}

	public push<T>(klass: {new (): T, name: string}): void {
		const manager = this.klasses.get(klass);
		if(!manager) { throw new Error(`Unknown class "${klass.name}" provided to Transients`); }
		return manager.push();
	}

	public pop<T>(klass: {new (): T, name: string}): void {
		const manager = this.klasses.get(klass);
		if(!manager) { throw new Error(`Unknown class "${klass.name}" provided to Transients`); }
		return manager.pop();
	}

	public reset(): void {
		this.klasses.forEach(manager => manager.reset());
	}
}

export default new Transients();
