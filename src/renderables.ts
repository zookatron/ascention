import { Vector2, Vector3, Vector4, Quaternion, Euler, Color, Object3D, Group, Sprite, Mesh, MeshStandardMaterial,
	InstancedBufferGeometry, InstancedBufferAttribute, BufferGeometry, BufferAttribute, ShaderMaterial, RGBADepthPacking,
	BackSide, FrontSide, UniformsUtils, UniformsLib, Texture, DynamicDrawUsage, IUniform } from 'three';
import { ShaderAttributeType, ShaderAttributeValue, ShaderIncludes } from './shaders';

export enum ModelType {
	None,
	HealthBar,
	UnitSelector,
	Tank,
	Rock,
	Rock2,
	Tree,
	Bush,
	MainBase,
	Factory,
	Miner,
}

export interface ModelSpec {
	url: string;
	scale: number;
	offset: Vector3;
	rotation: Quaternion;
}

// tslint:disable-next-line:variable-name
export const ModelSpecs: { [type: number]: ModelSpec } = {
	[ModelType.Tank]: {
		url: 'assets/models/tank/tank.gltf',
		scale: 0.1,
		offset: new Vector3(0, 0, 0),
		rotation: new Quaternion().setFromEuler(new Euler(0, 0, 0)),
	},
	[ModelType.Rock]: {
		url: 'assets/models/rock/rock.gltf',
		scale: 0.2,
		offset: new Vector3(0, -0.15, 0),
		rotation: new Quaternion().setFromEuler(new Euler(0, 0, 0)),
	},
	[ModelType.Rock2]: {
		url: 'assets/models/rock2/rock2.gltf',
		scale: 0.4,
		offset: new Vector3(0, -0.15, 0),
		rotation: new Quaternion().setFromEuler(new Euler(0, 0, 0)),
	},
	[ModelType.Tree]: {
		url: 'assets/models/tree/tree.gltf',
		scale: 0.2,
		offset: new Vector3(0, 0, 0),
		rotation: new Quaternion().setFromEuler(new Euler(0, 0, 0)),
	},
	[ModelType.Bush]: {
		url: 'assets/models/bush/bush.gltf',
		scale: 0.07,
		offset: new Vector3(0, 0, 0),
		rotation: new Quaternion().setFromEuler(new Euler(0, 0, 0)),
	},
	[ModelType.MainBase]: {
		url: 'assets/models/mainbase/mainbase.gltf',
		scale: 2.0,
		offset: new Vector3(0, 0.1, 0),
		rotation: new Quaternion().setFromEuler(new Euler(0, 0, 0)),
	},
	[ModelType.Factory]: {
		url: 'assets/models/factory/factory.gltf',
		scale: 2.0,
		offset: new Vector3(0, 0.1, 0),
		rotation: new Quaternion().setFromEuler(new Euler(0, Math.PI, 0)),
	},
	[ModelType.Miner]: {
		url: 'assets/models/miner/miner.gltf',
		scale: 0.85,
		offset: new Vector3(0, 0, 0),
		rotation: new Quaternion().setFromEuler(new Euler(0, Math.PI / 6, 0)),
	},
};

export type RenderableID = number;

export class Renderable {
	public id!: number;

	protected vertexShader = `
		#include <std_vert_parameters>
		void main() {
			#include <std_vert_main>
		}
	`;
	protected fragmentShader = `
		#include <std_frag_parameters>
		void main() {
			#include <std_frag_main>
		}
	`;
	protected depthVertexShader = `
		#include <std_depth_vert_parameters>
		void main() {
			#include <std_depth_vert_main>
		}
	`;
	protected depthFragmentShader = `
		#include <std_depth_frag_parameters>
		void main() {
			#include <std_depth_frag_main>
		}
	`;

	static material: ShaderMaterial;
	static depthMaterial: ShaderMaterial;
	private mesh!: Mesh;
	private geometry!: InstancedBufferGeometry;
	private attribute_names: string[] = [];
	private attribute_types: ShaderAttributeType[] = [];
	private attributes: InstancedBufferAttribute[] = [];
	private max_instances = 0;
	private counter = 0;
	private preserve = false;
	private updated = true;

	constructor() { /* Nothing here */ }

	public render(...attributes: ShaderAttributeValue[]): void {
		const counter = this.counter;
		if(counter === this.max_instances) { return; }
		for(let index = 0, max = this.attributes.length; index < max; index++) {
			const array = this.attributes[index].array as number[];
			const attribute_type = this.attribute_types[index];
			if(attribute_type === Vector2) {
				const vector2 = attributes[index] as Vector2;
				array[counter * 2 + 0] = vector2.x;
				array[counter * 2 + 1] = vector2.y;
			} else if(attribute_type === Vector3) {
				const vector3 = attributes[index] as Vector3;
				array[counter * 3 + 0] = vector3.x;
				array[counter * 3 + 1] = vector3.y;
				array[counter * 3 + 2] = vector3.z;
			} else if(attribute_type === Vector4) {
				const vector4 = attributes[index] as Vector4;
				array[counter * 4 + 0] = vector4.x;
				array[counter * 4 + 1] = vector4.y;
				array[counter * 4 + 2] = vector4.z;
				array[counter * 4 + 3] = vector4.w;
			} else if(attribute_type === Quaternion) {
				const quaternion = attributes[index] as Quaternion;
				array[counter * 4 + 0] = quaternion.x;
				array[counter * 4 + 1] = quaternion.y;
				array[counter * 4 + 2] = quaternion.z;
				array[counter * 4 + 3] = quaternion.w;
			} else if(attribute_type === Color) {
				const color = attributes[index] as Color;
				array[counter * 3 + 0] = color.r;
				array[counter * 3 + 1] = color.g;
				array[counter * 3 + 2] = color.b;
			} else if(attribute_type === Number) {
				array[counter] = attributes[index] as number;
			} else {
				throw new Error(`Invalid renderable attribute type for attribute ${index}`);
			}
		}
		this.counter += 1;
		this.updated = true;
	}

	public clear() {
		this.counter = 0;
		this.updated = true;
	}

	public update(): void {
		if(this.preserve && !this.updated) {
			return;
		}
		if(this.counter <= 0) { this.mesh.visible = false; return; }
		this.mesh.visible = true;
		this.geometry.instanceCount = this.counter;
		this.counter = 0;
		for(let index = this.attributes.length - 1; index >= 0; index--) {
			this.attributes[index].needsUpdate = true;
		}
		this.updated = false;
	}

	public clone(): Renderable {
		const renderable: Renderable = new Renderable();

		renderable.id = this.id;
		renderable.vertexShader = this.vertexShader;
		renderable.fragmentShader = this.fragmentShader;
		renderable.depthVertexShader = this.depthVertexShader;
		renderable.depthFragmentShader = this.depthFragmentShader;
		renderable.mesh = this.mesh.clone();
		renderable.mesh.customDepthMaterial = this.mesh.customDepthMaterial;
		const geometry = renderable.mesh.geometry.clone() as InstancedBufferGeometry;
		for(let index = 0, max = this.attribute_names.length; index < max; index++) {
			renderable.attributes.push(geometry.attributes[this.attribute_names[index]] as InstancedBufferAttribute);
		}
		renderable.mesh.geometry = geometry;
		renderable.geometry = geometry;
		renderable.attribute_names = this.attribute_names;
		renderable.attribute_types = this.attribute_types;
		renderable.max_instances = this.max_instances;
		renderable.preserve = this.preserve;
		renderable.updated = this.updated;

		return renderable;
	}

	public getObject(): Object3D {
		return this.mesh;
	}

	public uniform(name: string, value: unknown) {
		const uniform = (this.mesh.material as ShaderMaterial).uniforms[name];
		if(uniform.value instanceof Vector2) {
			uniform.value.copy(value as Vector2);
		} else if(uniform.value instanceof Vector3) {
			uniform.value.copy(value as Vector3);
		} else if(uniform.value instanceof Vector4) {
			uniform.value.copy(value as Vector4);
		} else if(uniform.value instanceof Quaternion) {
			uniform.value.copy(value as Quaternion);
		} else if(uniform.value instanceof Color) {
			uniform.value.copy(value as Color);
		} else {
			uniform.value = value;
		}
	}

	protected initialize(
		// eslint-disable-next-line @typescript-eslint/no-explicit-any
		klass: {new (...arg: any[]): Renderable, material: ShaderMaterial, depthMaterial: ShaderMaterial},
		geometry: BufferGeometry,
		max_instances: number,
		attributes: Array<{ [key: string]: ShaderAttributeType }>,
		uniforms: { [key: string]: unknown } = {},
		material_props: { [key: string]: unknown } = {},
		mesh_props: { [key: string]: unknown } = {},
		preserve = false,
	) {

		if(!klass.material) { this.buildMaterials(klass); }

		this.preserve = preserve;
		this.max_instances = max_instances;
		const material = klass.material.clone();
		const depthMaterial = mesh_props.castShadow ? klass.depthMaterial?.clone() : null;
		if(depthMaterial) { (depthMaterial as {depthPacking?: unknown}).depthPacking = RGBADepthPacking; }

		for(const key in uniforms) { material.uniforms[key] = { value: uniforms[key] }; }
		for(const key in material_props) { (material as unknown as Record<string, unknown>)[key] = material_props[key]; }
		material.extensions.derivatives = true;
		material.extensions.shaderTextureLOD = true;

		this.geometry = new InstancedBufferGeometry();
		this.geometry.instanceCount = 0;

		this.geometry.setIndex(geometry.index?.clone() || null);
		for(const attribute_name in geometry.attributes) {
			const attribute = geometry.attributes[attribute_name];
			if(!(attribute instanceof BufferAttribute)) { throw new Error(`Invalid attribute "${attribute_name}" on geometry`); }
			this.geometry.setAttribute(attribute_name, attribute.clone());
		}

		for(let index = 0, max = attributes.length; index < max; index++) {
			const attribute_name = Object.keys(attributes[index])[0];
			const attribute_type = attributes[index][attribute_name];
			this.attribute_names.push(attribute_name);
			this.attribute_types.push(attribute_type);

			let length: number;
			/* tslint:disable:one-line */
			if(attribute_type === Vector2) { length = 2; }
			else if(attribute_type === Vector3) { length = 3; }
			else if(attribute_type === Vector4) { length = 4; }
			else if(attribute_type === Quaternion) { length = 4; }
			else if(attribute_type === Color) { length = 3; }
			else if(attribute_type === Number) { length = 1; }
			else { throw new Error(`Invalid renderable attribute type for "${attribute_name}"`); }
			/* tslint:enable:one-line */

			const attribute = new InstancedBufferAttribute(new Float32Array(this.max_instances * length), length);
			attribute.setUsage(DynamicDrawUsage);
			this.geometry.setAttribute(attribute_name, attribute);
			this.attributes.push(attribute);
		}

		this.mesh = new Mesh(this.geometry, material);
		for(const key in mesh_props) { (this.mesh as unknown as Record<string, unknown>)[key] = mesh_props[key]; }
		this.mesh.frustumCulled = false;
		if(depthMaterial) { this.mesh.customDepthMaterial = depthMaterial; }
	}

	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	private buildMaterials(klass: {new (...arg: any[]): Renderable, material: ShaderMaterial, depthMaterial: ShaderMaterial}): void {
		klass.material = new ShaderMaterial({
			uniforms: UniformsUtils.merge([
				UniformsLib.common,
				UniformsLib.envmap,
				UniformsLib.aomap,
				UniformsLib.lightmap,
				UniformsLib.emissivemap,
				UniformsLib.bumpmap,
				UniformsLib.normalmap,
				UniformsLib.displacementmap,
				UniformsLib.roughnessmap,
				UniformsLib.metalnessmap,
				UniformsLib.fog,
				UniformsLib.lights,
				{
					emissive: { value: new Color( 0x000000 ) },
					roughness: { value: 1 },
					metalness: { value: 0 },
					reflectivity: { value: 0 },
					opacity: { value: 1 },
					diffuse: { value: new Color(0xffffff) },
					envMapIntensity: { value: 1 },
				},
			]) as { [uniform: string]: IUniform },
			vertexShader: this.parseIncludes(this.vertexShader),
			fragmentShader: this.parseIncludes(this.fragmentShader),
			lights: true,
			side: FrontSide,
			transparent: true,
		});

		klass.depthMaterial = new ShaderMaterial({
			uniforms: UniformsUtils.merge([
				UniformsLib.common,
				UniformsLib.displacementmap,
			]) as { [uniform: string]: IUniform },
			vertexShader: this.parseIncludes(this.depthVertexShader),
			fragmentShader: this.parseIncludes(this.depthFragmentShader),
			side: BackSide,
		});
	}

	private parseIncludes(str: string): string {
		const pattern = /^[ \t]*#include +<([\w\d.]+)>/gm;
		return str.replace(pattern, (match: string, include: string) => {
			const replace = ShaderIncludes[include];
			if(replace === undefined) { return match; }
			return this.parseIncludes(replace);
		});
	}
}

export class MeshRenderable extends Renderable {
	protected vertexShader = `
		attribute vec3 offset;
		attribute vec4 rotation;
		attribute vec3 scale;
		attribute vec3 tint;
		varying vec3 vtint;
		#include <std_vert_parameters>
		void main() {
			#include <std_vert_color>
			vtint = tint;
			#include <std_vert_normal>
			objectNormal = applyQuaternion(objectNormal, rotation);
			#include <std_vert_position>
			transformed = offset + applyQuaternion(transformed * scale, rotation);
			#include <std_vert_extras>
		}
	`;

	protected fragmentShader = `
		varying vec3 vtint;
		#include <std_frag_parameters>
		void main() {
			#include <std_frag_color>
			diffuseColor.rgb *= vtint;
			#include <std_frag_extras>
		}
	`;

	protected depthVertexShader = `
		attribute vec3 offset;
		attribute vec4 rotation;
		attribute vec3 scale;
		#include <std_depth_vert_parameters>
		void main() {
			#include <std_depth_vert_position>
			transformed = offset + applyQuaternion(transformed * scale, rotation);
			#include <std_depth_vert_extras>
		}
	`;

	constructor(mesh: Mesh, max_instances: number) {
		super();

		if(!(mesh.material instanceof MeshStandardMaterial)) { throw new Error('Mesh provided to MeshRenderable must have MeshStandardMaterial'); }
		if(!(mesh.geometry instanceof BufferGeometry)) { throw new Error('Mesh provided to MeshRenderable must have BufferGeometry'); }

		this.initialize(MeshRenderable, mesh.geometry, max_instances, [
			{ offset: Vector3 },
			{ rotation: Quaternion },
			{ scale: Vector3 },
			{ tint: Color },
			{ texScale: Vector2 },
		], {
			map: mesh.material.map,
			diffuse: mesh.material.color,
			emissive: mesh.material.emissive,
			roughness: mesh.material.roughness,
			metalness: mesh.material.metalness,
			opacity: mesh.material.opacity,
		}, {
			transparent: mesh.material.transparent,
			depthWrite: mesh.material.depthWrite,
			side: mesh.material.side,
			map: mesh.material.map,
		}, {
			castShadow: mesh.castShadow,
			receiveShadow: mesh.receiveShadow,
		});
	}
}

export class SpriteRenderable extends Renderable {
	private sprites: Sprite[];
	private group: Group;
	private my_counter = 0;

	constructor(sprite: Sprite, private my_max_instances: number) {
		super();

		this.sprites = new Array<Sprite>(my_max_instances);
		this.group = new Group();

		for(let i = my_max_instances - 1; i >= 0; i--) {
			const newSprite = sprite.clone();
			newSprite.material = sprite.material.clone();
			newSprite.material.map = sprite.material.map?.clone() || null;
			if(newSprite.material.map) {
				newSprite.material.map.needsUpdate = true;
			}
			this.sprites[i] = newSprite;
			this.group.add(newSprite);
		}
	}

	public render(position: Vector3, rotation: Quaternion, scale: Vector3, color: Color, texScale: Vector2): void {
		if(this.my_counter === this.my_max_instances) { return; }
		const sprite = this.sprites[this.my_counter];
		sprite.visible = true;
		sprite.position.copy(position);
		sprite.rotation.setFromQuaternion(rotation);
		sprite.scale.copy(scale);
		sprite.material.color.copy(color);
		sprite.material.map?.repeat.copy(texScale);
		this.my_counter += 1;
	}

	public update(): void {
		for(let i = this.my_max_instances - 1; i >= this.my_counter; i--) {
			this.sprites[i].visible = false;
		}
		this.my_counter = 0;
	}

	public getObject(): Object3D {
		return this.group;
	}
}

export class ModelNode {
	public renderable: Renderable | null = null;
	public position: Vector3 = new Vector3();
	public rotation: Quaternion = new Quaternion();
	public scale: Vector3 = new Vector3(1, 1, 1);
	public color: Color = new Color();
	public texScale: Vector2 = new Vector2(1, 1);
	public absolutePosition: Vector3 = new Vector3();
	public absoluteRotation: Quaternion = new Quaternion();
	public absoluteScale: Vector3 = new Vector3(1, 1, 1);
	public absoluteColor: Color = new Color();
	public absoluteTexScale: Vector2 = new Vector2(1, 1);
	public children: { [name: string]: ModelNode } = {};

	public copy(other: ModelNode, cloneRenderables = false): ModelNode {
		this.renderable = cloneRenderables && other.renderable ? other.renderable.clone() : other.renderable;
		this.position.copy(other.position);
		this.rotation.copy(other.rotation);
		this.scale.copy(other.scale);
		this.color.copy(other.color);
		this.texScale.copy(other.texScale);
		this.children = {};
		for(const child in other.children) {
			this.children[child] = new ModelNode();
			this.children[child].copy(other.children[child], cloneRenderables);
		}
		return this;
	}

	public clone(cloneRenderables = false): ModelNode {
		return new ModelNode().copy(this, cloneRenderables);
	}

	public render(parentPosition: Vector3, parentRotation: Quaternion, parentScale: Vector3, parentColor: Color, parentTexScale: Vector2): void {
		this.absolutePosition.copy(parentPosition).add(this.position);
		this.absoluteRotation.copy(parentRotation).multiply(this.rotation);
		this.absoluteScale.copy(parentScale).multiply(this.scale);
		this.absoluteColor.copy(parentColor).multiply(this.color);
		this.absoluteTexScale.copy(parentTexScale).multiply(this.texScale);
		if(this.renderable) { this.renderable.render(this.absolutePosition, this.absoluteRotation, this.absoluteScale, this.absoluteColor, this.absoluteTexScale); }
		for(const childname in this.children) {
			this.children[childname].render(this.absolutePosition, this.absoluteRotation, this.absoluteScale, this.absoluteColor, this.absoluteTexScale);
		}
	}

	public fromObject3D(other: Object3D): void {
		this.position.copy(other.position);
		this.rotation.setFromEuler(other.rotation);
		this.scale.copy(other.scale);
	}
}

export class Model {
	public type!: ModelType;
	public position: Vector3 = new Vector3();
	public rotation: Quaternion = new Quaternion();
	public scale: Vector3 = new Vector3(1, 1, 1);
	public color: Color = new Color();
	public texScale: Vector2 = new Vector2(1, 1);
	public children: { [name: string]: ModelNode } = {};

	public copy(other: Model, cloneRenderables = false): Model {
		this.type = other.type;
		this.position.copy(other.position);
		this.rotation.copy(other.rotation);
		this.scale.copy(other.scale);
		this.color.copy(other.color);
		this.children = {};
		for(const childname in other.children) {
			this.children[childname] = new ModelNode();
			this.children[childname].copy(other.children[childname], cloneRenderables);
		}
		return this;
	}

	public clone(cloneRenderables = false): Model {
		return new Model().copy(this, cloneRenderables);
	}

	public traverse(func: (child: ModelNode) => unknown): void {
		const stack: Array<Model | ModelNode> = [this];

		while(true) {
			const next = stack.pop();
			if(!next) { break; }

			for(const childname in next.children) {
				const child = next.children[childname];
				func(child);
				stack.push(child);
			}
		}
	}

	public *getRenderables(): IterableIterator<Renderable> {
		const stack: Array<Model | ModelNode> = [this];

		while(true) {
			const next = stack.pop();
			if(!next) { break; }

			for(const childname in next.children) {
				const child = next.children[childname];
				if(child.renderable) { yield child.renderable; }
				stack.push(child);
			}
		}
	}

	public render(): void {
		for(const childname in this.children) {
			this.children[childname].render(this.position, this.rotation, this.scale, this.color, this.texScale);
		}
	}

	public fromObject3D(other: Object3D): void {
		this.position.copy(other.position);
		this.rotation.setFromEuler(other.rotation);
		this.scale.copy(other.scale);
	}
}

export class HexagonRenderable extends Renderable {
	protected vertexShader = `
		attribute vec3 offset;
		attribute vec3 tint;
		uniform vec2 rotation;
		uniform float scale;
		varying vec3 vtint;
		#include <std_vert_parameters>

		vec3 applyRotation2D(vec3 pos, vec2 rot) {
			vec2 rotated = pos.x * rot + pos.z * vec2(-rot.y, rot.x);
			return vec3(rotated.x, pos.y, rotated.y);
		}

		void main() {
			#include <std_vert_color>
			#include <std_vert_normal>
			#include <std_vert_position>
			transformed = offset + applyRotation2D(transformed * scale, rotation);
			vtint = tint + 0.25;
			#include <std_vert_extras>
		}
	`;

	protected fragmentShader = `
		varying vec3 vtint;
		#include <std_frag_parameters>
		void main() {
			#include <std_frag_color>
			gl_FragColor = diffuseColor * vec4(vtint, 1.0);
		}
	`;

	constructor(texture: Texture, scale: number, rotation: number, max_instances: number) {
		super();
		this.initialize(HexagonRenderable, hex_geometry, max_instances, [{ offset: Vector3 }, { tint: Color }], {
			map: texture,
			scale,
			rotation: new Vector2(1, 0).rotateAround(new Vector2(0, 0), rotation),
		}, {map: texture}, {});
	}
}

export class TiltedHexagonRenderable extends Renderable {
	protected vertexShader = `
		attribute vec3 offset;
		attribute vec3 tint;
		uniform vec2 rotation;
		uniform float scale;
		varying vec3 vtint;
		#include <std_vert_parameters>

		vec3 applyRotation2D(vec3 pos, vec2 rot) {
			vec2 rotated = pos.x * rot + pos.z * vec2(-rot.y, rot.x);
			return vec3(rotated.x, pos.y, rotated.y);
		}

		void main() {
			#include <std_vert_color>
			#include <std_vert_normal>
			#include <std_vert_position>
			transformed = offset + applyRotation2D(transformed * scale, rotation);
			vtint = tint;
			#include <std_vert_extras>
		}
	`;

	protected fragmentShader = `
		varying vec3 vtint;
		#include <std_frag_parameters>
		void main() {
			#include <std_frag_color>
			gl_FragColor = diffuseColor * vec4(vtint, 1.0);
		}
	`;

	constructor(texture: Texture, scale: number, rotation: number, max_instances: number) {
		super();
		this.initialize(TiltedHexagonRenderable, tilted_hex_geometry, max_instances, [{ offset: Vector3 }, { tint: Color }], {
			map: texture,
			scale,
			rotation: new Vector2(1, 0).rotateAround(new Vector2(0, 0), rotation),
		}, {map: texture}, {});
	}
}

function HexagonGeometry(scale = 1): BufferGeometry {
	const positions = new Float32Array(3 * 6);
	const uvs = new Float32Array(2 * 6);
	const normals = new Float32Array(3 * 6);
	const indexes = new Array<number>(3 * 4);

	for(let vertex = 0; vertex < 6; vertex++) {
		positions[vertex * 3 + 0] = Math.cos(Math.PI / 3 * vertex) * scale;
		positions[vertex * 3 + 1] = 0;
		positions[vertex * 3 + 2] = Math.sin(Math.PI / 3 * vertex) * scale;

		uvs[vertex * 2 + 0] = 0.5 + Math.cos(Math.PI / 3 * vertex) * 0.5;
		uvs[vertex * 2 + 1] = 0.5 + Math.sin(Math.PI / 3 * vertex) * 0.5;

		normals[vertex * 3 + 0] = 0;
		normals[vertex * 3 + 1] = 1;
		normals[vertex * 3 + 2] = 0;
	}

	indexes[0 * 3 + 0] = 0;
	indexes[0 * 3 + 1] = 1;
	indexes[0 * 3 + 2] = 2;

	indexes[1 * 3 + 0] = 0;
	indexes[1 * 3 + 1] = 2;
	indexes[1 * 3 + 2] = 3;

	indexes[2 * 3 + 0] = 0;
	indexes[2 * 3 + 1] = 3;
	indexes[2 * 3 + 2] = 5;

	indexes[3 * 3 + 0] = 5;
	indexes[3 * 3 + 1] = 3;
	indexes[3 * 3 + 2] = 4;

	const geometry = new BufferGeometry();
	geometry.setIndex(indexes);
	geometry.setAttribute('position', new BufferAttribute(positions, 3));
	geometry.setAttribute('uv', new BufferAttribute(uvs, 2));
	geometry.setAttribute('normal', new BufferAttribute(normals, 3));

	return geometry;
}

const hex_geometry = HexagonGeometry(1);

function TiltedHexagonGeometry(scale = 1): BufferGeometry {
	const positions = new Float32Array(3 * 6);
	const uvs = new Float32Array(2 * 6);
	const normals = new Float32Array(3 * 6);
	const indexes = new Array<number>(3 * 4);

	for(let vertex = 0; vertex < 6; vertex++) {
		positions[vertex * 3 + 0] = Math.cos(Math.PI / 6 + Math.PI / 3 * vertex) * scale;
		positions[vertex * 3 + 1] = 0;
		positions[vertex * 3 + 2] = Math.sin(Math.PI / 6 + Math.PI / 3 * vertex) * scale;

		uvs[vertex * 2 + 0] = 0.5 + Math.cos(Math.PI / 6 + Math.PI / 3 * vertex) * 0.5;
		uvs[vertex * 2 + 1] = 0.5 + Math.sin(Math.PI / 6 + Math.PI / 3 * vertex) * 0.5;

		normals[vertex * 3 + 0] = 0;
		normals[vertex * 3 + 1] = 1;
		normals[vertex * 3 + 2] = 0;
	}

	indexes[0 * 3 + 0] = 0;
	indexes[0 * 3 + 1] = 1;
	indexes[0 * 3 + 2] = 2;

	indexes[1 * 3 + 0] = 0;
	indexes[1 * 3 + 1] = 2;
	indexes[1 * 3 + 2] = 3;

	indexes[2 * 3 + 0] = 0;
	indexes[2 * 3 + 1] = 3;
	indexes[2 * 3 + 2] = 5;

	indexes[3 * 3 + 0] = 5;
	indexes[3 * 3 + 1] = 3;
	indexes[3 * 3 + 2] = 4;

	const geometry = new BufferGeometry();
	geometry.setIndex(indexes);
	geometry.setAttribute('position', new BufferAttribute(positions, 3));
	geometry.setAttribute('uv', new BufferAttribute(uvs, 2));
	geometry.setAttribute('normal', new BufferAttribute(normals, 3));

	return geometry;
}

const tilted_hex_geometry = TiltedHexagonGeometry(1);
