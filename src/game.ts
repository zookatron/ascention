import Input from './input';
import Transients from './transients';
import { HexVector, hexCircleArea } from './hexagon';
import { pointingLocation } from './grid';
import { StructureType, StructureSpecs, createStructure } from './structure';
import { deltaInterp, Timer, clamp } from './utilities';
import { Vector2 } from 'three';
import { Renderer2D } from './renderer2D';
import { Interface } from './interface';
import { Universe } from './universe';
import { WorldSpecs } from './world';
import { TICKS_PER_SECOND } from './constants';

export class Game {
	private universe: Universe;
	private renderer: Renderer2D;
	private interface: Interface;
	private timer: Timer = new Timer(1);

	private selected: number[] = [];
	private zoomOut = 0;
	private zoomOutTarget = 0;
	private firstAtMaxZoom = -1;
	private position = new Vector2();

	private frames = 0;
	private lastframesupdate = 0;

	constructor(render_container: HTMLElement | null, interface_container: HTMLElement | null) {
		if(!render_container) { throw new Error('Invalid render container element provided'); }
		if(!interface_container) { throw new Error('Invalid interface container element provided'); }
		this.renderer = new Renderer2D(render_container);
		this.interface = new Interface(interface_container);
		this.universe = new Universe();
		this.universe.snapTo(this.universe.world.child(0));

		void this.renderer.load().then(() => {
			createStructure(StructureType.MainBase, new HexVector(0, 0), this.universe.world);
			createStructure(StructureType.Factory, new HexVector(4, 0), this.universe.world);
			createStructure(StructureType.Miner, new HexVector(6, 0), this.universe.world);
			requestAnimationFrame(this.frame.bind(this));
		});
	}

	// private getSelected(): Structure[] {
	// 	return this.selected.map(id => this.universe.world.structures.get(id)).filter(_.identity) as Structure[];
	// }

	private exponentialZoom(zoom: number) {
		return Math.exp(zoom * 0.25);
	}

	private frame(currentTime: number) {
		requestAnimationFrame(this.frame.bind(this));

		this.timer.update(currentTime * 0.001 * TICKS_PER_SECOND);

		const world = this.universe.world;
		const worldspec = WorldSpecs[world.type];

		if(Input.mouse.scroll()) { this.zoomOutTarget += Input.mouse.scroll(); }
		this.zoomOutTarget = clamp(this.zoomOutTarget, worldspec.minZoom, worldspec.maxZoom);
		const realZoomOutTarget = this.exponentialZoom(this.zoomOutTarget);
		this.zoomOut = deltaInterp(clamp(this.zoomOut, this.exponentialZoom(worldspec.minZoom), this.exponentialZoom(worldspec.maxZoom)), realZoomOutTarget, 0.9, this.timer.delta);

		// if(this.firstAtMaxZoom === -1 && realZoomOutTarget === worldspec.maxZoom) {
		// 	this.firstAtMaxZoom = this.timer.time;
		// } else if(this.firstAtMaxZoom !== -1 && realZoomOutTarget < worldspec.maxZoom) {
		// 	this.firstAtMaxZoom = -1;
		// }

		// if(this.universe.transitioning === 0) {
		// 	if(this.firstAtMaxZoom !== -1 && this.timer.time - this.firstAtMaxZoom > 10 && Input.mouse.scroll() > 0 && world.id) {
		// 		this.selected = [];
		// 		this.universe.transitionTo(world.parent);
		// 	}
		// } else if(this.universe.transitioning < 0) {
		// 	if(world.id) {
		// 		this.zoomOut = realZoomOutTarget = worldspec.maxZoom + (1 + this.universe.transitioning) * worldspec.maxZoom / 6;
		// 	} else {
		// 		this.zoomOut = realZoomOutTarget = worldspec.minZoom - (1 + this.universe.transitioning) * worldspec.minZoom / 3;
		// 	}
		// } else if(this.universe.transitioning > 0) {
		// 	if(world.id) {
		// 		this.zoomOut = realZoomOutTarget = worldspec.maxZoom + (1 - this.universe.transitioning) * worldspec.maxZoom / 6;
		// 	} else {
		// 		this.zoomOut = realZoomOutTarget = worldspec.minZoom - (1 - this.universe.transitioning) * worldspec.minZoom / 3;
		// 	}
		// }

		const mousePos = Input.mouse.position();
		const dragStart = Input.mouse.dragStart('left');
		const size = this.renderer.size();
		const screenMove = size ? Input.mouse.movement().multiplyScalar(1 / size.x) : null;

		if(Input.mouse.down('middle') && screenMove) {
			this.position.add(Transients.get(Vector2).copy(screenMove).multiplyScalar(-2.4).multiplyScalar(this.zoomOut));
		}

		const mouseRay = this.renderer.ray(mousePos);
		const currentPointingLocation = mouseRay ? pointingLocation(world.grid, mouseRay) : null;
		const currentPointingCell = currentPointingLocation ? Transients.get(HexVector).copy(currentPointingLocation).round() : null;

		let indicating = '';
		if(currentPointingCell && Input.mouse.released('right') && this.selected.length) {
			const enemies = world.structuresAt(currentPointingCell, true);
			if(enemies.length) {
				indicating = 'attack';
				// for(const structure of this.getSelected()) {
				// 	if(UnitSpecs[unit.type].dps === 0) { continue; }
				// 	setUnitGoal(unit, new AttackGoal(enemies[0]), world, this.events);
				// }
			} else {
				indicating = 'move';
				// const destination = this.pathfinder.destination(world, currentPointingCell);
				// for(const unit of this.getSelected()) {
				// 	if(UnitSpecs[unit.type].speed === 0) { continue; }
				// 	setUnitGoal(unit, new MoveGoal(destination), world, this.events);
				// }
			}
		}

		const making = this.interface.getState().make;
		let buildingGood = false;
		if(making.building && currentPointingCell) {
			buildingGood = true;
			const buildingSize = StructureSpecs[making.building].size;
			Transients.push(HexVector);
			const temp = Transients.get(HexVector);
			for(let i = hexCircleArea(buildingSize) - 1; i >= 0; i--) {
				const cell = world.grid.get(temp.fromIndex(i).add(currentPointingCell));
				if(!cell || cell.obstacle/* || cell.occupiedBy.length !== 0*/) { buildingGood = false; break; }
			}
			Transients.pop(HexVector);
		}

		// if(Input.mouse.released('left')) {
		// 	if(making.building) {
		// 		if(this.selected.length) { this.selected.length = 0; }
		// 		if(currentPointingCell && buildingGood) {
		// 			createStructure(making.building, currentPointingCell, world);
		// 			this.interface.setState({ make: { ...making, building: null } });
		// 		}
		// 	} else if(Input.mouse.dragging('left')) {
		// 		const frustum = this.renderer.frustum(dragStart.x, dragStart.y, mousePos.x, mousePos.y);
		// 		if(frustum) { this.selected = world.structuresInside(frustum, this.timer.time).map(structure => structure.id); }
		// 	} else if(currentPointingCell) {
		// 		if(world.id) {
		// 			this.selected = world.structuresAt(currentPointingCell).map(structure => structure.id).slice(0, 1);
		// 		} else {
		// 			currentPointingCell.toVector2(this.position);
		// 			this.universe.transitionTo(world.child(currentPointingCell.toIndex()));
		// 		}
		// 	} else {
		// 		this.selected.length = 0;
		// 	}
		// }
		if(Input.mouse.released('right')) {
			if(making.building) {
				this.interface.setState({ make: { ...making, building: null } });
			}
		}

		this.frames += 1;
		if(this.timer.time > this.lastframesupdate + TICKS_PER_SECOND) {
			this.interface.setState({ fps: this.frames });
			this.lastframesupdate = this.timer.time;
			this.frames = 0;
		}

		world.tick = Math.floor(this.timer.time);
		this.universe.update(this.timer.delta);

		this.renderer.render(
			this.timer.time,
			this.timer.delta,
			world,
			this.zoomOut,
			this.position,
			currentPointingCell,
			this.selected,
			Input.mouse.dragging('left'),
			mousePos,
			dragStart,
			indicating,
			making.building,
			buildingGood,
		);

		Input.mouse.update();
		Input.keyboard.update();
		Transients.reset();
	}
}
