import { Vector2, Vector3, Vector4, Quaternion, Color } from 'three';

export type ShaderAttributeType = typeof Vector2 | typeof Vector3 | typeof Vector4 | typeof Quaternion | typeof Color | typeof Number;
export type ShaderAttributeValue = Vector2 | Vector3 | Vector4 | Quaternion | Color | number;

// tslint:disable-next-line:variable-name
export const ShaderIncludes: { [key: string]: string } = {
	std_vert_parameters: `
		vec3 applyQuaternion(in vec3 pos, in vec4 rot) { return pos + 2.0 * cross(rot.xyz, cross(rot.xyz, pos) + rot.w * pos); }
		#define PHYSICAL
		varying vec3 vViewPosition;
		#ifndef FLAT_SHADED
			varying vec3 vNormal;
		#endif
		#include <common>
		#include <uv_pars_vertex>
		#include <uv2_pars_vertex>
		#include <displacementmap_pars_vertex>
		#include <color_pars_vertex>
		#include <fog_pars_vertex>
		#include <morphtarget_pars_vertex>
		#include <skinning_pars_vertex>
		#include <shadowmap_pars_vertex>
		#include <logdepthbuf_pars_vertex>
		#include <clipping_planes_pars_vertex>
	`,
	std_vert_main: `
		#include <std_vert_color>
		#include <std_vert_normal>
		#include <std_vert_position>
		#include <std_vert_extras>
	`,
	std_vert_color: `
		#include <uv_vertex>
		#include <uv2_vertex>
		#include <color_vertex>
	`,
	std_vert_normal: `
		#include <beginnormal_vertex>
		#include <morphnormal_vertex>
		#include <skinbase_vertex>
		#include <skinnormal_vertex>
	`,
	std_vert_position: `
		#include <defaultnormal_vertex>
		#ifndef FLAT_SHADED
			vNormal = normalize( transformedNormal );
		#endif
		#include <begin_vertex>
		#include <morphtarget_vertex>
		#include <skinning_vertex>
		#include <displacementmap_vertex>
	`,
	std_vert_extras: `
		#include <project_vertex>
		#include <logdepthbuf_vertex>
		#include <clipping_planes_vertex>
		vViewPosition = - mvPosition.xyz;
		#include <worldpos_vertex>
		#include <shadowmap_vertex>
		#include <fog_vertex>
	`,
	std_frag_parameters: `
		#define PHYSICAL
		uniform vec3 diffuse;
		uniform vec3 emissive;
		uniform float roughness;
		uniform float metalness;
		uniform float opacity;
		#ifndef STANDARD
			uniform float clearCoat;
			uniform float clearCoatRoughness;
		#endif
		varying vec3 vViewPosition;
		#ifndef FLAT_SHADED
			varying vec3 vNormal;
		#endif
		#include <common>
		#include <packing>
		#include <dithering_pars_fragment>
		#include <color_pars_fragment>
		#include <uv_pars_fragment>
		#include <uv2_pars_fragment>
		#include <map_pars_fragment>
		#include <alphamap_pars_fragment>
		#include <aomap_pars_fragment>
		#include <lightmap_pars_fragment>
		#include <emissivemap_pars_fragment>
		#include <envmap_pars_fragment>
		#include <fog_pars_fragment>
		#include <bsdfs>
		#include <cube_uv_reflection_fragment>
		#include <lights_pars_begin>
		#include <envmap_physical_pars_fragment>
		#include <lights_physical_pars_fragment>
		#include <customized_shadowmap_pars_fragment>
		#include <bumpmap_pars_fragment>
		#include <normalmap_pars_fragment>
		#include <roughnessmap_pars_fragment>
		#include <metalnessmap_pars_fragment>
		#include <logdepthbuf_pars_fragment>
		#include <clipping_planes_pars_fragment>
	`,
	std_frag_main: `
		#include <std_frag_color>
		#include <std_frag_extras>
	`,
	std_frag_color: `
		#include <clipping_planes_fragment>
		vec4 diffuseColor = vec4( diffuse, opacity );
		ReflectedLight reflectedLight = ReflectedLight( vec3( 0.0 ), vec3( 0.0 ), vec3( 0.0 ), vec3( 0.0 ) );
		vec3 totalEmissiveRadiance = emissive;
		#include <logdepthbuf_fragment>
		#include <map_fragment>
		#include <color_fragment>
	`,
	std_frag_extras: `
		#include <alphamap_fragment>
		#include <alphatest_fragment>
		#include <roughnessmap_fragment>
		#include <metalnessmap_fragment>
		#include <normal_fragment_begin>
		#include <normal_fragment_maps>
		#include <emissivemap_fragment>
		#include <lights_physical_fragment>
		#include <lights_fragment_begin>
		#include <lights_fragment_maps>
		#include <lights_fragment_end>
		#include <aomap_fragment>
		vec3 outgoingLight = reflectedLight.directDiffuse + reflectedLight.indirectDiffuse + reflectedLight.directSpecular + reflectedLight.indirectSpecular + totalEmissiveRadiance;
		gl_FragColor = vec4( outgoingLight, diffuseColor.a );
		#include <tonemapping_fragment>
		#include <encodings_fragment>
		#include <fog_fragment>
		#include <premultiplied_alpha_fragment>
		#include <dithering_fragment>
	`,
	std_depth_vert_parameters: `
		vec3 applyQuaternion(in vec3 pos, in vec4 rot) { return pos + 2.0 * cross(rot.xyz, cross(rot.xyz, pos) + rot.w * pos); }
		#include <common>
		#include <uv_pars_vertex>
		#include <displacementmap_pars_vertex>
		#include <morphtarget_pars_vertex>
		#include <skinning_pars_vertex>
		#include <logdepthbuf_pars_vertex>
		#include <clipping_planes_pars_vertex>
	`,
	std_depth_vert_main: `
		#include <std_depth_vert_position>
		#include <std_depth_vert_extras>
	`,
	std_depth_vert_position: `
		#include <uv_vertex>
		#include <skinbase_vertex>
		#ifdef USE_DISPLACEMENTMAP
			#include <beginnormal_vertex>
			#include <morphnormal_vertex>
			#include <skinnormal_vertex>
		#endif
		#include <begin_vertex>
		#include <morphtarget_vertex>
		#include <skinning_vertex>
		#include <displacementmap_vertex>
	`,
	std_depth_vert_extras: `
		#include <project_vertex>
		#include <logdepthbuf_vertex>
		#include <clipping_planes_vertex>
	`,
	std_depth_frag_parameters: `
		#if DEPTH_PACKING == 3200
			uniform float opacity;
		#endif
		#include <common>
		#include <packing>
		#include <uv_pars_fragment>
		#include <map_pars_fragment>
		#include <alphamap_pars_fragment>
		#include <logdepthbuf_pars_fragment>
		#include <clipping_planes_pars_fragment>
	`,
	std_depth_frag_main: `
		#include <clipping_planes_fragment>
		vec4 diffuseColor = vec4( 1.0 );
		#if DEPTH_PACKING == 3200
			diffuseColor.a = opacity;
		#endif
		#include <map_fragment>
		#include <alphamap_fragment>
		#include <alphatest_fragment>
		#include <logdepthbuf_fragment>
		#if DEPTH_PACKING == 3200
			gl_FragColor = vec4( vec3( 1.0 - gl_FragCoord.z ), opacity );
		#elif DEPTH_PACKING == 3201
			gl_FragColor = packDepthToRGBA( gl_FragCoord.z );
		#endif
	`,
	customized_shadowmap_pars_fragment: `
		#ifdef USE_SHADOWMAP
			#if NUM_DIR_LIGHTS > 0
				uniform sampler2D directionalShadowMap[ NUM_DIR_LIGHTS ];
				varying vec4 vDirectionalShadowCoord[ NUM_DIR_LIGHTS ];
			#endif
			#if NUM_SPOT_LIGHTS > 0
				uniform sampler2D spotShadowMap[ NUM_SPOT_LIGHTS ];
				varying vec4 vSpotShadowCoord[ NUM_SPOT_LIGHTS ];
			#endif
			#if NUM_POINT_LIGHTS > 0
				uniform sampler2D pointShadowMap[ NUM_POINT_LIGHTS ];
				varying vec4 vPointShadowCoord[ NUM_POINT_LIGHTS ];
			#endif
			float texture2DCompare( sampler2D depths, vec2 uv, float compare ) {
				return step( compare, unpackRGBAToDepth( texture2D( depths, uv ) ) );
			}
			float texture2DShadowLerp( sampler2D depths, vec2 size, vec2 uv, float compare ) {
				const vec2 offset = vec2( 0.0, 1.0 );
				vec2 texelSize = vec2( 1.0 ) / size;
				vec2 centroidUV = floor( uv * size + 0.5 ) / size;
				float lb = texture2DCompare( depths, centroidUV + texelSize * offset.xx, compare );
				float lt = texture2DCompare( depths, centroidUV + texelSize * offset.xy, compare );
				float rb = texture2DCompare( depths, centroidUV + texelSize * offset.yx, compare );
				float rt = texture2DCompare( depths, centroidUV + texelSize * offset.yy, compare );
				vec2 f = fract( uv * size + 0.5 );
				float a = mix( lb, lt, f.y );
				float b = mix( rb, rt, f.y );
				float c = mix( a, b, f.x );
				return c;
			}
			float getShadow( sampler2D shadowMap, vec2 shadowMapSize, float shadowBias, float shadowRadius, vec4 shadowCoord ) {
				float shadow = 0.0;
				shadowCoord.xyz /= shadowCoord.w;
				shadowCoord.z += shadowBias;
				bvec4 inFrustumVec = bvec4 ( shadowCoord.x >= 0.0, shadowCoord.x <= 1.0, shadowCoord.y >= 0.0, shadowCoord.y <= 1.0 );
				bool inFrustum = all( inFrustumVec );
				bvec2 frustumTestVec = bvec2( inFrustum, shadowCoord.z <= 1.0 );
				bool frustumTest = all( frustumTestVec );
				if ( frustumTest ) {
				#if defined( SHADOWMAP_TYPE_PCF )
					vec2 texelSize = vec2( 1.0 ) / shadowMapSize;
					float dx0 = - texelSize.x * shadowRadius;
					float dy0 = - texelSize.y * shadowRadius;
					float dx1 = + texelSize.x * shadowRadius;
					float dy1 = + texelSize.y * shadowRadius;
					shadow = (
						texture2DCompare( shadowMap, shadowCoord.xy + vec2( dx0, dy0 ), shadowCoord.z ) +
						texture2DCompare( shadowMap, shadowCoord.xy + vec2( 0.0, dy0 ), shadowCoord.z ) +
						texture2DCompare( shadowMap, shadowCoord.xy + vec2( dx1, dy0 ), shadowCoord.z ) +
						texture2DCompare( shadowMap, shadowCoord.xy + vec2( dx0, 0.0 ), shadowCoord.z ) +
						texture2DCompare( shadowMap, shadowCoord.xy, shadowCoord.z ) +
						texture2DCompare( shadowMap, shadowCoord.xy + vec2( dx1, 0.0 ), shadowCoord.z ) +
						texture2DCompare( shadowMap, shadowCoord.xy + vec2( dx0, dy1 ), shadowCoord.z ) +
						texture2DCompare( shadowMap, shadowCoord.xy + vec2( 0.0, dy1 ), shadowCoord.z ) +
						texture2DCompare( shadowMap, shadowCoord.xy + vec2( dx1, dy1 ), shadowCoord.z )
					) * ( 1.0 / 9.0 );
				#elif defined( SHADOWMAP_TYPE_PCF_SOFT )
					vec2 texelSize = vec2( 1.0 ) / shadowMapSize;
					float dx0 = - texelSize.x * shadowRadius;
					float dy0 = - texelSize.y * shadowRadius;
					float dx1 = + texelSize.x * shadowRadius;
					float dy1 = + texelSize.y * shadowRadius;
					shadow = (
						texture2DShadowLerp( shadowMap, shadowMapSize, shadowCoord.xy + vec2( dx0, dy0 ), shadowCoord.z ) +
						texture2DShadowLerp( shadowMap, shadowMapSize, shadowCoord.xy + vec2( 0.0, dy0 ), shadowCoord.z ) +
						texture2DShadowLerp( shadowMap, shadowMapSize, shadowCoord.xy + vec2( dx1, dy0 ), shadowCoord.z ) +
						texture2DShadowLerp( shadowMap, shadowMapSize, shadowCoord.xy + vec2( dx0, 0.0 ), shadowCoord.z ) +
						texture2DShadowLerp( shadowMap, shadowMapSize, shadowCoord.xy, shadowCoord.z ) +
						texture2DShadowLerp( shadowMap, shadowMapSize, shadowCoord.xy + vec2( dx1, 0.0 ), shadowCoord.z ) +
						texture2DShadowLerp( shadowMap, shadowMapSize, shadowCoord.xy + vec2( dx0, dy1 ), shadowCoord.z ) +
						texture2DShadowLerp( shadowMap, shadowMapSize, shadowCoord.xy + vec2( 0.0, dy1 ), shadowCoord.z ) +
						texture2DShadowLerp( shadowMap, shadowMapSize, shadowCoord.xy + vec2( dx1, dy1 ), shadowCoord.z )
					) * ( 1.0 / 9.0 );
				#else
					shadow = texture2DCompare( shadowMap, shadowCoord.xy, shadowCoord.z );
				#endif
				}
				return shadow;
			}
			vec2 cubeToUV( vec3 v, float texelSizeY ) {
				vec3 absV = abs( v );
				float scaleToCube = 1.0 / max( absV.x, max( absV.y, absV.z ) );
				absV *= scaleToCube;
				v *= scaleToCube * ( 1.0 - 2.0 * texelSizeY );
				vec2 planar = v.xy;
				float almostATexel = 1.5 * texelSizeY;
				float almostOne = 1.0 - almostATexel;
				if ( absV.z >= almostOne ) {
					if ( v.z > 0.0 )
						planar.x = 4.0 - v.x;
				} else if ( absV.x >= almostOne ) {
					float signX = sign( v.x );
					planar.x = v.z * signX + 2.0 * signX;
				} else if ( absV.y >= almostOne ) {
					float signY = sign( v.y );
					planar.x = v.x + 2.0 * signY + 2.0;
					planar.y = v.z * signY - 2.0;
				}
				return vec2( 0.125, 0.25 ) * planar + vec2( 0.375, 0.75 );
			}
			float getPointShadow( sampler2D shadowMap, vec2 shadowMapSize, float shadowBias, float shadowRadius, vec4 shadowCoord, float shadowCameraNear, float shadowCameraFar ) {
				vec2 texelSize = vec2( 1.0 ) / ( shadowMapSize * vec2( 4.0, 2.0 ) );
				vec3 lightToPosition = shadowCoord.xyz;
				float dp = ( length( lightToPosition ) - shadowCameraNear ) / ( shadowCameraFar - shadowCameraNear );        dp += shadowBias;
				vec3 bd3D = normalize( lightToPosition );
				#if defined( SHADOWMAP_TYPE_PCF ) || defined( SHADOWMAP_TYPE_PCF_SOFT )
					vec2 offset = vec2( - 1, 1 ) * shadowRadius * texelSize.y;
					return (
						texture2DCompare( shadowMap, cubeToUV( bd3D + offset.xyy, texelSize.y ), dp ) +
						texture2DCompare( shadowMap, cubeToUV( bd3D + offset.yyy, texelSize.y ), dp ) +
						texture2DCompare( shadowMap, cubeToUV( bd3D + offset.xyx, texelSize.y ), dp ) +
						texture2DCompare( shadowMap, cubeToUV( bd3D + offset.yyx, texelSize.y ), dp ) +
						texture2DCompare( shadowMap, cubeToUV( bd3D, texelSize.y ), dp ) +
						texture2DCompare( shadowMap, cubeToUV( bd3D + offset.xxy, texelSize.y ), dp ) +
						texture2DCompare( shadowMap, cubeToUV( bd3D + offset.yxy, texelSize.y ), dp ) +
						texture2DCompare( shadowMap, cubeToUV( bd3D + offset.xxx, texelSize.y ), dp ) +
						texture2DCompare( shadowMap, cubeToUV( bd3D + offset.yxx, texelSize.y ), dp )
					) * ( 1.0 / 9.0 );
				#else
					return texture2DCompare( shadowMap, cubeToUV( bd3D, texelSize.y ), dp );
				#endif
			}
		#endif
	`,
};
