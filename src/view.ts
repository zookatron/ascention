import { Vector2, OrthographicCamera } from 'three';
import { HexVector, hexCircleArea, hexCircleSideLength } from './hexagon';
import Transients from './transients';
import { COS30 } from './constants';
import { modulate, antimodulate } from './utilities';

export class View {
	camera: OrthographicCamera;
	position = new Vector2();
	size = new Vector2();
	scale = 1;
	level = 0;
	lowerLevel = 0;
	upperLevel = 0;
	lowerLevelRatio = 0;
	upperLevelRatio = 0;
	lowerTransform = new Vector2();
	upperTransform = new Vector2();
	lowerOffset = new HexVector();
	upperOffset = new HexVector();
	lowerVisibleGroups = new Array<number>();
	upperVisibleGroups = new Array<number>();
	readonly VISIBILITY_GROUP_SIZE = 10;
	readonly VISIBILITY_GROUP_LENGTH = hexCircleArea(this.VISIBILITY_GROUP_SIZE);
	readonly LEVEL_SIZE = 5;
	readonly LEVEL_SCALE = hexCircleSideLength(this.LEVEL_SIZE);
	private readonly LEVEL_SCALE_LOG = 1 / Math.log(this.LEVEL_SCALE);
	private readonly LEVEL_TRANSFORM = new Vector2(1.5 * this.LEVEL_SIZE - 1, -COS30 * this.LEVEL_SIZE);

	constructor() {
		this.camera = new OrthographicCamera(0, 0, 0, 0, 1, 1000);
		this.camera.position.set(0, 500, 0);
		this.camera.rotation.x = -Math.PI / 2;
		this.camera.rotation.z = Math.PI;
	}

	update() {
		this.level = Math.log(this.scale) * this.LEVEL_SCALE_LOG - 1.1;
		this.lowerLevel = Math.floor(this.level);
		this.upperLevel = this.lowerLevel + 1;
		this.upperLevelRatio = this.level % 1;
		this.lowerLevelRatio = 1 - this.upperLevelRatio;

		this.lowerTransform.set(1, 0);
		this.upperTransform.set(1, 0);
		for(let iteration = 0; iteration < this.upperLevel; iteration++) {
			this.lowerTransform.copy(this.upperTransform);
			modulate(this.upperTransform, this.LEVEL_TRANSFORM);
		}

		const exactUpperPosition2 = antimodulate(Transients.get(Vector2).copy(this.position), this.upperTransform);
		this.upperOffset.fromVector2(exactUpperPosition2).round();
		this.lowerOffset.copy(this.upperOffset).antituple(this.LEVEL_SIZE);
		const roundedUpperPosition2 = modulate(this.upperOffset.toVector2(Transients.get(Vector2)), this.upperTransform);
		const realPosition = Transients.get(Vector2).copy(this.position).sub(roundedUpperPosition2);

		const aspect = this.size.x / this.size.y;
		this.camera.left = this.scale * aspect;
		this.camera.right = -this.scale * aspect;
		this.camera.top = -this.scale;
		this.camera.bottom = this.scale;
		this.camera.updateProjectionMatrix();

		this.camera.position.set(realPosition.x, 500, realPosition.y);
		this.camera.updateMatrixWorld(false);

		this.getVisibleGroups(this.lowerVisibleGroups, realPosition, this.lowerTransform);
		this.getVisibleGroups(this.upperVisibleGroups, realPosition, this.upperTransform);
	}

	private getVisibleGroups(visibleGroups: number[], realPosition: Vector2, transform: Vector2) {
		// 18 ~= Math.ceil(hexCircleSideLength(this.VISIBILITY_GROUP_SIZE)) + 1;
		const margin = 18 * transform.length();
		const minx = realPosition.x + this.camera.right - margin;
		const maxx = realPosition.x + this.camera.left + margin;
		const miny = realPosition.y + this.camera.top - margin;
		const maxy = realPosition.y + this.camera.bottom + margin;

		visibleGroups.length = 0;
		const center = antimodulate(Transients.get(Vector2).copy(realPosition), transform);
		const hexCenter = Transients.get(HexVector).fromVector2(center).tuple(this.VISIBILITY_GROUP_SIZE).round();
		const groupHexPosition = Transients.get(HexVector).set(0, 0);
		const groupPosition = Transients.get(Vector2).set(0, 0);
		for(let index = 150; index >= 0; index--) {
			groupHexPosition.fromIndex(index).add(hexCenter).antituple(this.VISIBILITY_GROUP_SIZE).toVector2(groupPosition);
			modulate(groupPosition, transform);
			if(groupPosition.x > minx && groupPosition.x < maxx && groupPosition.y > miny && groupPosition.y < maxy) {
				visibleGroups.push(groupHexPosition.fromIndex(index).add(hexCenter).toIndex());
			}
		}
	}
}
