import * as _ from 'lodash';
import { HexGrid, HexVector, hexNeighborIndexes } from './hexagon';
import { HexCell, HexGroup, HEX_GROUP_SIZE, HEX_GROUP_LENGTH, groupSegments, groupOpen, groupConnections, getGridPosition /*hexGroupIndex, groupDistances, groupGradient*/ } from './grid';
import { Structure, StructureType } from './structure';
import { Frustum, Vector2, Vector3 } from 'three';
import Transients from './transients';
import Random from './random';

const ONE_THIRD = 1 / 3;

export enum TerrainType {
	Dirt,
	Flowers,
	Grass,
	Stone,
	Noise,
	Hex,
}

export enum WorldType {
	None,
	Region,
	Planet,
	SolarSystem,
	Galaxy,
	Intergalactic,
}

export interface WorldSpec {
	gridSize: number;
	terrainHeight: number;
	minZoom: number;
	maxZoom: number;
}

// tslint:disable-next-line:variable-name
export const WorldSpecs: { [type: number]: WorldSpec } = {
	[WorldType.Region]: {
		gridSize: 40,
		terrainHeight: 5,
		minZoom: 4,
		maxZoom: 100,
	},
	[WorldType.Planet]: {
		gridSize: 40,
		terrainHeight: 5,
		minZoom: 10,
		maxZoom: 120,
	},
};

export class World {
	public type: WorldType;
	public id: string;
	public parent: string;
	public structures: Map<number, Structure> = new Map<number, Structure>();
	public grid: HexGrid<HexCell>;
	public groups: HexGrid<HexGroup>;
	public tick: number;
	public visibilityChangeTime = 10;

	constructor(id: string) {
		this.id = id;
		const idParts = _.dropRight(id.split('-'));
		this.parent = idParts.join('-');
		this.type = 2 - idParts.length;
		const spec = WorldSpecs[this.type];

		this.grid = new HexGrid(spec.gridSize);
		// TODO: How to properly calculate groups needed for given grid size?
		this.groups = new HexGrid(Math.ceil(spec.gridSize / (HEX_GROUP_SIZE + 2)));
		this.tick = 0;

		const position = Transients.get(HexVector);
		const normal = Transients.get(Vector3).set(0, 1, 0);
		for(let index = 0; index < this.grid.length; index++) {
			this.grid.setByIndex(index, new HexCell(position.fromIndex(index), 0, false, 0, 0, 0, 0, normal));
		}

		const seed = Random.number.integer(Random.number.context(4));
		// const context = Random.number.context(seed);
		// const offset = Transients.get(Vector2).set(0, 0);
		// const smoothstep = (x: number) => x * x * (3.0 - 2.0 * x);
		// for(let index = 0; index < this.grid.length; index++) {
		// 	const cell = this.grid.getByIndex(index);
		// 	if(!cell) { continue; }
		// 	let noiseval = 0;
		// 	offset.set(Random.noise.noise2d(cell.position.row * 0.005 + 3000.0, cell.position.col * 0.005 + 7000.0, seed), Random.noise.noise2d(cell.position.row * 0.005 + 11000.0, cell.position.col * 0.005 + 5000.0, seed));
		// 	offset.multiplyScalar(0.5);
		// 	noiseval += 1.0 * Random.noise.noise2d(cell.position.row * 0.01 + offset.x, cell.position.col * 0.01 + offset.y, seed);
		// 	offset.set(Random.noise.noise2d(cell.position.row * 0.02 + 3000.0, cell.position.col * 0.02 + 7000.0, seed), Random.noise.noise2d(cell.position.row * 0.02 + 11000.0, cell.position.col * 0.02 + 5000.0, seed));
		// 	offset.multiplyScalar(0.5);
		// 	noiseval += 0.5 * Random.noise.noise2d(cell.position.row * 0.02 + offset.x, cell.position.col * 0.02 + offset.y, seed);
		// 	offset.set(Random.noise.noise2d(cell.position.row * 0.04 + 3000.0, cell.position.col * 0.04 + 7000.0, seed), Random.noise.noise2d(cell.position.row * 0.04 + 11000.0, cell.position.col * 0.04 + 5000.0, seed));
		// 	offset.multiplyScalar(0.5);
		// 	noiseval += 0.2 * Random.noise.noise2d(cell.position.row * 0.04 + offset.x, cell.position.col * 0.04 + offset.y, seed) * Random.noise.noise2d(cell.position.row * 0.04 + 5000.0, cell.position.col * 0.04 + 3000.0, seed);
		// 	offset.set(Random.noise.noise2d(cell.position.row * 0.04 + 7000.0, cell.position.col * 0.04 + 13000.0, seed), Random.noise.noise2d(cell.position.row * 0.04 + 7000.0, cell.position.col * 0.04 + 1000.0, seed));
		// 	offset.multiplyScalar(0.5);
		// 	noiseval += 0.2 * Random.noise.noise2d(cell.position.row * 0.04 + offset.x + 7000.0, cell.position.col * 0.04 + offset.y + 3000.0, seed) * Random.noise.noise2d(cell.position.row * 0.04 + 7000.0, cell.position.col * 0.04 + 11000.0, seed);
		// 	noiseval = Math.pow(noiseval / 1.9, 3);
		// 	const dist = cell.position.length();
		// 	const edges = (dist > spec.gridSize - spec.terrainHeight * 0.75) ? spec.terrainHeight * 0.5 * smoothstep((dist - spec.gridSize + spec.terrainHeight * 0.75) / (spec.terrainHeight * 0.75)) : 0;
		// 	cell.height = noiseval * spec.terrainHeight + edges;
		// }
		for(let index = 0; index < this.grid.length; index++) {
			const cell = this.grid.getByIndex(index);
			if(!cell) { continue; }
			let noiseval = 0;
			noiseval += 1.0 * Random.noise.noise2d(cell.position.row * 0.01, cell.position.col * 0.01, seed);
			noiseval += 0.5 * Random.noise.noise2d(cell.position.row * 0.02 + 13997, cell.position.col * 0.02 + 13963, seed);
			noiseval += 0.25 * Random.noise.noise2d(cell.position.row * 0.04 - 14929, cell.position.col * 0.04 - 13907, seed);
			let caves = 0;
			caves += Random.noise.noise2d(cell.position.row * 0.01 - 15809, cell.position.col * 0.01 + 16451, seed);
			caves += 0.5 * Random.noise.noise2d(cell.position.row * 0.02 + 15881, cell.position.col * 0.02 - 17011, seed);
			const inside_cave = Math.abs(caves - 0.75) < 0.03;
			cell.height = inside_cave ? 0 : noiseval * spec.terrainHeight;
		}

		const temp = Transients.get(HexVector);
		const tempvec = Transients.get(Vector2);
		const tempveca = Transients.get(Vector3);
		const tempvecb = Transients.get(Vector3);
		for(let index = 0; index < this.grid.length; index++) {
			const cell = this.grid.getByIndex(index);
			if(!cell) { continue; }
			position.fromIndex(index);

			const midcell = this.grid.get(position);
			const mid = midcell ? midcell.height : 0;
			const acell = this.grid.get(temp.set(0, 1).add(position));
			const a = acell ? acell.height : 0;
			const bcell = this.grid.get(temp.set(-1, 1).add(position));
			const b = bcell ? bcell.height : 0;
			const ccell = this.grid.get(temp.set(-1, 0).add(position));
			const c = ccell ? ccell.height : 0;
			const dcell = this.grid.get(temp.set(0, -1).add(position));
			const d = dcell ? dcell.height : 0;
			const ecell = this.grid.get(temp.set(1, -1).add(position));
			const e = ecell ? ecell.height : 0;
			const fcell = this.grid.get(temp.set(1, 0).add(position));
			const f = fcell ? fcell.height : 0;

			let min = mid, max = mid;
			const amidheight = (a + mid) * 0.5;
			if(amidheight > max) { max = amidheight; }
			if(amidheight < min) { min = amidheight; }
			const abmidheight = (a + b + mid) * ONE_THIRD;
			if(abmidheight > max) { max = abmidheight; }
			if(abmidheight < min) { min = abmidheight; }
			const bmidheight = (b + mid) * 0.5;
			if(bmidheight > max) { max = bmidheight; }
			if(bmidheight < min) { min = bmidheight; }
			const bcmidheight = (b + c + mid) * ONE_THIRD;
			if(bcmidheight > max) { max = bcmidheight; }
			if(bcmidheight < min) { min = bcmidheight; }
			const cmidheight = (c + mid) * 0.5;
			if(cmidheight > max) { max = cmidheight; }
			if(cmidheight < min) { min = cmidheight; }
			const cdmidheight = (c + d + mid) * ONE_THIRD;
			if(cdmidheight > max) { max = cdmidheight; }
			if(cdmidheight < min) { min = cdmidheight; }
			const dmidheight = (d + mid) * 0.5;
			if(dmidheight > max) { max = dmidheight; }
			if(dmidheight < min) { min = dmidheight; }
			const demidheight = (d + e + mid) * ONE_THIRD;
			if(demidheight > max) { max = demidheight; }
			if(demidheight < min) { min = demidheight; }
			const emidheight = (e + mid) * 0.5;
			if(emidheight > max) { max = emidheight; }
			if(emidheight < min) { min = emidheight; }
			const efmidheight = (e + f + mid) * ONE_THIRD;
			if(efmidheight > max) { max = efmidheight; }
			if(efmidheight < min) { min = efmidheight; }
			const fmidheight = (f + mid) * 0.5;
			if(fmidheight > max) { max = fmidheight; }
			if(fmidheight < min) { min = fmidheight; }
			const famidheight = (f + a + mid) * ONE_THIRD;
			if(famidheight > max) { max = famidheight; }
			if(famidheight < min) { min = famidheight; }
			cell.minheight = min;
			cell.maxheight = max;

			// let value = mid/2 + (a+b+c+d+e+f)/12;
			// cell.height = value;

			// let value = Random.number.float(context);
			// value = value * value * value * value * value;
			// const max_difference = Math.max(Math.abs(mid - a), Math.abs(mid - b), Math.abs(mid - c), Math.abs(mid - d), Math.abs(mid - e), Math.abs(mid - f));

			// cell.texture = max_difference * 0.2 + cell.height / spec.terrainHeight;
			// cell.texture = Math.min(Math.max(cell.texture, 0.0), 1.0);
			// cell.rocks = cell.texture > 0.4 ? 1 : 0;
			// cell.trees = Math.max((max_difference - 0.5) * 2, 0) + Math.max(((cell.height / spec.terrainHeight) - 0.3), 0);
			// if(cell.rocks !== 0) { cell.trees = 0; }
			// if(cell.trees <= 0.1) { cell.trees = 0; }
			// cell.trees = Math.min(Math.max(cell.trees, 0.0), 1.0);
			// cell.obstacle = cell.trees > 0 || cell.rocks > 0;
			// cell.decoration = value > 0.95 ? 1 : 0;
			// if(cell.rocks !== 0 || cell.trees !== 0) { cell.decoration = 0; }

			cell.obstacle = cell.height > spec.terrainHeight * 1.75 * 0.5;

			const deriv_xy = (a - d) / 9 + (b - e + f - c) / 18;
			const deriv_yz = (c - f) / 9 + (d - a + b - e) / 18;

			temp.set(0, 1).toVector2(tempvec);
			const deriv_xy_vec = tempveca.set(tempvec.x, deriv_xy * 3, tempvec.y);
			temp.set(-1, 0).toVector2(tempvec);
			const deriv_yz_vec = tempvecb.set(tempvec.x, deriv_yz * 3, tempvec.y);
			cell.normal.copy(deriv_xy_vec).cross(deriv_yz_vec).normalize();

			cell.texture = cell.obstacle ? 0.2 : 0.1;
		}

		const segments: Array<HexGrid<number>> = [];
		for(let index = 0; index < this.groups.length; index++) {
			segments[index] = groupSegments(groupOpen(this.grid, index));
		}

		const connections: number[][][] = [];
		const neighborsegments: Array<HexGrid<number> | null> = [];
		const neighborindexes: number[] = [];
		for(let index = 0; index < this.groups.length; index++) {
			let neighbor = 0;
			for(const neighbor_index of hexNeighborIndexes(index)) {
				neighborsegments[neighbor] = neighbor_index >= this.groups.length ? null : segments[neighbor_index];
				neighborindexes[neighbor] = neighbor_index;
				neighbor++;
			}
			connections[index] = groupConnections(segments[index], neighborsegments, neighborindexes);
			neighborsegments.length = 0;
			neighborindexes.length = 0;
		}

		const hexVertexes: Vector3[] = [];
		for(let i = 5; i >= 0; i--) {
			hexVertexes.push(Transients.get(Vector3).set(Math.cos(Math.PI / 3 * i + Math.PI / 6), 0, Math.sin(Math.PI / 3 * i + Math.PI / 6)));
			hexVertexes.push(Transients.get(Vector3).set(Math.cos(Math.PI / 3 * i + Math.PI / 6), 1, Math.sin(Math.PI / 3 * i + Math.PI / 6)));
		}

		const center = Transients.get(HexVector);
		const centervec = Transients.get(Vector3);
		const cellvertex = Transients.get(Vector3);
		const cellorigin = Transients.get(Vector3);
		for(let index = 0; index < this.groups.length; index++) {
			center.fromIndex(index).antituple(HEX_GROUP_SIZE);
			center.toVector3(centervec);
			let radius = 0;
			let initalized = false;
			for(let cellindex = HEX_GROUP_LENGTH - 1; cellindex >= 0; cellindex--) {
				const cell = this.grid.get(temp.fromIndex(cellindex).add(center));
				if(!cell) { continue; }
				if(!initalized) { centervec.setY(cell.height); initalized = true; }
				cell.position.toVector3(cellorigin);
				const base = cell.minheight;
				const height = cell.maxheight - cell.minheight + 2; // Constant factor for cell decorations
				for(let i = hexVertexes.length - 1; i >= 0; i--) {
					const length = cellvertex.copy(hexVertexes[i]).setY(base + cellvertex.y * height).add(cellorigin).sub(centervec).length();
					if(length > radius) { radius = length; }
				}
			}
			this.groups.setByIndex(index, new HexGroup(center, centervec, radius, segments[index], connections[index]));
		}

		// let target = Transients.get(HexVector).set(10, -32);
		// let [target_group_index, target_cell_index] = hexGroupIndex(target.toIndex());
		// let target_group = this.groups.getByIndex(target_group_index);
		// if(!target_group) { throw new Error('Invalid target hex'); }
		// let target_segment = target_group.segments.getByIndex(target_cell_index);
		// if(target_segment == undefined || target_segment == -1) { throw new Error('Invalid target hex'); }

		// let distances = new HexGrid<number[]>(this.groups.size);
		// groupDistances(distances, this.groups, target_group_index, target_segment);

		// let gradients: HexGrid<number>[] = [];
		// for(let index = 0; index < this.groups.length; index++) {
		// 	gradients[index] = new HexGrid<number>(HEX_GROUP_SIZE);
		// 	groupGradient(gradients[index], target, index, this.groups, distances);
		// }

		// for(let index = 0; index < this.grid.length; index++) {
		// 	let cell = this.grid.getByIndex(index);
		// 	if(!cell) { continue; }
		// 	cell.texture = 0;
		// 	let [group_index, cell_index] = hexGroupIndex(index);
		// 	let group = this.groups.getByIndex(group_index);
		// 	if(!group) { continue; }
		// 	let segment = group.segments.getByIndex(cell_index);
		// 	if(segment == undefined) { continue; }
		// 	let group_distances = distances.getByIndex(group_index);
		// 	if(!group_distances) { continue; }
		// 	//let distance = group_distances[segment];
		// 	//cell.texture = (distance+1)*0.03;
		// 	let group_gradients = gradients[group_index];
		// 	if(!group_gradients) { continue; }
		// 	let cell_gradient = group_gradients.getByIndex(cell_index);
		// 	if(cell_gradient == undefined) { continue; }
		// 	cell.texture = (cell_gradient+1)*0.03;
		// 	//cell.texture = ((distance+1)*12 + (cell_gradient+1))*0.01;
		// }
	}

	public structuresAt(hex: HexVector, enemy = false, dead = false): Structure[] {
		return Array.from(this.structures.values()).filter(structure => {
			const gridPos = Transients.get(Vector3);
			getGridPosition(this.grid, structure.position, gridPos);
			return structure.position === hex && structure.enemy === enemy && (!structure.died || dead);
		});
	}

	public structuresInside(frustum: Frustum, time: number, enemy = false, dead = false): Structure[] {
		return Array.from(this.structures.values()).filter(structure => {
			const gridPos = Transients.get(Vector3);
			getGridPosition(this.grid, structure.position, gridPos);
			return structure.enemy === enemy && (!structure.died || dead) && frustum.containsPoint(gridPos);
		});
	}

	public unitsOfType(type: StructureType): Structure[] {
		return Array.from(this.structures.values()).filter(structure => structure.type === type);
	}

	public child(index: number): string {
		if(this.type === WorldType.Region) { throw new Error(); }
		return `${this.id}-${index}`;
	}
}
