import { Vector2, Vector3, Vector4, Quaternion, Ray, Plane, Frustum, Color, WebGLRenderer, Scene, TextureLoader, Texture, Sphere, Matrix4 } from 'three';
import Transients from './transients';
import Input from './input';
import { Renderable, RenderableID, HexagonRenderable, TiltedHexagonRenderable } from './renderables';
import { TerrainRenderer } from './terrain2D';
import { HexVector } from './hexagon';
import { World } from './world';
import { View } from './view';
import { StructureType, StructureSpec, StructureSpecs } from './structure';

Transients.register(Vector2, 1000);
Transients.register(Vector3, 1000);
Transients.register(Vector4, 1000);
Transients.register(Quaternion, 1000);
Transients.register(Ray, 100);
Transients.register(Plane, 100);
Transients.register(Frustum, 100);
Transients.register(Color, 100);
Transients.register(Matrix4, 100);
Transients.register(Sphere, 10);

export class Renderer2D {
	private renderer: WebGLRenderer;
	private scene: Scene = new Scene();
	private overlayScene: Scene = new Scene();

	private terrainRenderer: TerrainRenderer;
	private renderables: Renderable[] = [];
	private structureRenderables: Record<string, RenderableID> = {};

	private textureLoader: TextureLoader = new TextureLoader();

	private view: View = new View();

	constructor(container: HTMLElement) {
		this.renderer = new WebGLRenderer({ antialias: true });
		this.renderer.setPixelRatio(window.devicePixelRatio);
		// this.renderer.setPixelRatio(2);
		this.renderer.autoClear = false;
		container.appendChild(this.renderer.domElement);

		this.terrainRenderer = new TerrainRenderer();

		window.addEventListener('resize', this.onResize.bind(this), false);
		this.onResize();
	}

	public async load(): Promise<void> {
		try {
			const makeStructureRenderable = async (type: string, structure: StructureSpec) => {
				this.structureRenderables[type] = this.addRenderable(new (structure.size === 1 ? HexagonRenderable : TiltedHexagonRenderable)(
					await this.loadTexture(`assets/textures/${structure.image}.png`),
					structure.size === 1 ? 1 : (0.5 + (structure.size - 1) * 1.5) / Math.cos(Math.PI / 6),
					0,
					this.getMaxInstances(),
				));
			};

			for(const type in StructureSpecs) {
				await makeStructureRenderable(type, StructureSpecs[type]);
			}

			await this.terrainRenderer.load(this);
		} catch(error) {
			// tslint:disable-next-line:no-empty no-console
			console.error(error);
		}
	}

	public loadTexture(url: string): Promise<Texture> {
		return new Promise((resolve, reject) => {
			return this.textureLoader.load(url, resolve, undefined, reject);
		});
	}

	public addRenderable(renderable: Renderable): RenderableID {
		renderable.id = this.renderables.length;
		this.scene.add(renderable.getObject());
		this.renderables.push(renderable);
		return renderable.id;
	}

	public getRenderable(id: RenderableID): Renderable {
		if(!this.renderables[id]) { throw new Error(`Unknown renderable "${id}"`); }
		return this.renderables[id];
	}

	public getMaxInstances(): number {
		return 10000;
	}

	public size(): Vector2 | null {
		if(isNaN(this.renderer.domElement.clientWidth) && isNaN(this.renderer.domElement.clientHeight)) { return null; }
		const result = Transients.get(Vector2);
		result.set(this.renderer.domElement.clientWidth, this.renderer.domElement.clientHeight);
		return result;
	}

	public ray(position: Vector2): Ray | null {
		if(isNaN(position.x) || isNaN(position.y)) { return null; }
		Transients.push(Vector2);
		const coords = Transients.get(Vector2);
		const size = this.size();
		Transients.pop(Vector2);
		if(!size) { return null; }
		coords.set((position.x / size.x) * 2 - 1, 1 - (position.y / size.y) * 2);
		const ray = Transients.get(Ray);
		ray.origin.set(coords.x, coords.y, -1).unproject(this.view.camera);
		ray.direction.set(coords.x, coords.y, 1).unproject(this.view.camera).sub(ray.origin).normalize();
		if(isNaN(ray.direction.x) || isNaN(ray.direction.y) || isNaN(ray.direction.z)) { return null; }
		return ray;
	}

	public frustum(left: number, top: number, right: number, bottom: number): Frustum | null {
		const neartopleft = Transients.get(Vector3).set(left, 1, top);
		const neartopright = Transients.get(Vector3).set(right, 1, top);
		const nearbottomleft = Transients.get(Vector3).set(left, 1, bottom);
		const nearbottomright = Transients.get(Vector3).set(right, 1, bottom);
		const fartopleft = Transients.get(Vector3).set(left, -1, top);
		const fartopright = Transients.get(Vector3).set(right, -1, top);
		const farbottomleft = Transients.get(Vector3).set(left, -1, bottom);
		const farbottomright = Transients.get(Vector3).set(right, -1, bottom);

		return Transients.get(Frustum).set(
			Transients.get(Plane).setFromCoplanarPoints(neartopright, neartopleft, fartopleft),
			Transients.get(Plane).setFromCoplanarPoints(neartopleft, nearbottomleft, farbottomleft),
			Transients.get(Plane).setFromCoplanarPoints(nearbottomleft, nearbottomright, farbottomright),
			Transients.get(Plane).setFromCoplanarPoints(nearbottomright, neartopright, fartopright),
			Transients.get(Plane).setFromCoplanarPoints(nearbottomright, nearbottomleft, neartopleft),
			Transients.get(Plane).setFromCoplanarPoints(fartopright, fartopleft, farbottomleft),
		);
	}

	public render(time: number, delta: number, world: World, zoomOut: number, position: Vector2, currentPointingCell: HexVector | null, selected: number[], dragging: boolean, mousePos: Vector2, dragStart: Vector2, indicating: string, building: StructureType | null, buildingGood: boolean) {
		this.view.position.copy(position);
		this.view.scale = zoomOut;
		this.view.update();

		this.terrainRenderer.render(world, this.view);

		this.renderStructures(world);

		this.updateBuildingPreview(world, currentPointingCell, building, buildingGood);

		this.renderables.forEach(renderable => renderable.update());

		this.renderer.clear();
		this.renderer.render(this.scene, this.view.camera);
		this.renderer.clearDepth();
		this.renderer.render(this.overlayScene, this.view.camera);
	}

	// Private Functions

	private onResize(): void {
		let width, height;
		if(window.innerWidth < window.innerHeight * 1.618) {
			width = window.innerWidth;
			height = window.innerWidth * 0.618;
		} else {
			width = window.innerHeight * 1.618;
			height = window.innerHeight;
		}

		this.renderer.setSize(width, height);
		this.view.size.set(width, height);

		const rect = this.renderer.domElement.getBoundingClientRect();
		Input.mouse.setBounds(rect.left, rect.top, rect.right - rect.left, rect.bottom - rect.top);
	}

	private updateBuildingPreview(world: World, currentPointingCell: HexVector | null, building: StructureType | null, buildingGood: boolean) {
		if(currentPointingCell && building) {
			const position = Transients.get(Vector3);
			const color = Transients.get(Color);
			currentPointingCell.toVector3(position);
			position.y = 1;
			color.setHex(buildingGood ? 0x00ff00 : 0xff0000);
			this.getRenderable(this.structureRenderables[building]).render(position, color);
		}
	}

	private renderStructures(world: World) {
		Transients.push(Vector3);
		const position = Transients.get(Vector3);
		const color = Transients.get(Color);
		color.setHex(0xffffff);

		if(this.view.lowerLevel === 0) {
			const visibleGroups = this.view.lowerVisibleGroups;
			for(let index = visibleGroups.length - 1; index >= 0; index--) {
				const group = world.groups.getByIndex(visibleGroups[index]);
				if(!group) { continue; }

				for(let structureIndex = group.structures.length - 1; structureIndex >= 0; structureIndex--) {
					const structure = group.structures[structureIndex];
					structure.position.toVector3(position);
					position.y = 1;
					this.getRenderable(this.structureRenderables[structure.type]).render(position, color);
				}
			}
		}

		Transients.pop(Vector3);
	}
}
