import { Vector2 } from 'three';
import Transients from './transients';

const keys: { [key: string]: number } = {
	left: 37,
	up: 38,
	right: 39,
	down: 40,
	space: 32,
	pageup: 33,
	pagedown: 34,
	tab: 9,
	escape: 27,
};

for(let i = 65; i <= 90; i++) {
	keys[String.fromCharCode(i)] = i;
}

class KeyboardInput {
	private keysPressed: boolean[] = new Array<boolean>(128).fill(false);
	private keysDown: boolean[] = new Array<boolean>(128).fill(false);
	private keysReleased: boolean[] = new Array<boolean>(128).fill(false);

	constructor() {
		document.addEventListener('keydown', this.onKeyDown.bind(this), false);
		document.addEventListener('keyup', this.onKeyUp.bind(this), false);
	}

	public update(): void {
		this.keysPressed.fill(false);
		this.keysReleased.fill(false);
	}

	public pressed(key: string): boolean {
		if(!(key in keys)) { throw new Error(`Invalid key ${key}`); }
		return this.keysPressed[keys[key]];
	}

	public down(key: string): boolean {
		if(!(key in keys)) { throw new Error(`Invalid key ${key}`); }
		return this.keysDown[keys[key]];
	}

	public released(key: string): boolean {
		if(!(key in keys)) { throw new Error(`Invalid key ${key}`); }
		return this.keysReleased[keys[key]];
	}

	private onKeyDown(event: KeyboardEvent): void {
		this.keysPressed[event.keyCode] = true;
		this.keysDown[event.keyCode] = true;
	}

	private onKeyUp(event: KeyboardEvent): void {
		this.keysReleased[event.keyCode] = true;
		this.keysDown[event.keyCode] = false;
	}
}

const buttons: { [key: string]: number } = {
	left: 0,
	middle: 1,
	right: 2,
};

class MouseInput {
	private buttonsPressed: boolean[] = new Array<boolean>(3).fill(false);
	private buttonsDown: boolean[] = new Array<boolean>(3).fill(false);
	private buttonsReleased: boolean[] = new Array<boolean>(3).fill(false);
	private buttonsDragStart: Vector2[] = new Array(3).fill(null).map(() => new Vector2());
	private lastPosition: Vector2 = new Vector2();
	private nextPosition: Vector2 = new Vector2();
	private positionDelta: Vector2 = new Vector2();
	private currentPosition: Vector2 = new Vector2();
	private scrollDelta = 0;
	private bounds: { x: number, y: number, width: number, height: number } = { x: 0, y: 0, width: 0, height: 0 };

	constructor() {
		this.resetPosition();
		document.addEventListener('mousedown', this.onMouseDown.bind(this), false);
		document.addEventListener('contextmenu', this.onMouseDown.bind(this), false);
		document.addEventListener('mouseup', this.onMouseUp.bind(this), false);
		document.addEventListener('mousemove', this.onMouseMove.bind(this), false);
		document.addEventListener('wheel', this.onMouseWheel.bind(this), false);
		document.addEventListener('mouseout', this.onMouseOut.bind(this), false);
	}

	public setBounds(x: number, y: number, width: number, height: number) {
		this.bounds = { x, y, width, height };
	}

	public pressed(button: string): boolean {
		if(!(button in buttons)) { throw new Error(`Invalid button ${button}`); }
		return this.buttonsPressed[buttons[button]];
	}

	public down(button: string): boolean {
		if(!(button in buttons)) { throw new Error(`Invalid button ${button}`); }
		return this.buttonsDown[buttons[button]];
	}

	public released(button: string): boolean {
		if(!(button in buttons)) { throw new Error(`Invalid button ${button}`); }
		return this.buttonsReleased[buttons[button]];
	}

	public dragging(button: string, minDragDistance = 10): boolean {
		if(!(button in buttons)) { throw new Error(`Invalid button ${button}`); }
		return (this.buttonsDown[buttons[button]] || this.buttonsReleased[buttons[button]]) && this.currentPosition.distanceTo(this.buttonsDragStart[buttons[button]]) > minDragDistance;
	}

	public dragStart(button: string): Vector2 {
		if(!(button in buttons)) { throw new Error(`Invalid button ${button}`); }
		return Transients.get(Vector2).copy(this.buttonsDragStart[buttons[button]]);
	}

	public movement(): Vector2 {
		return Transients.get(Vector2).copy(this.positionDelta);
	}

	public position(): Vector2 {
		return Transients.get(Vector2).copy(this.currentPosition);
	}

	public scroll(): number {
		return this.scrollDelta;
	}

	public update(): void {
		this.buttonsPressed.fill(false);
		this.buttonsReleased.fill(false);
		this.positionDelta.set(0, 0);
		this.scrollDelta = 0;
	}

	private onMouseDown(event: MouseEvent): void {
		this.buttonsDragStart[event.button].copy(this.currentPosition);
		this.buttonsPressed[event.button] = true;
		this.buttonsDown[event.button] = true;
		if(this.isOnInterface(event)) { return; }
		if(event.button === buttons.right) { event.preventDefault(); }
	}

	private onMouseUp(event: MouseEvent): void {
		this.buttonsReleased[event.button] = true;
		this.buttonsDown[event.button] = false;
	}

	private onMouseMove(event: MouseEvent): void {
		if(this.isOnInterface(event)) {
			this.resetPosition();
			return;
		}
		this.currentPosition.set(event.clientX, event.clientY);
		this.currentPosition.x -= this.bounds.x;
		this.currentPosition.y -= this.bounds.y;
		if(this.currentPosition.x < 0 || this.currentPosition.y < 0 || this.currentPosition.x > this.bounds.width || this.currentPosition.y > this.bounds.height) {
			this.resetPosition();
		}
		if(!isNaN(this.lastPosition.x) && !isNaN(this.lastPosition.y)) {
			this.nextPosition.copy(this.currentPosition);
			this.nextPosition.sub(this.lastPosition);
			this.positionDelta.add(this.nextPosition);
		}
		this.lastPosition.copy(this.currentPosition);
	}

	private onMouseWheel(event: WheelEvent): void {
		if(this.isOnInterface(event)) { return; }
		this.scrollDelta += event.deltaY > 0 ? 1 : -1;
	}

	private onMouseOut(event: MouseEvent): void {
		if(event.relatedTarget === null) {
			this.resetPosition();
		}
	}

	private resetPosition(): void {
		this.currentPosition.set(NaN, NaN);
		this.lastPosition.set(NaN, NaN);
	}

	private isOnInterface(event: MouseEvent) {
		return event.target && event.target instanceof Element && event.target.id !== 'interface';
	}
}

export default {
	keyboard: new KeyboardInput(),
	mouse: new MouseInput(),
};
