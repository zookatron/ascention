import { Vector2, Vector4, BufferGeometry, BufferAttribute, Texture, RepeatWrapping, DataArrayTexture, RGBAFormat } from 'three';
import { mergeBufferGeometries } from 'three/examples/jsm/utils/BufferGeometryUtils';
import Transients from './transients';
import { COS30, SIN30 } from './constants';
import { HexVector } from './hexagon';
import { Renderer2D } from './renderer2D';
import { Renderable } from './renderables';
import { View } from './view';
import { World, TerrainType } from './world';
import { Textures } from './assets';

export class TerrainRenderable extends Renderable {
	protected vertexShader = `
		uniform float height;
		uniform vec2 transform;
		uniform vec2 uvtransform;
		attribute vec2 cellposition;
		attribute vec4 cellattrs;
		attribute vec3 barycentric;
		attribute int triangle;
		attribute vec2 uva;
		attribute vec2 uvb;
		attribute vec2 uvc;
		varying vec3 vbarycentric;
		flat varying vec4 cellcolora;
		flat varying vec4 cellcolorb;
		flat varying vec4 cellcolorc;
		flat varying vec4 cellattrsa;
		flat varying vec4 cellattrsb;
		flat varying vec4 cellattrsc;
		varying vec2 vUv;
		varying vec2 vuva;
		varying vec2 vuvb;
		varying vec2 vuvc;

		vec2 modulate(vec2 pos, vec2 rot) {
			return pos.x * rot + pos.y * vec2(-rot.y, rot.x);
		}

		vec4 unpack4(in float i) {
			return mod(floor(vec4(i, i / 64.0, i / 4096.0, i / 262144.0)), 64.0) / 63.0;
		}

		vec3 hsv2rgb(vec3 c) {
			vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
			vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
			return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
		}

		vec3 toColor(float x) {
			return x == 0.0 ? vec3(1.0) : hsv2rgb(vec3(x, 1, 1));
		}

		#include <std_vert_parameters>

		void main() {
			#include <std_vert_color>
			#include <std_vert_normal>
			#include <std_vert_position>

			vec4 cell1attrs = unpack4(cellattrs.x);
			vec4 cell2attrs = unpack4(cellattrs.y);
			vec4 cell3attrs = unpack4(cellattrs.z);
			vec4 cell4attrs = unpack4(cellattrs.w);
			vec3 cell1color = toColor(cell1attrs.x);
			vec3 cell2color = toColor(cell2attrs.x);
			vec3 cell3color = toColor(cell3attrs.x);
			vec3 cell4color = toColor(cell4attrs.x);
			if(triangle == 0) {
				cellcolora = vec4(cell1color.r, cell1color.g, cell1color.b, 1);
				cellcolorb = vec4(cell4color.r, cell4color.g, cell4color.b, 1);
				cellcolorc = vec4(cell2color.r, cell2color.g, cell2color.b, 1);
				cellattrsa = cell1attrs;
				cellattrsb = cell4attrs;
				cellattrsc = cell2attrs;
			} else {
				cellcolora = vec4(cell1color.r, cell1color.g, cell1color.b, 1);
				cellcolorb = vec4(cell3color.r, cell3color.g, cell3color.b, 1);
				cellcolorc = vec4(cell4color.r, cell4color.g, cell4color.b, 1);
				cellattrsa = cell1attrs;
				cellattrsb = cell3attrs;
				cellattrsc = cell4attrs;
			}

			vuva = modulate(uva, uvtransform) + vec2(0.5, 0.5);
			vuvb = modulate(uvb, uvtransform) + vec2(0.5, 0.5);
			vuvc = modulate(uvc, uvtransform) + vec2(0.5, 0.5);
			vbarycentric = barycentric;
			// vUv = uv; // (cellposition + transformed.xz) * 0.5;
			vec2 transformed2d = modulate(cellposition, transform) + modulate(transformed.xz, transform);
			transformed = vec3(transformed2d.x, height, transformed2d.y);

			#include <std_vert_extras>
		}
	`;

	protected fragmentShader = `
		varying vec3 vbarycentric;
		flat varying vec4 cellcolora;
		flat varying vec4 cellcolorb;
		flat varying vec4 cellcolorc;
		flat varying vec4 cellattrsa;
		flat varying vec4 cellattrsb;
		flat varying vec4 cellattrsc;
		varying vec2 vUv;
		varying vec2 vuva;
		varying vec2 vuvb;
		varying vec2 vuvc;

		precision lowp sampler2DArray;
		uniform sampler2DArray textures;
		uniform float transparency;

		// vec3 textureNoTile(in sampler2D sampler, in vec2 uv) {
		// 	float k = texture2D(noisemap, 0.05*uv).x;

		// 	vec2 duvdx = dFdx(uv);
		// 	vec2 duvdy = dFdy(uv);

		// 	float l = k*8.0;
		// 	float i = floor(l);
		// 	float f = fract(l);

		// 	vec2 offa = sin(vec2(3.0,7.0)*(i+0.0));
		// 	vec2 offb = sin(vec2(3.0,7.0)*(i+1.0));

		// 	vec3 cola = texture2DGradEXT(sampler, uv + offa, duvdx, duvdy).xyz;
		// 	vec3 colb = texture2DGradEXT(sampler, uv + offb, duvdx, duvdy).xyz;

		// 	vec3 colabdiff = cola-colb;
		// 	return mix(cola, colb, smoothstep(0.2,0.8,f-0.1*(colabdiff.x+colabdiff.y+colabdiff.z)));
		// }

		#include <std_frag_parameters>

		void main() {
			#include <std_frag_color>

			vec4 cellcolor;
			vec4 cellattrs;
			vec2 uv;
			if(vbarycentric.x > vbarycentric.y && vbarycentric.x > vbarycentric.z) {
				cellcolor = cellcolora;
				cellattrs = cellattrsa;
				uv = vuva;
			} else if(vbarycentric.y > vbarycentric.x && vbarycentric.y > vbarycentric.z) {
				cellcolor = cellcolorb;
				cellattrs = cellattrsb;
				uv = vuvb;
			} else {
				cellcolor = cellcolorc;
				cellattrs = cellattrsc;
				uv = vuvc;
			}

			// diffuseColor.rgb = cellattrs.x == 0.0 ? textureNoTile(grassmap, vUv) : textureNoTile(stonemap, vUv);
			diffuseColor.rgb = texture(textures, vec3(uv, cellattrs.x * 63.0)).rgb;
			diffuseColor.a = 1.0;
			// diffuseColor.rgb *= cellcolor.rgb;

			float width = 0.01;
			float xy = abs(vbarycentric.x-vbarycentric.y) < width && vbarycentric.x > vbarycentric.z ? 1.0 : 0.0;
			float xz = abs(vbarycentric.x-vbarycentric.z) < width && vbarycentric.x > vbarycentric.y ? 1.0 : 0.0;
			float zy = abs(vbarycentric.z-vbarycentric.y) < width && vbarycentric.z > vbarycentric.x ? 1.0 : 0.0;
			vec3 line = xy+xz+zy > 0.0 ? vec3(-0.5) : vec3(0.0);
			diffuseColor.rgb += line;

			gl_FragColor = vec4(diffuseColor.rgb, transparency);
		}
	`;

	constructor(textures: DataArrayTexture, order: number, max_instances: number) {
		super();

		const geometry = mergeBufferGeometries([makeUVs(TriangleGeometry(2 * COS30)), makeUVs2(TriangleGeometry(2 * COS30).rotateY(-Math.PI/3))], false);
		geometry.setAttribute('triangle', new BufferAttribute(new Int32Array([0, 0, 0, 1, 1, 1]), 1));

		this.initialize(TerrainRenderable, geometry, max_instances, [
			{ cellposition: Vector2 },
			{ cellattrs: Vector4 },
		], {
			textures,
			transparency: 1,
			transform: new Vector2(1, 0),
			uvtransform: new Vector2(1, 0),
			height: 0,
			roughness: 1,
			metalness: 0,
		}, {}, {
			renderOrder: order,
		});
	}
}

function makeUVs(geometry: BufferGeometry) {
	const SQRT_THREE = Math.sqrt(3);
	const x = COS30 * 0.5 * SQRT_THREE;
	const y = SIN30 * 0.5 * SQRT_THREE;

	const uva = new Float32Array(6);
	uva[0] = 0;
	uva[1] = 0;
	uva[2] = x;
	uva[3] = -y;
	uva[4] = x;
	uva[5] = y;

	const uvb = new Float32Array(6);
	uvb[0] = -x;
	uvb[1] = -y;
	uvb[2] = 0;
	uvb[3] = -2 * y;
	uvb[4] = 0;
	uvb[5] = 0;

	const uvc = new Float32Array(6);
	uvc[0] = -x;
	uvc[1] = y;
	uvc[2] = 0;
	uvc[3] = 0;
	uvc[4] = 0;
	uvc[5] = 2 * y;

	geometry.setAttribute('uva', new BufferAttribute(uva, 2));
	geometry.setAttribute('uvb', new BufferAttribute(uvb, 2));
	geometry.setAttribute('uvc', new BufferAttribute(uvc, 2));
	return geometry;
}

function makeUVs2(geometry: BufferGeometry) {
	const SQRT_THREE = Math.sqrt(3);
	const x = COS30 * 0.5 * SQRT_THREE;
	const y = SIN30 * 0.5 * SQRT_THREE;

	const uva = new Float32Array(6);
	uva[0] = 0;
	uva[1] = 0;
	uva[2] = x;
	uva[3] = y;
	uva[4] = 0;
	uva[5] = 2 * y;

	const uvb = new Float32Array(6);
	uvb[0] = 0;
	uvb[1] = -2 * y;
	uvb[2] = x;
	uvb[3] = -y;
	uvb[4] = 0;
	uvb[5] = 0;

	const uvc = new Float32Array(6);
	uvc[0] = -x;
	uvc[1] = -y;
	uvc[2] = 0;
	uvc[3] = 0;
	uvc[4] = -x;
	uvc[5] = y;

	geometry.setAttribute('uva', new BufferAttribute(uva, 2));
	geometry.setAttribute('uvb', new BufferAttribute(uvb, 2));
	geometry.setAttribute('uvc', new BufferAttribute(uvc, 2));
	return geometry;
}

function TriangleGeometry(scale = 1, parts = 1): BufferGeometry {
	const verts = (parts + 1) * (parts + 2) / 2;
	const faces = parts * (parts + 1) / 2;

	const positions = new Float32Array(3 * verts);
	const uvs = new Float32Array(2 * verts);
	const normals = new Float32Array(3 * verts);
	const barycentrics = new Float32Array(3 * verts);
	const indexes = new Array<number>(3 * faces);

	let vert = 0;
	let face = 0;

	const pos0x = 0, pos0y = 0;
	const pos1x = scale * COS30, pos1y = scale * SIN30;
	const pos2x = scale * COS30, pos2y = scale * -SIN30;

	const uv0x = 0, uv0y = 0.5;
	const uv1x = COS30, uv1y = 1;
	const uv2x = COS30, uv2y = 0;

	for(let x = 0; x <= parts; x++) {
		for(let y = 0; y <= parts - x; y++) {
			const baryx = 1 - (x + y) / parts;
			const baryy = x / parts;
			const baryz = y / parts;

			barycentrics[vert * 3 + 0] = baryx;
			barycentrics[vert * 3 + 1] = baryy;
			barycentrics[vert * 3 + 2] = baryz;

			positions[vert * 3 + 0] = baryx * pos0x + baryy * pos1x + baryz * pos2x;
			positions[vert * 3 + 1] = 0;
			positions[vert * 3 + 2] = baryx * pos0y + baryy * pos1y + baryz * pos2y;

			uvs[vert * 2 + 0] = baryx * uv0x + baryy * uv1x + baryz * uv2x;
			uvs[vert * 2 + 1] = baryx * uv0y + baryy * uv1y + baryz * uv2y;

			normals[vert * 3 + 0] = 0;
			normals[vert * 3 + 1] = 1;
			normals[vert * 3 + 2] = 0;

			if(x + y < parts - 1) {
				indexes[face * 3 + 0] = x * (parts + 1) - (x) * (x - 1) / 2  +  y;
				indexes[face * 3 + 1] = (x + 1) * (parts + 1) - (x + 1) * (x) / 2  +  y;
				indexes[face * 3 + 2] = x * (parts + 1) - (x) * (x - 1) / 2  +  y  +  1;
				indexes[face * 3 + 3] = x * (parts + 1) - (x) * (x - 1) / 2  +  y  +  1;
				indexes[face * 3 + 4] = (x + 1) * (parts + 1) - (x + 1) * (x) / 2  +  y;
				indexes[face * 3 + 5] = (x + 1) * (parts + 1) - (x + 1) * (x) / 2  +  y  +  1;
				face += 2;
			} else if(x + y < parts) {
				indexes[face * 3 + 0] = x * (parts + 1) - (x) * (x - 1) / 2  +  y;
				indexes[face * 3 + 1] = (x + 1) * (parts + 1) - (x + 1) * (x) / 2  +  y;
				indexes[face * 3 + 2] = x * (parts + 1) - (x) * (x - 1) / 2  +  y  +  1;
				face += 1;
			}

			vert += 1;
		}
	}

	const geometry = new BufferGeometry();
	geometry.setIndex(indexes);
	geometry.setAttribute('position', new BufferAttribute(positions, 3));
	geometry.setAttribute('uv', new BufferAttribute(uvs, 2));
	geometry.setAttribute('normal', new BufferAttribute(normals, 3));
	geometry.setAttribute('barycentric', new BufferAttribute(barycentrics, 3));

	return geometry;
}

export class TerrainRenderer {
	private lowerRenderable!: TerrainRenderable;
	private upperRenderable!: TerrainRenderable;

	public async load(renderer: Renderer2D): Promise<void> {
		const maxInstances = 20000;
		const textures = await this.loadTextures();
		this.lowerRenderable = new TerrainRenderable(textures, 2, maxInstances);
		this.upperRenderable = new TerrainRenderable(textures, 1, maxInstances);
		renderer.addRenderable(this.lowerRenderable);
		renderer.addRenderable(this.upperRenderable);
	}

	private async loadTextures() {
		const types = [];
		for(let type = 0; TerrainType[type]; type++) {
			types.push(type);
		}

		const array = new Uint8Array(256 * 256 * 4 * types.length);
		await Promise.all(types.map(type => Textures.load(Textures[TerrainType[type]]).then(texture => {
			const canvas = document.createElement('canvas');
			canvas.width = 256;
			canvas.height = 256;
			const context = canvas.getContext('2d');
			if(!context) { throw new Error('Unable to create context for texture processing!'); }
			context.drawImage(texture.image, 0, 0, 256, 256);
			array.set(context.getImageData(0, 0, 256, 256).data, 256 * 256 * 4 * type);
		})));

		const textures = new DataArrayTexture(array, 256, 256, types.length);
		textures.format = RGBAFormat;
		textures.needsUpdate = true;
		textures.wrapS = RepeatWrapping;
		textures.wrapT = RepeatWrapping;
		return textures;
	}

	public render(world: World, view: View): void {
		this.renderGroups(
			this.lowerRenderable,
			world,
			view.lowerLevel,
			view.lowerVisibleGroups,
			view.lowerOffset,
			view.lowerTransform,
			1 - Math.pow(1 - view.lowerLevelRatio, 8),
			1,
			view,
		);
		this.renderGroups(
			this.upperRenderable,
			world, view.upperLevel,
			view.upperVisibleGroups,
			view.upperOffset,
			view.upperTransform,
			1,
			0,
			view,
		);
	}

	private renderGroups(
		renderable: TerrainRenderable,
		world: World,
		level: number,
		visibleGroups: number[],
		offset: HexVector,
		transform: Vector2,
		transparency: number,
		height: number,
		view: View,
	): void {
		renderable.uniform('transparency', transparency);
		renderable.uniform('transform', transform);
		renderable.uniform('uvtransform', Transients.get(Vector2).copy(transform).normalize());
		renderable.uniform('height', height);

		if(level >= 0) {
			for(let index = visibleGroups.length - 1; index >= 0; index--) {
				// const group = world.groups.getByIndex(visibleGroups[index]);
				// if(!group) { continue; }
				this.renderGroup(visibleGroups[index], offset, view, renderable);
			}
		}
	}

	private renderGroup(index: number, offset: HexVector, view: View, renderable: TerrainRenderable): void {
		Transients.push(HexVector);
		Transients.push(Vector2);
		Transients.push(Vector4);

		const hex = Transients.get(HexVector);
		const temphex = Transients.get(HexVector);
		const position = Transients.get(Vector2);
		const cellattrs = Transients.get(Vector4);

		const off00 = Transients.get(HexVector).set(0, 0);
		const off01 = Transients.get(HexVector).set(-1, 1);
		const off10 = Transients.get(HexVector).set(1, 0);
		const off11 = Transients.get(HexVector).set(0, 1);

		for(let cellindex = view.VISIBILITY_GROUP_LENGTH - 1; cellindex >= 0; cellindex--) {
			//hex.fromIndex(cellindex).add(group.center);
			// const cell00 = world.grid.get(temphex.copy(hex).add(off00));
			// if(!cell00) { continue; }
			// const cell01 = world.grid.get(temphex.copy(hex).add(off01));
			// if(!cell01) { continue; }
			// const cell10 = world.grid.get(temphex.copy(hex).add(off10));
			// if(!cell10) { continue; }
			// const cell11 = world.grid.get(temphex.copy(hex).add(off11));
			// if(!cell11) { continue; }
			hex.fromIndex(cellindex).add(temphex.fromIndex(index).antituple(view.VISIBILITY_GROUP_SIZE));

			const cell00group = temphex.copy(hex).add(offset).add(off00).tuple(view.LEVEL_SIZE).round().toIndex();
			const cell01group = temphex.copy(hex).add(offset).add(off01).tuple(view.LEVEL_SIZE).round().toIndex();
			const cell10group = temphex.copy(hex).add(offset).add(off10).tuple(view.LEVEL_SIZE).round().toIndex();
			const cell11group = temphex.copy(hex).add(offset).add(off11).tuple(view.LEVEL_SIZE).round().toIndex();

			hex.toVector2(position);
			cellattrs.set(
				// this.pack4(cell00group % 63 / 63, 0, 0, 0),
				// this.pack4(cell01group % 63 / 63, 0, 0, 0),
				// this.pack4(cell10group % 63 / 63, 0, 0, 0),
				// this.pack4(cell11group % 63 / 63, 0, 0, 0),
				this.pack4(cell00group % 5 / 63, 0, 0, 0),
				this.pack4(cell01group % 5 / 63, 0, 0, 0),
				this.pack4(cell10group % 5 / 63, 0, 0, 0),
				this.pack4(cell11group % 5 / 63, 0, 0, 0),
			);
			renderable.render(position, cellattrs);
		}

		Transients.pop(HexVector);
		Transients.pop(Vector2);
		Transients.pop(Vector4);
	}

	private pack4(a: number, b: number, c: number, d: number): number {
		const x = Math.floor(Math.max(0, Math.min(1, a)) * 63);
		const y = Math.floor(Math.max(0, Math.min(1, b)) * 63);
		const z = Math.floor(Math.max(0, Math.min(1, c)) * 63);
		const w = Math.floor(Math.max(0, Math.min(1, d)) * 63);
		return x + y * 64 + z * 4096 + w * 262144;
	}
}
