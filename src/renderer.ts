import { Vector2, Vector3, Vector4, Quaternion, Ray, Plane, Frustum, Color, WebGLRenderer, PerspectiveCamera, Scene, DirectionalLight, Group,
	Object3D, Mesh, LineSegments, EdgesGeometry, BufferGeometry, TextureLoader, Texture, AmbientLight, PlaneBufferGeometry, Sprite, Sphere,
	SpriteMaterial, MeshStandardMaterial, MeshBasicMaterial, LineBasicMaterial, DoubleSide, PCFSoftShadowMap, Matrix4, Material } from 'three';
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader';
import Transients from './transients';
import Input from './input';
import { ModelType, ModelSpecs, ModelSpec, Renderable, RenderableID, MeshRenderable, SpriteRenderable, Model, ModelNode } from './renderables';
import { TerrainRenderer } from './terrain';
import { HexVector, hexCircleArea, hexRay } from './hexagon';
import { StructureType } from './structure';
import { World, WorldSpecs } from './world';
import { HexGroup, getGridHeight, HEX_GROUP_SIZE } from './grid';
import { lerp } from './utilities';

Transients.register(Vector2, 8000); // TODO: Need to optimize transient use in precalculateVisibilityList
Transients.register(Vector3, 1000);
Transients.register(Vector4, 1000);
Transients.register(Quaternion, 1000);
Transients.register(Ray, 100);
Transients.register(Plane, 100);
Transients.register(Frustum, 100);
Transients.register(Color, 100);
Transients.register(Matrix4, 100);
Transients.register(Sphere, 10);

export class Renderer {
	private renderer: WebGLRenderer;
	private camera: PerspectiveCamera;
	private light: DirectionalLight;
	private scene: Scene;
	private overlayScene: Scene;

	private renderables: Renderable[] = [];
	private models: Map<ModelType, Model> = new Map<ModelType, Model>();

	private terrainRenderer: TerrainRenderer;

	// private buildingPreviewModel: Model | null = null;
	// private buildingPreviewType: StructureType | null = null;
	// private buildingPreviewModelSize: number = 0;

	private dragselector!: Group;
	private cellselector!: Group;
	private cellselectorSpeed = 0;

	private textureLoader: TextureLoader;
	private gltfloader: GLTFLoader;

	private MAX_VIEW_DISTANCE = 100;
	private visibilityGroups: number[] = [];

	constructor(container: HTMLElement) {
		this.textureLoader = new TextureLoader();
		this.gltfloader = new GLTFLoader();

		this.scene = new Scene();
		this.overlayScene = new Scene();

		this.camera = new PerspectiveCamera(30, window.innerWidth / window.innerHeight, 1, 500);
		this.camera.position.set(0, 20, 20);
		this.camera.lookAt(this.scene.position);
		this.camera.rotation.reorder('YXZ');

		this.renderer = new WebGLRenderer({ antialias: true });
		this.renderer.setPixelRatio(window.devicePixelRatio);
		// this.renderer.setPixelRatio(2);
		this.renderer.shadowMap.enabled = true;
		this.renderer.shadowMap.type = PCFSoftShadowMap;
		this.renderer.autoClear = false;
		container.appendChild(this.renderer.domElement);

		window.addEventListener('resize', this.onResize.bind(this), false);
		this.onResize();

		this.light = new DirectionalLight(0xffffff);
		this.light.position.set(0, 0, 0);
		this.light.target.position.set(-30, -50, -30);
		this.scene.add(this.light.target);
		this.light.castShadow = true;
		this.scene.add(this.light);

		this.light.shadow.mapSize.width = 2048;
		this.light.shadow.mapSize.height = 2048;
		this.light.shadow.camera.near = 10;
		this.light.shadow.camera.far = 100;
		this.light.shadow.camera.top = 20;
		this.light.shadow.camera.bottom = -20;
		this.light.shadow.camera.left = -20;
		this.light.shadow.camera.right = 20;

		this.scene.add(new AmbientLight(0xffffff, 0.2));

		this.terrainRenderer = new TerrainRenderer();

		// this.pointer = new Group();
		// let mesh = new Mesh(new CylinderBufferGeometry(0, 0.3, 1), new MeshStandardMaterial({ metalness: 0, roughness: 1, color: 0xffff00 }));
		// mesh.position.set(0, 0.5, 0);
		// mesh.rotation.x = Math.PI;
		// mesh.castShadow = true;
		// mesh.receiveShadow = true;
		// this.pointer.add(mesh);
		// this.scene.add(this.pointer);

		// this.points = new Array<Mesh>();
		// for(let i = 0; i < 60; i++) {
		// 	let point = new Mesh(new SphereBufferGeometry(0.1), new MeshStandardMaterial({ metalness: 0, roughness: 1, color: 0xff0000 }));
		// 	point.position.set(0, 0, 0);
		// 	point.castShadow = true;
		// 	point.receiveShadow = true;
		// 	this.scene.add(point);
		// 	this.points.push(point);
		// }

		// this.laser = new Mesh(
		// 	new PlaneBufferGeometry(2, 2),
		// 	new MeshBasicMaterial({ color: 0xff0000, transparent: true, opacity: 0.7, side: DoubleSide }),
		// );
		// this.laser.frustumCulled = false;
	}

	public load(): Promise<void> {
		const loadHealthbar = Promise.all([this.loadTexture('assets/textures/healthbg.png'), this.loadTexture('assets/textures/healthbar.png')]).then(([bgmap, barmap]) => {
			const healthbar_bg = new Sprite(new SpriteMaterial({ map: bgmap, transparent: true, color: 0xffffff }));
			healthbar_bg.position.y = 0.3;
			healthbar_bg.position.z = -0.2;
			healthbar_bg.scale.y = 0.2 * 0.6;
			healthbar_bg.scale.x = 0.6;

			const healthbar_bar = new Sprite(new SpriteMaterial({ map: barmap, transparent: true, color: 0xffffff }));
			healthbar_bar.position.y = 0.3;
			healthbar_bar.position.z = -0.2;
			healthbar_bar.scale.y = 0.2 * 0.6;
			healthbar_bar.scale.x = 0.6;

			const healthbar = new Group();
			healthbar.add(healthbar_bar);
			healthbar.add(healthbar_bg);

			this.addModel(ModelType.HealthBar, healthbar);
		});

		const loadUnitSelector = this.loadTexture('assets/textures/selector.png').then(map => {
			const unitselectorgeometry = new PlaneBufferGeometry(1.9 * 0.333, 1.9 * 0.333);
			unitselectorgeometry.rotateX(-Math.PI / 2);
			unitselectorgeometry.translate(0, 0.2 * 0.333, 0);
			const unitselectormaterial = new MeshStandardMaterial({ map, metalness: 0, roughness: 1, opacity: 0.7, transparent: true, depthWrite: false });
			const unitselectormesh = new Mesh(unitselectorgeometry, unitselectormaterial);
			const unitselector = new Group();
			unitselector.add(unitselectormesh);
			this.addModel(ModelType.UnitSelector, unitselector);
		});

		const loadDragSelector = Promise.resolve().then(() => {
			const dragselector = new Mesh(
				new PlaneBufferGeometry(2, 2),
				new MeshBasicMaterial({ color: 0xffffff, transparent: true, opacity: 0.4, side: DoubleSide }),
			);
			dragselector.frustumCulled = false;

			const dragselectoroutline = new LineSegments(new EdgesGeometry(dragselector.geometry as BufferGeometry, 1), new LineBasicMaterial({color: 0xffffff, linewidth: 2}));
			dragselectoroutline.frustumCulled = false;

			this.dragselector = new Group();
			this.dragselector.add(dragselector);
			this.dragselector.add(dragselectoroutline);
			this.overlayScene.add(this.dragselector);
			this.dragselector.frustumCulled = false;
		});

		const loadCellSelector = this.loadTexture('assets/textures/hexagon.png').then(map => {
			const cellselector = new Mesh(
				new PlaneBufferGeometry(1.9, 1.9),
				new MeshBasicMaterial({ map, color: 0xff0000, transparent: true, opacity: 0.7 }),
			);
			cellselector.rotation.x = -Math.PI * 0.5;
			cellselector.position.y = 0.1;
			this.cellselector = new Group();
			this.cellselector.add(cellselector);
			this.scene.add(this.cellselector);
			this.cellselector.visible = false;
		});

		const loadModels = Promise.resolve().then(() => {
			let promise = Promise.resolve();
			for(const typename in ModelType) {
				const type = ModelType[typename] as unknown as number;
				const spec: ModelSpec = ModelSpecs[type];
				if(!spec) { continue; }
				promise = promise.then(() => {
					return this.loadModel(spec.url).then(group => {
						group.traverse(child => {
							if(!(child instanceof Mesh)) { return; }
							child.castShadow = true;
							child.receiveShadow = true;
						});
						const model = this.addModel(type, group);
						for(const childname in model.children) {
							const child = model.children[childname];
							child.scale.multiplyScalar(spec.scale);
							child.position.add(spec.offset);
							child.rotation.multiply(spec.rotation);
						}
					});
				});
			}
			return promise;
		});

		return Promise.all([
			loadHealthbar,
			loadUnitSelector,
			loadDragSelector,
			loadCellSelector,
			loadModels,
		])
		.then(() => this.terrainRenderer.load(this, this.light))
		// eslint-disable-next-line @typescript-eslint/no-empty-function
		.then(() => {}, error => console.error(error));
	}

	public loadTexture(url: string): Promise<Texture> {
		return new Promise((resolve, reject) => {
			return this.textureLoader.load(url, resolve, undefined, reject);
		});
	}

	public addRenderable(renderable: Renderable): RenderableID {
		renderable.id = this.renderables.length;
		this.scene.add(renderable.getObject());
		this.renderables.push(renderable);
		return renderable.id;
	}

	public getRenderable(id: RenderableID): Renderable {
		if(!this.renderables[id]) { throw new Error(`Unknown renderable "${id}"`); }
		return this.renderables[id];
	}

	public getModel(type: ModelType): Model {
		const original = this.models.get(type);
		if(!original) { throw new Error(`Unknown model "${type}"`); }
		const model = new Model();
		model.copy(original);
		return model;
	}

	public getMaxInstances(): number {
		return hexCircleArea(this.MAX_VIEW_DISTANCE);
	}

	public size(): Vector2 | null {
		if(isNaN(this.renderer.domElement.clientWidth) && isNaN(this.renderer.domElement.clientHeight)) { return null; }
		const result = Transients.get(Vector2);
		result.set(this.renderer.domElement.clientWidth, this.renderer.domElement.clientHeight);
		return result;
	}

	public ray(position: Vector2): Ray | null {
		if(isNaN(position.x) || isNaN(position.y)) { return null; }
		Transients.push(Vector2);
		const coords = Transients.get(Vector2);
		const size = this.size();
		Transients.pop(Vector2);
		if(!size) { return null; }
		coords.set((position.x / size.x) * 2 - 1, 1 - (position.y / size.y) * 2);
		const ray = Transients.get(Ray);
		ray.origin.set(coords.x, coords.y, -1).unproject(this.camera);
		ray.direction.set(coords.x, coords.y, 1).unproject(this.camera).sub(ray.origin).normalize();
		if(isNaN(ray.direction.x) || isNaN(ray.direction.y) || isNaN(ray.direction.z)) { return null; }
		return ray;
	}

	public frustum(left: number, top: number, right: number, bottom: number): Frustum | null {
		const temp = Transients.get(Vector2);

		if(left > right) { [left, right] = [right, left]; }
		if(top > bottom) { [top, bottom] = [bottom, top]; }

		const topleftray = this.ray(temp.set(left, top));
		const toprightray = this.ray(temp.set(right, top));
		const bottomleftray = this.ray(temp.set(left, bottom));
		const bottomrightray = this.ray(temp.set(right, bottom));

		if(topleftray && toprightray && bottomleftray && bottomrightray) {
			const neartopleft = Transients.get(Vector3).copy(topleftray.origin);
			const neartopright = Transients.get(Vector3).copy(toprightray.origin);
			const nearbottomleft = Transients.get(Vector3).copy(bottomleftray.origin);
			const nearbottomright = Transients.get(Vector3).copy(bottomrightray.origin);
			const fartopleft = Transients.get(Vector3).copy(topleftray.origin).add(topleftray.direction.multiplyScalar(this.camera.far - this.camera.near));
			const fartopright = Transients.get(Vector3).copy(toprightray.origin).add(toprightray.direction.multiplyScalar(this.camera.far - this.camera.near));
			const farbottomleft = Transients.get(Vector3).copy(bottomleftray.origin).add(bottomleftray.direction.multiplyScalar(this.camera.far - this.camera.near));
			const farbottomright = Transients.get(Vector3).copy(bottomrightray.origin).add(bottomrightray.direction.multiplyScalar(this.camera.far - this.camera.near));

			return Transients.get(Frustum).set(
				Transients.get(Plane).setFromCoplanarPoints(neartopright, neartopleft, fartopleft),
				Transients.get(Plane).setFromCoplanarPoints(neartopleft, nearbottomleft, farbottomleft),
				Transients.get(Plane).setFromCoplanarPoints(nearbottomleft, nearbottomright, farbottomright),
				Transients.get(Plane).setFromCoplanarPoints(nearbottomright, neartopright, fartopright),
				Transients.get(Plane).setFromCoplanarPoints(nearbottomright, nearbottomleft, neartopleft),
				Transients.get(Plane).setFromCoplanarPoints(fartopright, fartopleft, farbottomleft),
			);
		}

		return null;
	}

	public render(time: number, delta: number, world: World, zoomOut: number, position: Vector2, currentPointingCell: HexVector | null, selected: number[], dragging: boolean, mousePos: Vector2, dragStart: Vector2, indicating: string, _building: StructureType | null, _buildingGood: boolean) {
		this.updateCamera(world, zoomOut, position);

		this.calculateVisibility(world, this.camera);

		this.updateLights(world);

		this.updateCellSelector(world, indicating, currentPointingCell, delta);

		this.updateDragSelector(currentPointingCell, dragging, mousePos, dragStart);

		// this.updateBuildingPreview(world, currentPointingCell, building, buildingGood);

		this.terrainRenderer.render(world, this, this.renderer, this.scene, this.visibilityGroups, zoomOut < 150);

		// this.renderUnits(world, time, delta, selected, zoomOut < 150);

		this.renderables.forEach(renderable => renderable.update());

		this.renderer.clear();
		this.renderer.render(this.scene, this.camera);
		this.renderer.clearDepth();
		this.renderer.render(this.overlayScene, this.camera);
	}

	// Private Functions

	private loadModel(url: string): Promise<Group> {
		return new Promise((resolve, reject) => {
			this.gltfloader.load(url,
				gltf => resolve(gltf.scene),
				undefined,
				reject,
			);
		});
	}

	private addModel(type: ModelType, group: Group): Model {
		const model = new Model();
		model.fromObject3D(group);
		model.type = type;

		const stack: Array<{ obj: Object3D, node: Model | ModelNode }> = [ { obj: group, node: model }];

		while(true) {
			const next = stack.pop();
			if(!next) { break; }

			for(let index = 0; index < next.obj.children.length; index++) {
				const child = next.obj.children[index];
				const childname = child.name || 'child' + (index + 1).toString();

				if(child instanceof Mesh) {
					const node = new ModelNode();
					const renderable = new MeshRenderable(child as Mesh<BufferGeometry, Material>, this.getMaxInstances());
					this.addRenderable(renderable);
					node.renderable = renderable;
					node.fromObject3D(child);
					next.node.children[childname] = node;
				}

				if(child instanceof Sprite) {
					const node = new ModelNode();
					const renderable = new SpriteRenderable(child, 100);
					this.addRenderable(renderable);
					node.renderable = renderable;
					node.fromObject3D(child);
					next.node.children[childname] = node;
				}

				if(child instanceof Group) {
					const node = new ModelNode();
					node.fromObject3D(child);
					next.node.children[childname] = node;
					stack.push({ obj: child, node });
				}
			}
		}

		this.models.set(type, model);
		return model;
	}

	// private updateBuildingPreview(world: World, currentPointingCell: HexVector | null, building: UnitType | null, buildingGood: boolean) {
	// 	if(currentPointingCell && building) {
	// 		if(building !== this.buildingPreviewType) {
	// 			this.buildingPreviewType = building;
	// 			this.buildingPreviewModel = this.getModel(UnitSpecs[building].modelType);
	// 			this.buildingPreviewModelSize = UnitSpecs[building].size;
	// 		}
	// 		if(!this.buildingPreviewModel) { return; }

	// 		currentPointingCell.toVector3(this.buildingPreviewModel.position);
	// 		this.buildingPreviewModel.color.setHex(buildingGood ? 0x00ff00 : 0xff0000);
	// 		this.buildingPreviewModel.position.y = getGridCircleHeight(world.grid, currentPointingCell, this.buildingPreviewModelSize);
	// 		this.buildingPreviewModel.render();
	// 	} else {
	// 		this.buildingPreviewType = null;
	// 		this.buildingPreviewModel = null;
	// 		this.buildingPreviewModelSize = 0;
	// 	}
	// }

	private updateDragSelector(currentPointingCell: HexVector | null, dragging: boolean, mousePos: Vector2, dragStart: Vector2): void {
		this.dragselector.visible = false;

		if(!dragging) { return; }

		const temp = Transients.get(Vector2);
		const topleftray = this.ray(dragStart);
		const toprightray = this.ray(temp.set(mousePos.x, dragStart.y));
		const bottomleftray = this.ray(temp.set(dragStart.x, mousePos.y));
		const bottomrightray = this.ray(mousePos);

		if(!topleftray || !toprightray || !bottomleftray || !bottomrightray) { return; }

		const dist = 2;
		const topleft = topleftray.origin.add(topleftray.direction.multiplyScalar(dist));
		const topright = toprightray.origin.add(toprightray.direction.multiplyScalar(dist));
		const bottomleft = bottomleftray.origin.add(bottomleftray.direction.multiplyScalar(dist));
		const bottomright = bottomrightray.origin.add(bottomrightray.direction.multiplyScalar(dist));

		const dragselectorpos = (this.dragselector.children[0] as Mesh<BufferGeometry, Material>).geometry.attributes.position;
		const dragselectorposArray = dragselectorpos.array as unknown as Array<number>;
		dragselectorposArray[0] = topleft.x;
		dragselectorposArray[1] = topleft.y;
		dragselectorposArray[2] = topleft.z;
		dragselectorposArray[3] = topright.x;
		dragselectorposArray[4] = topright.y;
		dragselectorposArray[5] = topright.z;
		dragselectorposArray[6] = bottomleft.x;
		dragselectorposArray[7] = bottomleft.y;
		dragselectorposArray[8] = bottomleft.z;
		dragselectorposArray[9] = bottomright.x;
		dragselectorposArray[10] = bottomright.y;
		dragselectorposArray[11] = bottomright.z;
		dragselectorpos.needsUpdate = true;

		const dragselectoroutlinepos = (this.dragselector.children[1] as Mesh<BufferGeometry, Material>).geometry.attributes.position;
		const dragselectoroutlineposArray = dragselectorpos.array as unknown as Array<number>;
		dragselectoroutlineposArray[0] = topleft.x;
		dragselectoroutlineposArray[1] = topleft.y;
		dragselectoroutlineposArray[2] = topleft.z;
		dragselectoroutlineposArray[3] = topright.x;
		dragselectoroutlineposArray[4] = topright.y;
		dragselectoroutlineposArray[5] = topright.z;
		dragselectoroutlineposArray[6] = topright.x;
		dragselectoroutlineposArray[7] = topright.y;
		dragselectoroutlineposArray[8] = topright.z;
		dragselectoroutlineposArray[9] = bottomright.x;
		dragselectoroutlineposArray[10] = bottomright.y;
		dragselectoroutlineposArray[11] = bottomright.z;
		dragselectoroutlineposArray[12] = bottomright.x;
		dragselectoroutlineposArray[13] = bottomright.y;
		dragselectoroutlineposArray[14] = bottomright.z;
		dragselectoroutlineposArray[15] = bottomleft.x;
		dragselectoroutlineposArray[16] = bottomleft.y;
		dragselectoroutlineposArray[17] = bottomleft.z;
		dragselectoroutlineposArray[18] = bottomleft.x;
		dragselectoroutlineposArray[19] = bottomleft.y;
		dragselectoroutlineposArray[20] = bottomleft.z;
		dragselectoroutlineposArray[21] = topleft.x;
		dragselectoroutlineposArray[22] = topleft.y;
		dragselectoroutlineposArray[23] = topleft.z;
		dragselectoroutlinepos.needsUpdate = true;

		this.dragselector.visible = true;
	}

	private onResize(): void {
		let width, height;
		if(window.innerWidth < window.innerHeight * 1.618) {
			width = window.innerWidth;
			height = window.innerWidth * 0.618;
		} else {
			width = window.innerHeight * 1.618;
			height = window.innerHeight;
		}

		this.camera.aspect = width / height;
		this.camera.updateProjectionMatrix();
		this.camera.filmGauge = height;

		this.renderer.setSize(width, height);

		const rect = this.renderer.domElement.getBoundingClientRect();
		Input.mouse.setBounds(rect.left, rect.top, rect.right - rect.left, rect.bottom - rect.top);
	}

	private updateCellSelector(world: World, indicating: string, currentPointingCell: HexVector | null, delta: number): void {
		if(this.cellselector.visible) {
			const size = this.cellselector.scale.x + this.cellselectorSpeed * 0.5 * delta;
			this.cellselector.scale.set(size, 1, size);
			if(size < 0 || size > 1.6) { this.cellselector.visible = false; }
		}

		if(indicating && currentPointingCell) {
			this.cellselector.visible = true;
			const currentPointingCellPos = Transients.get(Vector2);
			currentPointingCell.toVector2(currentPointingCellPos);
			this.cellselector.position.set(currentPointingCellPos.x, getGridHeight(world.grid, currentPointingCell), currentPointingCellPos.y);
			this.cellselector.scale.set(1, 1, 1);
			this.cellselectorSpeed = indicating === 'attack' ? 1 : -1;
		}
	}

	private calculateVisibility(world: World, camera: PerspectiveCamera) {
		const centerray = Transients.get(Ray);
		centerray.origin.set(0, 0, -1).unproject(camera);
		centerray.direction.set(0, 0, 1).unproject(camera).sub(centerray.origin).normalize();
		if(isNaN(centerray.direction.y) || centerray.direction.y >= 0) { return; }

		const center = Transients.get(HexVector);
		Transients.push(HexVector);
		Transients.push(Vector2);
		const origin = Transients.get(Vector2).set(centerray.origin.x, centerray.origin.z);
		const direction = Transients.get(Vector2).set(centerray.direction.x, centerray.direction.z);
		const hexorigin = Transients.get(HexVector).fromVector2(origin).tuple(HEX_GROUP_SIZE);
		const hexdirection = Transients.get(HexVector).fromVector2(direction).tuple(HEX_GROUP_SIZE);
		let found = false;
		for(const [hex, mindist, maxdist] of hexRay(hexorigin, hexdirection)) {
			const cell = world.grid.get(hex);
			if(!cell) { break; }

			let intersection: number;
			intersection = (centerray.origin.y - cell.maxheight) / -centerray.direction.y;
			intersection = Math.max(intersection, mindist);

			if(intersection < maxdist) {
				center.copy(hex);
				found = true;
				break;
			}
		}
		Transients.pop(HexVector);
		Transients.pop(Vector2);
		if(!found) { return; }

		Transients.push(HexVector);
		Transients.push(Frustum);
		const temp = Transients.get(HexVector);
		const frustum = Transients.get(Frustum).setFromProjectionMatrix(Transients.get(Matrix4).multiplyMatrices(camera.projectionMatrix, camera.matrixWorldInverse));

		const groups = this.visibilityGroups;
		groups.length = 0;

		const centerrow = center.row;
		const centercol = center.col;
		groups.push(center.toIndex());

		let radius = 1;
		let count = 0;
		let index = 0;
		const sphere = Transients.get(Sphere);
		let group: HexGroup | undefined;
		while(true) {
			for(let pos = 0; pos < radius; pos++) {
				index = temp.set(-radius + pos + centerrow, radius + centercol).toIndex();
				group = world.groups.getByIndex(index);
				if(group && frustum.intersectsSphere(sphere.set(group.position, group.radius))) { count += 1; groups.push(index); }

				index = temp.set(pos + centerrow, radius - pos + centercol).toIndex();
				group = world.groups.getByIndex(index);
				if(group && frustum.intersectsSphere(sphere.set(group.position, group.radius))) { count += 1; groups.push(index); }

				index = temp.set(radius + centerrow, -pos + centercol).toIndex();
				group = world.groups.getByIndex(index);
				if(group && frustum.intersectsSphere(sphere.set(group.position, group.radius))) { count += 1; groups.push(index); }

				index = temp.set(radius - pos + centerrow, -radius + centercol).toIndex();
				group = world.groups.getByIndex(index);
				if(group && frustum.intersectsSphere(sphere.set(group.position, group.radius))) { count += 1; groups.push(index); }

				index = temp.set(-pos + centerrow, -radius + pos + centercol).toIndex();
				group = world.groups.getByIndex(index);
				if(group && frustum.intersectsSphere(sphere.set(group.position, group.radius))) { count += 1; groups.push(index); }

				index = temp.set(-radius + centerrow, pos + centercol).toIndex();
				group = world.groups.getByIndex(index);
				if(group && frustum.intersectsSphere(sphere.set(group.position, group.radius))) { count += 1; groups.push(index); }
			}
			if(!count) { break; }
			count = 0;
			radius += 1;
		}

		Transients.pop(HexVector);
		Transients.pop(Frustum);
	}

	private updateLights(world: World): void {
		this.light.shadow.camera.near = -1;
		this.light.shadow.camera.far = 1;
		this.light.shadow.camera.top = 1;
		this.light.shadow.camera.bottom = -1;
		this.light.shadow.camera.left = -1;
		this.light.shadow.camera.right = 1;
		this.light.shadow.camera.updateProjectionMatrix();

		let minx = Infinity, maxx = -Infinity;
		let miny = Infinity, maxy = -Infinity;
		let minz = Infinity, maxz = -Infinity;
		let maxradius = -Infinity;
		const vertex = Transients.get(Vector3);
		const groups = this.visibilityGroups;
		const camera = this.light.shadow.camera;
		for(let index = groups.length - 1; index >= 0; index--) {
			const group = world.groups.getByIndex(groups[index]);
			if(!group) { continue; }

			vertex.copy(group.position).project(camera);
			if(vertex.x < minx) { minx = vertex.x; }
			if(vertex.x > maxx) { maxx = vertex.x; }
			if(vertex.y < miny) { miny = vertex.y; }
			if(vertex.y > maxy) { maxy = vertex.y; }
			if(vertex.z < minz) { minz = vertex.z; }
			if(vertex.z > maxz) { maxz = vertex.z; }
			if(group.radius > maxradius) { maxradius = group.radius; }
		}

		this.light.shadow.camera.near = minz - maxradius;
		this.light.shadow.camera.far = maxz + maxradius;
		this.light.shadow.camera.top = maxy + maxradius;
		this.light.shadow.camera.bottom = miny - maxradius;
		this.light.shadow.camera.left = minx - maxradius;
		this.light.shadow.camera.right = maxx + maxradius;
		this.light.shadow.camera.updateProjectionMatrix();
	}

	// private renderUnits(world: World, time: number, delta: number, selected: number[], detail: boolean): void {
	// 	for(let index = this.visibilityGroups.length - 1; index >= 0; index--) {
	// 		const group = world.groups.getByIndex(this.visibilityGroups[index]);
	// 		if(!group) { continue; }
	// 		for(let unit_index = group.occupiedBy.length - 1; unit_index >= 0; unit_index--) {
	// 			const unit = group.occupiedBy[unit_index];
	// 			renderUnit(world, this, time, delta, selected.includes(unit.id), unit, detail);
	// 		}
	// 	}
	// }

	private updateCamera(world: World, zoomOut: number, position: Vector2) {
		const direction = Transients.get(Vector2).set(0, -1);
		const camerahex = Transients.get(HexVector).fromVector2(position);
		const height = zoomOut > 150 ? 150 - zoomOut : getGridHeight(world.grid, camerahex);
		const sideView = WorldSpecs[world.type].minZoom * (1 - Math.max(0, Math.min(1, (zoomOut - 100) / 50)));
		this.camera.position.set(position.x - direction.x * sideView, zoomOut + height, position.y - direction.y * sideView);
		this.camera.lookAt(Transients.get(Vector3).set(position.x, height, position.y));
		this.camera.updateMatrixWorld(false);
		this.makeComboPerspOrthoMatrix(this.camera.projectionMatrix, this.camera.fov, this.camera.near, this.camera.far, this.camera.aspect, zoomOut, (zoomOut - 100) / 50);
	}

	private makeComboPerspOrthoMatrix(matrix: Matrix4, fov: number, near: number, far: number, aspect_ratio: number, convergence_distance: number, ratio: number): void {
		const tangent = Math.tan(Math.PI / 360 * fov);
		const depth_scaling = 1 / (far - near);

		const persp_c = -1 * (far + near) * depth_scaling;
		const persp_d = -2 * (far * near) * depth_scaling;
		const persp_e = -1;
		const persp_f = 0;

		const ortho_c = -2 * convergence_distance * depth_scaling;
		const ortho_d = -1 * (far + near) * convergence_distance * depth_scaling;
		const ortho_e = 0;
		const ortho_f = convergence_distance;

		const a = 1 / (tangent * aspect_ratio);
		const b = 1 / tangent;
		const c = lerp(persp_c, ortho_c, ratio);
		const d = lerp(persp_d, ortho_d, ratio);
		const e = lerp(persp_e, ortho_e, ratio);
		const f = lerp(persp_f, ortho_f, ratio);

		const elements = matrix.elements;
		elements[ 0 ] = a;	elements[ 4 ] = 0;	elements[ 8 ] = 0;	elements[ 12 ] = 0;
		elements[ 1 ] = 0;	elements[ 5 ] = b;	elements[ 9 ] = 0;	elements[ 13 ] = 0;
		elements[ 2 ] = 0;	elements[ 6 ] = 0;	elements[ 10 ] = c;	elements[ 14 ] = d;
		elements[ 3 ] = 0;	elements[ 7 ] = 0;	elements[ 11 ] = e;	elements[ 15 ] = f;
	}
}
