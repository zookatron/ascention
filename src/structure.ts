import { HexVector } from './hexagon';
import { World } from './world';
import { hexGroupIndex } from './grid';

export enum StructureType {
	MainBase,
	Factory,
	Miner,
}

export interface StructureSpec {
	health: number;
	size: number;
	image: string;
}

// tslint:disable-next-line:variable-name
export const StructureSpecs: { [type: number]: StructureSpec } = {
	[StructureType.MainBase]: {
		health: 1000,
		size: 3,
		image: 'building_black',
	},
	[StructureType.Factory]: {
		health: 1000,
		size: 2,
		image: 'building_black',
	},
	[StructureType.Miner]: {
		health: 1000,
		size: 1,
		image: 'building_black',
	},
};

export class Structure {
	public type!: StructureType;
	public id = -1;
	public position: HexVector = new HexVector();
	public health = 0;
	public enemy = false;
	public died = 0;
}

let nextId = 0;
export function createStructure(type: StructureType, position: HexVector, world: World, enemy = false) {
	const structure = new Structure();
	structure.id = nextId++;
	structure.type = type;
	structure.enemy = enemy;
	const specs = StructureSpecs[type];
	structure.health = specs.health;

	world.structures.set(structure.id, structure);
	structure.position.copy(position);
	const [group_index] = hexGroupIndex(position.toIndex());
	world.groups.getByIndex(group_index)?.structures.push(structure);

	return structure;
}

export function removeStructure(structure: Structure, world: World) {
	world.structures.delete(structure.id);
	const [group_index] = hexGroupIndex(structure.position.toIndex());
	world.groups.getByIndex(group_index)?.structures.push(structure);
}
