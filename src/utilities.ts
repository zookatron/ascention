import { identity } from 'lodash';
import { EPSILON } from './constants';
import { Vector2 } from 'three';

export function modulate(vec: Vector2, other: Vector2): Vector2 {
	const x = vec.x;
	const y = vec.y;

	const a = other.x;
	const b = other.y;

	vec.x = a * x - b * y;
	vec.y = b * x + a * y;

	return vec;
}

export function antimodulate(vec: Vector2, other: Vector2): Vector2 {
	const x = vec.x;
	const y = vec.y;

	const a = other.x;
	const b = other.y;

	const d = 1 / (a * a + b * b);
	const a2 = a * d;
	const b2 = -b * d;

	vec.x = a2 * x - b2 * y;
	vec.y = b2 * x + a2 * y;

	return vec;
}

export function* mapIterable<T, R>(iterable: Iterable<T>, func: (value: T) => R): IterableIterator<R> {
	const iterator = iterable[Symbol.iterator]();
	let result = iterator.next();

	while(!result.done) {
		yield func(result.value);
		result = iterator.next();
	}
}

export function* enumerateIterable<T>(iterable: Iterable<T>): IterableIterator<[number, T]> {
	const iterator = iterable[Symbol.iterator]();
	let result = iterator.next();
	let index = 0;
	const array: [number, T] = [0, null as unknown as T];

	while(!result.done) {
		array[0] = index;
		array[1] = result.value;
		yield array;
		result = iterator.next();
		index++;
	}
}

export function* rangeIterable(range: number): IterableIterator<number> {
	for(let index = 0; index < range; index++) {
		yield index;
	}
}

function zipIterablesIterize<T>(x: Iterable<T>) { return x[Symbol.iterator]() as Iterator<T, T, T> }
const zipIterablesNext = <T>(x: Iterator<T, T, T>) => x.next() ;
const zipIterablesDone = <T>(x: IteratorResult<T, T>) => x.done;
const zipIterablesValue = <T>(x: IteratorResult<T, T>) => x.value ;
export function zipIterables<T1>(i1: Iterable<T1>): IterableIterator<[T1]>;
export function zipIterables<T1, T2>(i1: Iterable<T1>, i2: Iterable<T2>): IterableIterator<[T1, T2]>;
export function zipIterables<T1, T2, T3>(i1: Iterable<T1>, i2: Iterable<T2>, i3: Iterable<T3>): IterableIterator<[T1, T2, T3]>;
export function* zipIterables<T>(...iterables: Array<Iterable<T>>): IterableIterator<T[]> {
	const iterators = iterables.map(zipIterablesIterize);
	let items = iterators.map(zipIterablesNext);
	let done = items.some(zipIterablesDone);

	while(!done) {
		yield items.map(zipIterablesValue);
		items = iterators.map(zipIterablesNext);
		done = items.some(zipIterablesDone);
	}
}

export function iterableEvery<T>(iterable: Iterable<T>, predicate: (element: T) => unknown = identity): boolean {
	for(const element of iterable) {
		if(!predicate(element)) { return false; }
	}
	return true;
}

export function deltaInterpFactor(speed: number, delta: number): number {
	return 1 - Math.exp(-speed * delta);
}

export function deltaInterp(current: number, target: number, speed: number, delta: number): number {
	return Math.abs(current - target) < EPSILON ? target : current + (target - current) * (1 - Math.exp(-speed * delta));
}

export function angleDiff(angle1: number, angle2: number): number {
	const difference = angle2 - angle1;
	return Math.abs(difference) > Math.PI ? difference - Math.sign(difference) * Math.PI * 2 : difference;
}

export function normalizeAngle(angle: number): number {
	if(angle > Math.PI * 2) { return angle - Math.PI * 2; }
	if(angle < 0) { return angle + Math.PI * 2; }
	return angle;
}

export function lerpTime(current: number, target: number, duration: number, start: number, currentTime: number): number {
	return current + (target - current) * Math.min(1, (currentTime - start) / duration);
}

export function clamp(value: number, min: number, max: number) {
	return Math.min(Math.max(value, min), max);
}

export function lerp(from: number, to: number, ratio: number) {
	const clampedratio = clamp(ratio, 0, 1);
	return from * (1 - clampedratio) + to * clampedratio;
}

function PriorityQueueParent(i: number): number { return ((i + 1) >>> 1) - 1; }
function PriorityQueueLeft(i: number): number { return (i << 1) + 1; }
function PriorityQueueRight(i: number): number { return (i + 1) << 1; }
function PriorityQueueSwap(heap: Array<PriorityQueueElement<unknown>>, i: number, j: number): void {
	[heap[i], heap[j]] = [heap[j], heap[i]];
}
function PriorityQueueSiftUp(heap: Array<PriorityQueueElement<unknown>>): void {
	let node = heap.length - 1;
	let nodeparent = PriorityQueueParent(node);
	while(node > 0 && heap[node].priority < heap[nodeparent].priority) {
		PriorityQueueSwap(heap, node, nodeparent);
		node = nodeparent;
		nodeparent = PriorityQueueParent(node);
	}
}
function PriorityQueueSiftDown(heap: Array<PriorityQueueElement<unknown>>): void {
	let node = 0;
	let nodeleft = PriorityQueueLeft(node);
	let noderight = PriorityQueueRight(node);
	while((nodeleft < heap.length && heap[nodeleft].priority < heap[node].priority) || (noderight < heap.length && heap[noderight].priority < heap[node].priority)) {
		const maxChild = (noderight < heap.length && heap[noderight].priority < heap[nodeleft].priority) ? noderight : nodeleft;
		PriorityQueueSwap(heap, node, maxChild);
		node = maxChild;
		nodeleft = PriorityQueueLeft(node);
		noderight = PriorityQueueRight(node);
	}
}

class PriorityQueueElement<T> {
	constructor(public priority: number, public value: T) {}
}

export class PriorityQueue<T> {
	private heap: Array<PriorityQueueElement<T>> = new Array<PriorityQueueElement<T>>();

	public size(): number {
		return this.heap.length;
	}

	public peek(): T {
		return this.heap[0].value;
	}

	public peekPriority(): number {
		return this.heap[0].priority;
	}

	public push(priority: number, value: T): void {
		this.heap.push(new PriorityQueueElement(priority, value));
		PriorityQueueSiftUp(this.heap);
	}

	public pop(): T {
		const poppedValue: T = this.peek();
		const bottom: number = this.heap.length - 1;
		if(bottom > 0) { PriorityQueueSwap(this.heap, 0, bottom); }
		this.heap.pop();
		PriorityQueueSiftDown(this.heap);
		return poppedValue;
	}

	public remove(priority: number, value: T): void {
		// TODO: make this actually use the heap structure
		let target = -1;
		for(let i = this.heap.length - 1; i >= 0; i--) {
			if(this.heap[i].priority === priority && this.heap[i].value === value) {
				target = i;
				break;
			}
		}
		if(target < 0) { throw new Error(`Unable to find value ${value as unknown as string} with priority ${priority}`); }

		const bottom: number = this.heap.length - 1;
		if(bottom > 0) { PriorityQueueSwap(this.heap, target, bottom); }
		this.heap.pop();
		PriorityQueueSiftDown(this.heap);
	}
}

export class Timer {
	public time = 0;
	public delta = 0;
	private startTime = 0;
	private lastUpdateTime = 0;
	private maxDelta = 0;

	constructor(maxDelta: number) {
		this.maxDelta = maxDelta;
	}

	public update(time: number): void {
		if(!this.startTime) { this.startTime = time || 0; }
		if(!this.lastUpdateTime) { this.lastUpdateTime = time || 0; }
		this.delta = time - this.lastUpdateTime;
		this.delta = Math.min(this.delta, this.maxDelta);
		this.time = time - this.startTime;
		this.lastUpdateTime = time;
	}
}

export function qsort(array: number[]): void {
	let index = 2;
	let start = 0;
	let end   = array.length - 1;
	const stack = [start, end];
	let temp;

	while(index > 0) {
		end = stack[--index];
		start = stack[--index];

		if(start < end) {
			let low = start;
			let high = end - 1;

			const pivot = start;
			const pivotvalue = array[pivot];
			array[pivot] = array[end];

			while(true) {
				while(low <= high && array[low] !== undefined && array[low] < pivotvalue) { low++; }
				while(low <= high && array[high] !== undefined && array[high] >= pivotvalue) { high--; }
				if(low > high) { break; }
				temp = array[low];
				array[low] = array[high];
				array[high] = temp;
			}

			array[end] = array[low];
			array[low] = pivotvalue;

			stack[index++] = start;
			stack[index++] = low - 1;
			stack[index++] = low + 1;
			stack[index++] = end;
		}
	}
}
