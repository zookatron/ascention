const TWOPOWMMINUSTHIRTYTWO = 2.3283064365386963e-10;
const TWOPOWTHIRTYTWO = 0x100000000;
// tslint:disable-next-line:whitespace
const PGPWORDS = [['aardvark','adroitness'],['absurd','adviser'],['accrue','aftermath'],['acme','aggregate'],['adrift','alkali'],['adult','almighty'],['afflict','amulet'],['ahead','amusement'],['aimless','antenna'],['algol','applicant'],['allow','apollo'],['alone','armistice'],['ammo','article'],['ancient','asteroid'],['apple','atlantic'],['artist','atmosphere'],['assume','autopsy'],['athens','babylon'],['atlas','backwater'],['aztec','barbecue'],['baboon','belowground'],['backfield','bifocals'],['backward','bodyguard'],['banjo','bookseller'],['beaming','borderline'],['bedlamp','bottomless'],['beehive','bradbury'],['beeswax','bravado'],['befriend','brazilian'],['belfast','breakaway'],['berserk','burlington'],['billiard','businessman'],['bison','butterfat'],['blackjack','camelot'],['blockade','candidate'],['blowtorch','cannonball'],['bluebird','capricorn'],['bombast','caravan'],['bookshelf','caretaker'],['brackish','celebrate'],['breadline','cellulose'],['breakup','certify'],['brickyard','chambermaid'],['briefcase','cherokee'],['burbank','chicago'],['button','clergyman'],['buzzard','coherence'],['cement','combustion'],['chairlift','commando'],['chatter','company'],['checkup','component'],['chisel','concurrent'],['choking','confidence'],['chopper','conformist'],['christmas','congregate'],['clamshell','consensus'],['classic','consulting'],['classroom','corporate'],['cleanup','corrosion'],['clockwork','councilman'],['cobra','crossover'],['commence','crucifix'],['concert','cumbersome'],['cowbell','customer'],['crackdown','dakota'],['cranky','decadence'],['crowfoot','december'],['crucial','decimal'],['crumpled','designing'],['crusade','detector'],['cubic','detergent'],['dashboard','determine'],['deadbolt','dictator'],['deckhand','dinosaur'],['dogsled','direction'],['dragnet','disable'],['drainage','disbelief'],['dreadful','disruptive'],['drifter','distortion'],['dropper','document'],['drumbeat','embezzle'],['drunken','enchanting'],['dupont','enrollment'],['dwelling','enterprise'],['eating','equation'],['edict','equipment'],['egghead','escapade'],['eightball','eskimo'],['endorse','everyday'],['endow','examine'],['enlist','existence'],['erase','exodus'],['escape','fascinate'],['exceed','filament'],['eyeglass','finicky'],['eyetooth','forever'],['facial','fortitude'],['fallout','frequency'],['flagpole','gadgetry'],['flatfoot','galveston'],['flytrap','getaway'],['fracture','glossary'],['framework','gossamer'],['freedom','graduate'],['frighten','gravity'],['gazelle','guitarist'],['geiger','hamburger'],['glitter','hamilton'],['glucose','handiwork'],['goggles','hazardous'],['goldfish','headwaters'],['gremlin','hemisphere'],['guidance','hesitate'],['hamlet','hideaway'],['highchair','holiness'],['hockey','hurricane'],['indoors','hydraulic'],['indulge','impartial'],['inverse','impetus'],['involve','inception'],['island','indigo'],['jawbone','inertia'],['keyboard','infancy'],['kickoff','inferno'],['kiwi','informant'],['klaxon','insincere'],['locale','insurgent'],['lockup','integrate'],['merit','intention'],['minnow','inventive'],['miser','istanbul'],['mohawk','jamaica'],['mural','jupiter'],['music','leprosy'],['necklace','letterhead'],['neptune','liberty'],['newborn','maritime'],['nightbird','matchmaker'],['oakland','maverick'],['obtuse','medusa'],['offload','megaton'],['optic','microscope'],['orca','microwave'],['payday','midsummer'],['peachy','millionaire'],['pheasant','miracle'],['physique','misnomer'],['playhouse','molasses'],['pluto','molecule'],['preclude','montana'],['prefer','monument'],['preshrunk','mosquito'],['printer','narrative'],['prowler','nebula'],['pupil','newsletter'],['puppy','norwegian'],['python','october'],['quadrant','ohio'],['quiver','onlooker'],['quota','opulent'],['ragtime','orlando'],['ratchet','outfielder'],['rebirth','pacific'],['reform','pandemic'],['regain','pandora'],['reindeer','paperweight'],['rematch','paragon'],['repay','paragraph'],['retouch','paramount'],['revenge','passenger'],['reward','pedigree'],['rhythm','pegasus'],['ribcage','penetrate'],['ringbolt','perceptive'],['robust','performance'],['rocker','pharmacy'],['ruffled','phonetic'],['sailboat','photograph'],['sawdust','pioneer'],['scallion','pocketful'],['scenic','politeness'],['scorecard','positive'],['scotland','potato'],['seabird','processor'],['select','provincial'],['sentence','proximate'],['shadow','puberty'],['shamrock','publisher'],['showgirl','pyramid'],['skullcap','quantity'],['skydive','racketeer'],['slingshot','rebellion'],['slowdown','recipe'],['snapline','recover'],['snapshot','repellent'],['snowcap','replica'],['snowslide','reproduce'],['solo','resistor'],['southward','responsive'],['soybean','retraction'],['spaniel','retrieval'],['spearhead','retrospect'],['spellbind','revenue'],['spheroid','revival'],['spigot','revolver'],['spindle','sandalwood'],['spyglass','sardonic'],['stagehand','saturday'],['stagnate','savagery'],['stairway','scavenger'],['standard','sensation'],['stapler','sociable'],['steamship','souvenir'],['sterling','specialist'],['stockman','speculate'],['stopwatch','stethoscope'],['stormy','stupendous'],['sugar','supportive'],['surmount','surrender'],['suspense','suspicious'],['sweatband','sympathy'],['swelter','tambourine'],['tactics','telephone'],['talon','therapist'],['tapeworm','tobacco'],['tempest','tolerance'],['tiger','tomorrow'],['tissue','torpedo'],['tonic','tradition'],['topmost','travesty'],['tracker','trombonist'],['transit','truncated'],['trauma','typewriter'],['treadmill','ultimate'],['trojan','undaunted'],['trouble','underfoot'],['tumor','unicorn'],['tunnel','unify'],['tycoon','universe'],['uncut','unravel'],['unearth','upcoming'],['unwind','vacancy'],['uproot','vagabond'],['upset','vertigo'],['upshot','virginia'],['vapor','visitor'],['village','vocalist'],['virus','voyager'],['vulcan','warranty'],['waffle','waterloo'],['wallet','whimsical'],['watchword','wichita'],['wayside','wilmington'],['willow','wyoming'],['woodlark','yesteryear'],['zulu','yucatan']];

// https://stackoverflow.com/questions/6232939/is-there-a-way-to-correctly-multiply-two-32-bit-integers-in-javascript
function mul32(a: number, b: number): number {
	a >>>= 0;
	b >>>= 0;
	const nlo = a & 0xffff;
	const nhi = a - nlo;
	return ((nhi * b >>> 0) + (nlo * b >>> 0)) >>> 0;
}

// https://github.com/LRFLEW/NoiseGame/blob/master/noiseFrag.glsl
class Hash {
	public string(str: string): number {
		let hash = 5381;
		for(let i = 0; i < str.length; i++) {
			hash = ((hash << 5) + hash) ^ str.charCodeAt(i);
		}
		return hash >>> 0;
	}

	public humanize(value: number): string {
		value = Random.hash.number(value, 179426407);
		const a = PGPWORDS[(value >>> 0) & 0xff][0];
		const b = PGPWORDS[(value >>> 8) & 0xff][1];
		const c = PGPWORDS[(value >>> 16) & 0xff][0];
		const d = PGPWORDS[(value >>> 24) & 0xff][1];
		return `${a}-${b}-${c}-${d}`;
	}

	public number(a: number, seed: number): number {
		let hash = seed;
		hash = this.subhash(a, hash);
		return this.finalize(hash);
	}

	public number2(a: number, b: number, seed: number): number {
		let hash = seed;
		hash = this.subhash(a, hash);
		hash = this.subhash(b, hash);
		return this.finalize(hash);
	}

	private subhash(value: number, hash: number): number {
		value = mul32(value, 0x5bd1e995);
		value ^= value >>> 24;
		value = mul32(value, 0x5bd1e995);
		hash = mul32(hash, 0x5bd1e995) ^ value;
		return hash;
	}

	private finalize(hash: number): number {
		hash ^= hash >>> 13;
		hash = mul32(hash, 0x5bd1e995);
		hash ^= hash >>> 15;
		return hash >>> 0;
	}
}

class GeneratorContext {
	public c: number;
	public s0: number;
	public s1: number;
	public s2: number;

	constructor(seed: number) {
		this.c = 1;
		this.s0 = Random.hash.number(seed, 1305169) * TWOPOWMMINUSTHIRTYTWO;
		this.s1 = Random.hash.number(seed, 15503687) * TWOPOWMMINUSTHIRTYTWO;
		this.s2 = Random.hash.number(seed, 179426407) * TWOPOWMMINUSTHIRTYTWO;
		return this;
	}
}

class Generator {
	private default_context: GeneratorContext | null = null;

	public context(seed: number): GeneratorContext {
		return new GeneratorContext(seed);
	}

	public float(context?: GeneratorContext): number {
		if(!context) {
			if(!this.default_context) { this.default_context = this.context(TWOPOWTHIRTYTWO); }
			context = this.default_context;
		}
		const t = 2091639 * context.s0 + context.c * TWOPOWMMINUSTHIRTYTWO;
		context.s0 = context.s1;
		context.s1 = context.s2;
		context.c = t >>> 0;
		context.s2 = t - context.c;
		return context.s2;
	}

	public integer(context?: GeneratorContext): number {
		return this.float(context) * TWOPOWTHIRTYTWO >>> 0;
	}
}

const SQRT_THREE = Math.sqrt(3);
const TO_SIMPLEX_FACTOR = 0.5 * (Math.sqrt(3.0) - 1.0);
const FROM_SIMPLEX_FACTOR = (3.0 - Math.sqrt(3.0)) / 6.0;
class Noise {
	private grad3 = new Float32Array([
		-1, -1,
		-1, 0,
		-1, 0,
		-1, 1,
		0, -1,
		0, -1,
		0, 0,
		0, 1,
		1, -1,
		1, 0,
		1, 0,
		1, 1,
	]);

	public noise2d(row: number, col: number, seed: number) {
		const xin = 1.5 * col;
		const yin = SQRT_THREE * (row + col * 0.5);

		// Skew the input space to determine which simplex cell we're in
		const s = (xin + yin) * TO_SIMPLEX_FACTOR; // Hairy factor for 2D
		const i = Math.floor(xin + s);
		const j = Math.floor(yin + s);
		const t = (i + j) * FROM_SIMPLEX_FACTOR;
		const X0 = i - t; // Unskew the cell origin back to (x,y) space
		const Y0 = j - t;
		const x0 = xin - X0; // The x,y distances from the cell origin
		const y0 = yin - Y0;

		// For the 2D case, the simplex shape is an equilateral triangle.
		// Determine which simplex we are in.
		let i1, j1; // Offsets for second (middle) corner of simplex in (i,j) coords
		if(x0 > y0) {
			// lower triangle, XY order: (0,0)->(1,0)->(1,1)
			i1 = 1;
			j1 = 0;
		} else {
			// upper triangle, YX order: (0,0)->(0,1)->(1,1)
			i1 = 0;
			j1 = 1;
		}
		// A step of (1,0) in (i,j) means a step of (1-c,-c) in (x,y), and
		// a step of (0,1) in (i,j) means a step of (-c,1-c) in (x,y), where
		// c = (3-sqrt(3))/6

		// Offsets for middle corner in (x,y) unskewed coords
		const x1 = x0 - i1 + FROM_SIMPLEX_FACTOR;
		const y1 = y0 - j1 + FROM_SIMPLEX_FACTOR;
		// Offsets for last corner in (x,y) unskewed coords
		const x2 = x0 - 1.0 + 2.0 * FROM_SIMPLEX_FACTOR;
		const y2 = y0 - 1.0 + 2.0 * FROM_SIMPLEX_FACTOR;

		// Calculate the contribution from the three corners
		const grad3 = this.grad3;
		let n0 = 0;
		let t0 = 0.5 - x0 * x0 - y0 * y0;
		if(t0 >= 0) {
			const factor = Math.exp(-2 * (Random.hash.number2(i, j, seed) * TWOPOWMMINUSTHIRTYTWO));
			const gi0 = (Random.hash.number2(i, j, seed) % 12) * 2;
			t0 *= t0;
			n0 = factor * t0 * t0 * (grad3[gi0] * x0 + grad3[gi0 + 1] * y0); // (x,y) of grad3 used for 2D gradient
		}
		let n1 = 0;
		let t1 = 0.5 - x1 * x1 - y1 * y1;
		if(t1 >= 0) {
			const factor = Math.exp(-2 * (Random.hash.number2(i + i1, j + j1, seed) * TWOPOWMMINUSTHIRTYTWO));
			const gi1 = (Random.hash.number2(i + i1, j + j1, seed) % 12) * 2;
			t1 *= t1;
			n1 = factor * t1 * t1 * (grad3[gi1] * x1 + grad3[gi1 + 1] * y1);
		}
		let n2 = 0;
		let t2 = 0.5 - x2 * x2 - y2 * y2;
		if(t2 >= 0) {
			const factor = Math.exp(-2 * (Random.hash.number2(i + 1, j + 1, seed) * TWOPOWMMINUSTHIRTYTWO));
			const gi2 = (Random.hash.number2(i + 1, j + 1, seed) % 12) * 2;
			t2 *= t2;
			n2 = factor * t2 * t2 * (grad3[gi2] * x2 + grad3[gi2 + 1] * y2);
		}

		// Add contributions from each corner to get the final noise value.
		// The result is scaled to return values in the interval [0,1].
		return 35.0 * (n0 + n1 + n2) + 0.5;
	}
}

// tslint:disable-next-line:variable-name
const Random = {
	hash: new Hash(),
	number: new Generator(),
	noise: new Noise(),
};

export default Random;
