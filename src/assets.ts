import _ from 'lodash';
import {Texture, TextureLoader} from 'three';

export enum TextureID {}

function textureLoader(getUrl: () => Promise<Record<string, string>>) {
	let promise: Promise<Texture> | null = null;
	const makePromise = () => getUrl().then(url => new Promise<Texture>((resolve, reject) => {
		return new TextureLoader().load(url.default, resolve, undefined, reject);
	}));
	return () => (promise || makePromise()).then(texture => texture.clone());
}

function makeTextures() {
	const files = import.meta.glob('./assets/textures/*.png');
	const textures = {} as Omit<Record<string, TextureID>, 'load'> & { load: (id: TextureID) => Promise<Texture> };
	const loaders = {} as Record<number, () => Promise<Texture>>;

	for(const [index, file] of Object.keys(files).entries()) {
		const name = _.last(file.split('/'))!.slice(0, -4);
		textures[name] = index;
		loaders[index] = textureLoader(files[file]);
	}

	textures.load = (id: TextureID) => loaders[id]();

	return textures;
}

export const Textures = makeTextures();
