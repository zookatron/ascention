import Transients from './transients';
import { Vector2, Vector3 } from 'three';
import { zipIterables, mapIterable, rangeIterable, enumerateIterable, lerp } from './utilities';
import { EPSILON } from './constants';

const ONE_THIRD = 1 / 3;
const TWO_THIRDS = 2 / 3;
const ONE_TWELFTH = 1 / 12;
const SQRT_THREE = Math.sqrt(3);
const SQRT_THREE_DIV_THREE = Math.sqrt(3) / 3;

export class HexVector {
	public row: number;
	public col: number;

	constructor(row = 0, col = 0) {
		this.row = row;
		this.col = col;
	}

	public copy(other: HexVector): HexVector {
		this.row = other.row;
		this.col = other.col;
		return this;
	}

	public add(other: HexVector): HexVector {
		this.row += other.row;
		this.col += other.col;
		return this;
	}

	public sub(other: HexVector): HexVector {
		this.row -= other.row;
		this.col -= other.col;
		return this;
	}

	public multiplyScalar(factor: number): HexVector {
		this.row *= factor;
		this.col *= factor;
		return this;
	}

	public dot(other: HexVector): number {
		return this.col * other.col + (-this.col - this.row) * (-other.col - other.row) + this.row * other.row;
	}

	public equals(other: HexVector): boolean {
		return this.row === other.row && this.col === other.col;
	}

	public set(row: number, col: number): HexVector {
		this.row = row;
		this.col = col;
		return this;
	}

	public setIdentity(): HexVector {
		this.row = -1;
		this.col = 0;
		return this;
	}

	public length(): number {
		return (Math.abs(this.col) + Math.abs(this.col + this.row) + Math.abs(this.row)) * 0.5;
	}

	public realLength(): number {
		const col = this.col;
		const row = this.row;
		return Math.sqrt(col * col + row * row + col * row);
	}

	public distanceTo(other: HexVector): number {
		const col = this.col - other.col;
		const row = this.row - other.row;
		return (Math.abs(col) + Math.abs(col + row) + Math.abs(row)) * 0.5;
	}

	public realDistanceTo(other: HexVector): number {
		const col = this.col - other.col;
		const row = this.row - other.row;
		return Math.sqrt(col * col + row * row + col * row);
	}

	public lerp(other: HexVector, factor: number): HexVector {
		this.row = lerp(this.row, other.row, factor);
		this.col = lerp(this.col, other.col, factor);
		return this;
	}

	public tuple(size: number): HexVector {
		const x = this.col;
		const z = this.row;

		const d = 1 / (3 * size * (size - 1) + 1);
		const a = size * d;
		const b = (size - 1) * d;
		const c = -a - b;

		this.col = b * x - a * z;
		this.row = a * x - c * z;

		return this;
	}

	public antituple(size: number): HexVector {
		const x = this.col;
		const z = this.row;

		const a = -size;
		const c = -size + 1;
		const b = -a - c;

		this.col = b * x - a * z;
		this.row = a * x - c * z;

		return this;
	}

	public modulate(other: HexVector): HexVector {
		const x = this.col;
		const z = this.row;

		const a = other.col;
		const b = -other.col - other.row;
		const c = other.row;

		this.col = b * x - a * z;
		this.row = a * x - c * z;

		return this;
	}

	public antimodulate(other: HexVector): HexVector {
		const x = this.col;
		const z = this.row;

		const a = other.col;
		const b = -other.col - other.row;
		const c = other.row;

		const d = 1 / (a * a - b * c);
		const a2 = -a * d;
		const c2 = -b * d;
		const b2 = -a2 - c2;

		this.col = b2 * x - a2 * z;
		this.row = a2 * x - c2 * z;

		return this;
	}

	public wrap(size: number): HexVector {
		if(this.length() <= size) { return this; }

		const x = this.col;
		const z = this.row;

		const d = 1 / (3 * size * (size - 1) + 1);
		const a = size * d;
		const b = (size - 1) * d;
		const c = -a - b;

		const x2 = b * x - a * z;
		const z2 = a * x - c * z;

		const rx = Math.round(x2);
		const ry = Math.round(-x2 - z2);
		const rz = Math.round(z2);

		const x_diff = Math.abs(rx - x2);
		const y_diff = Math.abs(ry + x2 + z2);
		const z_diff = Math.abs(rz - z2);

		const x_dom = x_diff > y_diff && x_diff > z_diff;
		const x3 = x_dom ? -ry - rz : rx;
		const z3 = !x_dom && z_diff > y_diff ? -rx - ry : rz;

		const a2 = -size;
		const c2 = -size + 1;
		const b2 = -a2 - c2;

		const center_x = b2 * x3 - a2 * z3;
		const center_y = a2 * x3 - c2 * z3;

		this.col -= center_x;
		this.row -= center_y;

		return this;
	}

	public toVector2(vector: Vector2) {
		vector.x = 1.5 * this.col;
		vector.y = SQRT_THREE * (this.row + this.col * 0.5);
		return vector;
	}

	public toVector3(vector: Vector3) {
		vector.x = 1.5 * this.col;
		vector.y = 0;
		vector.z = SQRT_THREE * (this.row + this.col * 0.5);
		return vector;
	}

	public fromVector2(vector: Vector2): HexVector {
		this.col = vector.x * TWO_THIRDS;
		this.row = vector.y * SQRT_THREE_DIV_THREE - vector.x * ONE_THIRD;
		return this;
	}

	public fromVector3(vector: Vector3): HexVector {
		this.col = vector.x * TWO_THIRDS;
		this.row = vector.z * SQRT_THREE_DIV_THREE - vector.x * ONE_THIRD;
		return this;
	}

	// public fromCube(x: number, y: number, z: number): HexVector { this.col = x; this.row = z; return this; }
	// public toCube(): [number, number, number] { return [this.col, -this.col-this.row, this.row]; }

	public round(): HexVector {
		const x = this.col;
		const z = this.row;
		const y = -x - z;

		const rx = Math.round(x);
		const ry = Math.round(y);
		const rz = Math.round(z);

		const x_diff = Math.abs(rx - x);
		const y_diff = Math.abs(ry - y);
		const z_diff = Math.abs(rz - z);

		if(x_diff > y_diff && x_diff > z_diff) {
			this.col = -ry - rz;
			this.row = rz;
		} else if(z_diff > y_diff) {
			this.col = rx;
			this.row = -rx - ry;
		} else {
			this.col = rx;
			this.row = rz;
		}

		return this;
	}

	public toIndex(): number {
		if(this.row === 0 && this.col === 0) { return 0; }

		const x = this.col;
		const z = this.row;
		const y = -x - z;

		const x_abs = Math.abs(x);
		const y_abs = Math.abs(y);
		const z_abs = Math.abs(z);

		const length = (x_abs + y_abs + z_abs) * 0.5;
		const radius = length - 1;
		const ring = 3 * radius * (radius + 1) + 1;
		let pos;

		if(z_abs >= y_abs && z_abs > x_abs) {
			if(z > 0) {
				pos = 3 * length + y_abs - 1;
			} else {
				pos = 0 * length + y_abs - 1;
			}
		} else if(y_abs >= x_abs) {
			if(y > 0) {
				pos = 1 * length + x_abs - 1;
			} else {
				pos = 4 * length + x_abs - 1;
			}
		} else {
			if(x > 0) {
				pos = 5 * length + z_abs - 1;
			} else {
				pos = 2 * length + z_abs - 1;
			}
		}

		return ring + pos;
	}

	public fromIndex(index: number): HexVector {
		if(index === 0) {
			this.row = 0;
			this.col = 0;
			return this;
		}

		const ring = Math.ceil(Math.sqrt((index + 1) * ONE_THIRD - ONE_TWELFTH) - 0.5);
		const radius = ring - 1;
		const area = 3 * radius * (radius + 1) + 1;
		const pos = index - area;
		const segment = Math.floor(pos / ring);
		const segmentpos = pos - ring * segment + 1;

		let x: number, z: number;

		if(segment === 0) {
			z = -ring;
			// y = segmentpos;
			// x = -y-z;
			x = -segmentpos + ring;
		} else if(segment === 1) {
			// y = ring;
			x = -segmentpos;
			// z = -x-y;
			z = segmentpos - ring;
		} else if(segment === 2) {
			x = -ring;
			z = segmentpos;
			// y = -x-z;
		} else if(segment === 3) {
			z = ring;
			// y = -segmentpos;
			// x = -y-z;
			x = segmentpos - ring;
		} else if(segment === 4) {
			// y = -ring;
			x = segmentpos;
			// z = -x-y;
			z = -segmentpos + ring;
		} else {
			x = ring;
			z = -segmentpos;
			// y = -x-z;
		}

		this.col = x;
		this.row = z;
		return this;
	}
}

Transients.register(HexVector, 1000);

export class HexGrid<Data> implements Iterable<Data> {
	public size: number;
	public length: number;
	private data: Array<Data>;

	constructor(size: number) {
		this.size = size;
		this.length = hexCircleArea(this.size);
		this.data = new Array(this.length).fill(undefined) as Array<Data>;
	}

	public get(key: HexVector): Data | undefined {
		if(key.length() > this.data.length) { return; }
		return this.getByIndex(key.toIndex());
	}

	public getByIndex(index: number): Data | undefined {
		if(index >= this.data.length) { return; }
		return this.data[index];
	}

	public set(key: HexVector, data: Data): void {
		if(key.length() > this.data.length) { throw new RangeError(); }
		return this.setByIndex(key.toIndex(), data);
	}

	public setByIndex(index: number, data: Data): void {
		if(index >= this.data.length) { throw new RangeError(); }
		this.data[index] = data;
	}

	public fill(data: Data): HexGrid<Data> {
		for(let index = this.data.length - 1; index >= 0; index--) {
			this.data[index] = data;
		}
		return this;
	}

	public keys(): IterableIterator<HexVector> {
		const hex = Transients.get(HexVector);
		return mapIterable(this.keysByIndex(), index => hex.fromIndex(index));
	}

	public keysByIndex(): IterableIterator<number> {
		return rangeIterable(this.data.length);
	}

	public values(): IterableIterator<Data> {
		return this.data[Symbol.iterator]();
	}

	public entries(): IterableIterator<[HexVector, Data]> {
		return zipIterables(this.keys(), this.values());
	}

	public entriesByIndex(): IterableIterator<[number, Data]> {
		return enumerateIterable(this.values());
	}

	public [Symbol.iterator](): IterableIterator<Data> {
		return this.values();
	}
}

export class HexMap<Data> implements Iterable<Data> {
	public data: Map<number, Data> = new Map<number, Data>();

	public clear(): void {
		this.data.clear();
	}

	public delete(key: HexVector) {
		this.data.delete(key.toIndex());
	}

	public get(key: HexVector): Data | undefined {
		return this.data.get(key.toIndex());
	}

	public set(key: HexVector, data: Data): void {
		this.data.set(key.toIndex(), data);
	}

	public has(key: HexVector): boolean {
		return this.data.has(key.toIndex());
	}

	public keys(): IterableIterator<HexVector> {
		const hex = Transients.get(HexVector);
		return mapIterable(this.data.keys(), index => hex.fromIndex(index));
	}

	public values(): IterableIterator<Data> {
		return this.data.values();
	}

	public entries(): IterableIterator<[HexVector, Data]> {
		return zipIterables(this.keys(), this.values());
	}

	public [Symbol.iterator](): IterableIterator<Data> {
		return this.values();
	}
}

export function hexCircleArea(radius: number): number {
	radius = Math.floor(radius);
	return 3 * radius * (radius - 1) + 1;
}

export function hexCircleSideLength(radius: number): number {
	radius = Math.floor(radius);
	return Math.sqrt(3 * radius * (radius - 1) + 1);
}

export function* hexRay(origin: HexVector, direction: HexVector): IterableIterator<[HexVector, number, number]> {
	const position = Transients.get(HexVector).copy(origin);
	const center = Transients.get(HexVector).copy(origin).round();
	const startcenter = Transients.get(HexVector);
	const dir = Transients.get(HexVector).copy(direction);
	const dir_x = dir.col;
	const dir_y = -dir.col - dir.row;
	const dir_z = dir.row;
	const speed_xz = (dir_x - dir_z);
	const speed_yx = (dir_y - dir_x);
	const speed_zy = (dir_z - dir_y);
	const speed_xz_sign = Math.sign(speed_xz);
	const speed_yx_sign = Math.sign(speed_yx);
	const speed_zy_sign = Math.sign(speed_zy);
	let distance = 0;
	let startdistance = 0;
	const temp = Transients.get(HexVector);
	const result: [HexVector, number, number] = [temp, 0, 0];

	while(true) {
		startdistance = distance;
		startcenter.copy(center);

		temp.copy(position).sub(center);
		const hex_x = temp.col;
		const hex_y = -temp.col - temp.row;
		const hex_z = temp.row;
		const dist_xz = speed_xz_sign - (hex_x - hex_z);
		const dist_yx = speed_yx_sign - (hex_y - hex_x);
		const dist_zy = speed_zy_sign - (hex_z - hex_y);
		const steps_xz = speed_xz_sign !== 0 ? Math.abs(dist_xz / speed_xz) : Infinity;
		const steps_yx = speed_yx_sign !== 0 ? Math.abs(dist_yx / speed_yx) : Infinity;
		const steps_zy = speed_yx_sign !== 0 ? Math.abs(dist_zy / speed_zy) : Infinity;

		let min_steps;
		if(steps_xz < steps_yx && steps_xz < steps_zy) {
			min_steps = steps_xz;
			center.add(temp.set(-1, 1).multiplyScalar(speed_xz_sign));
		} else if(steps_yx < steps_zy) {
			min_steps = steps_yx;
			center.add(temp.set(0, -1).multiplyScalar(speed_yx_sign));
		} else {
			min_steps = steps_zy;
			center.add(temp.set(1, 0).multiplyScalar(speed_zy_sign));
		}

		distance += min_steps;
		position.add(temp.copy(dir).multiplyScalar(min_steps));

		result[0] = startcenter;
		result[1] = startdistance;
		result[2] = distance;
		yield result;
	}
}

export function hexStep(position: HexVector, direction: HexVector): number {
	Transients.push(HexVector);
	const dir_x = direction.col;
	const dir_z = direction.row;
	const dir_y = -dir_x - dir_z;
	const speed_xz = (dir_x - dir_z);
	const speed_yx = (dir_y - dir_x);
	const speed_zy = (dir_z - dir_y);
	const speed_xz_sign = Math.sign(speed_xz);
	const speed_yx_sign = Math.sign(speed_yx);
	const speed_zy_sign = Math.sign(speed_zy);

	const center = Transients.get(HexVector).copy(position).round();
	const difference = Transients.get(HexVector).copy(position).sub(center);

	const hex_x = difference.col;
	const hex_z = difference.row;
	const hex_y = -hex_x - hex_z;
	const dist_xz_exact = speed_xz_sign - (hex_x - hex_z);
	const dist_yx_exact = speed_yx_sign - (hex_y - hex_x);
	const dist_zy_exact = speed_zy_sign - (hex_z - hex_y);
	const dist_xz = dist_xz_exact;
	const dist_yx = dist_yx_exact;
	const dist_zy = dist_zy_exact;
	const steps_xz = speed_xz_sign !== 0 ? Math.abs(dist_xz / speed_xz) : Infinity;
	const steps_yx = speed_yx_sign !== 0 ? Math.abs(dist_yx / speed_yx) : Infinity;
	const steps_zy = speed_yx_sign !== 0 ? Math.abs(dist_zy / speed_zy) : Infinity;
	const min_steps = steps_xz < steps_yx && steps_xz < steps_zy ? steps_xz : steps_yx < steps_zy ? steps_yx : steps_zy;

	const step = Transients.get(HexVector).copy(direction).multiplyScalar(min_steps + EPSILON / direction.length());
	position.add(step);

	Transients.pop(HexVector);
	return min_steps;
}

export function* triangleRay(origin: HexVector, direction: HexVector): IterableIterator<[HexVector, number]> {
	const position = Transients.get(HexVector).copy(origin);
	const dir = Transients.get(HexVector).copy(direction);
	const temp = Transients.get(HexVector);
	const speed_x = dir.col;
	const speed_z = dir.row;
	const speed_y = -speed_x - speed_z;
	const speed_x_abs = Math.abs(speed_x);
	const speed_y_abs = Math.abs(speed_y);
	const speed_z_abs = Math.abs(speed_z);
	const speed_x_sign = Math.sign(speed_x) > 0;
	const speed_y_sign = Math.sign(speed_y) > 0;
	const speed_z_sign = Math.sign(speed_z) > 0;
	let distance = 0;
	const result: [HexVector, number] = [temp, 0];

	while(true) {
		result[0] = position;
		result[1] = distance;
		yield result;
		if(Math.abs(speed_x) === 0 && Math.abs(speed_y) === 0) {
			return;
		}

		const x = position.col;
		const z = position.row;
		const y = -x - z;
		const x_part_exact = x - Math.floor(x);
		const y_part_exact = y - Math.floor(y);
		const z_part_exact = z - Math.floor(z);
		const x_part = x_part_exact < EPSILON || 1 - x_part_exact < EPSILON ? 0 : x_part_exact;
		const y_part = y_part_exact < EPSILON || 1 - y_part_exact < EPSILON ? 0 : y_part_exact;
		const z_part = z_part_exact < EPSILON || 1 - z_part_exact < EPSILON ? 0 : z_part_exact;
		const dist_x = x_part === 0 ? 1 : speed_x_sign ? 1 - x_part : x_part;
		const dist_y = y_part === 0 ? 1 : speed_y_sign ? 1 - y_part : y_part;
		const dist_z = z_part === 0 ? 1 : speed_z_sign ? 1 - z_part : z_part;
		const steps_x = dist_x / speed_x_abs;
		const steps_y = dist_y / speed_y_abs;
		const steps_z = dist_z / speed_z_abs;
		const min_steps = steps_x < steps_y && steps_x < steps_z ? steps_x : steps_y < steps_z ? steps_y : steps_z;

		distance += min_steps;
		position.add(temp.copy(dir).multiplyScalar(min_steps));
	}
}

export function* hexNeighbors(hex: HexVector): IterableIterator<HexVector> {
	Transients.push(HexVector);
	const copy = Transients.get(HexVector).copy(hex);
	const temp = Transients.get(HexVector);

	for(let direction = 0; direction < 6; direction++) {
		yield temp.copy(copy).add(HexDirections[direction]);
	}
	Transients.pop(HexVector);
}

export function* hexNeighborIndexes(index: number): IterableIterator<number> {
	Transients.push(HexVector);
	const copy = Transients.get(HexVector).fromIndex(index);
	const temp = Transients.get(HexVector);

	for(let direction = 0; direction < 6; direction++) {
		yield temp.copy(copy).add(HexDirections[direction]).toIndex();
	}
	Transients.pop(HexVector);
}

export function* hexLine(start: HexVector, end: HexVector): IterableIterator<HexVector> {
	Transients.push(HexVector);
	const copy = Transients.get(HexVector).copy(start);
	const step = Transients.get(HexVector).copy(end).sub(start);
	const length = step.length();
	step.multiplyScalar(1 / length);
	const result = Transients.get(HexVector);
	let index = 0;

	while(index <= length) {
		yield result.copy(step).multiplyScalar(index).add(copy).round();
		index += 1;
	}
	Transients.pop(HexVector);
}

export function* hexExactLine(start: HexVector, end: HexVector): IterableIterator<HexVector> {
	Transients.push(HexVector);
	const copy = Transients.get(HexVector).copy(start);
	const step = Transients.get(HexVector).copy(end).sub(start);
	const length = step.length();
	step.multiplyScalar(1 / length);
	const result = Transients.get(HexVector);
	let index = 0;

	while(index <= length) {
		yield result.copy(step).multiplyScalar(index).add(copy);
		index += 1;
	}
	Transients.pop(HexVector);
}

export function* hexFilledCircle(center: HexVector, radius: number): IterableIterator<HexVector> {
	Transients.push(HexVector);
	const copy = Transients.get(HexVector).copy(center);
	const max = hexCircleArea(radius);
	const result = Transients.get(HexVector);

	for(let index = 0; index < max; index++) {
		yield result.fromIndex(index).add(copy);
	}
	Transients.pop(HexVector);
}

export function* hexCircleEdges(center: HexVector, radius: number): IterableIterator<HexVector> {
	Transients.push(HexVector);
	const copy = Transients.get(HexVector).copy(center);
	const result = Transients.get(HexVector);
	const ringlength = 6 * radius;

	for(let pos = 0; pos < ringlength; pos++) {
		const segment = Math.floor(pos / radius);
		const segmentpos = pos - radius * segment + 1;

		let x: number, z: number;

		if(segment === 0) {
			x = radius;
			z = -radius + segmentpos;
		} else if(segment === 1) {
			x = radius - segmentpos;
			z = segmentpos;
		} else if(segment === 2) {
			x = -segmentpos;
			z = radius;
		} else if(segment === 3) {
			x = -radius;
			z = radius - segmentpos;
		} else if(segment === 4) {
			x = -radius + segmentpos;
			z = -segmentpos;
		} else {
			x = segmentpos;
			z = -radius;
		}

		yield result.set(z, x).add(copy);
	}
	Transients.pop(HexVector);
}

// tslint:disable-next-line:variable-name
export const HexDirections = [
	new HexVector().fromIndex(1),
	new HexVector().fromIndex(2),
	new HexVector().fromIndex(3),
	new HexVector().fromIndex(4),
	new HexVector().fromIndex(5),
	new HexVector().fromIndex(6),
];

export const HexDirectionNames = {
	up: new HexVector().fromIndex(1),
	upright: new HexVector().fromIndex(2),
	downright: new HexVector().fromIndex(3),
	down: new HexVector().fromIndex(4),
	downleft: new HexVector().fromIndex(5),
	upleft: new HexVector().fromIndex(6),
};
