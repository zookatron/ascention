# Ascention

Ascention is a real-time strategy game with 4X elements, intended to be played on an epic scale. It will use procedural generation for all assets.

![First Gameplay Screenshot](screenshots/screenshot1.jpg "First Gameplay Screenshot")
![Second Gameplay Screenshot](screenshots/screenshot2.jpg "Second Gameplay Screenshot")
![Third Gameplay Screenshot](screenshots/screenshot3.jpg "Third Gameplay Screenshot")
